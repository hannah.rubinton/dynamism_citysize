#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
Two types of labor unskilled 
\begin_inset Formula $L$
\end_inset

 and high skilled 
\begin_inset Formula $H$
\end_inset

.
 
\end_layout

\begin_layout Standard
Final goods aggregator in each city produces according to 
\begin_inset Formula 
\[
Y_{j}=(\gamma_{Lj}(z_{Lj}L_{j})^{\frac{\epsilon-1}{\epsilon}}+\gamma_{Hj}(z_{Hj}H_{j})^{\frac{\epsilon-1}{\epsilon}})^{\frac{\epsilon}{\epsilon-1}}
\]

\end_inset

The final goods producer sovle 
\begin_inset Formula 
\[
maxY_{j}-w_{Lj}L_{j}-w_{Hj}H_{j}
\]

\end_inset

focs: 
\begin_inset Formula 
\[
Y_{j}^{\frac{1}{\epsilon}}\gamma_{Lj}z_{Lj}^{\frac{\epsilon-1}{\epsilon}}L_{j}^{\frac{-1}{\epsilon}}=w_{Lj}
\]

\end_inset


\begin_inset Formula 
\[
Y_{j}^{\frac{1}{\epsilon}}\gamma_{Hj}z_{Hj}^{\frac{\epsilon-1}{\epsilon}}H_{j}^{\frac{-1}{\epsilon}}=w_{Hj}
\]

\end_inset

So the skill premium in city 
\begin_inset Formula $j$
\end_inset

 will be 
\begin_inset Formula 
\[
\hat{w}_{j}=\frac{w_{H_{j}}}{w_{Lj}}=\frac{\gamma_{Hj}}{\gamma_{Lj}}\left(\frac{z_{Hj}}{z_{Lj}}\right)^{\frac{\epsilon-1}{\epsilon}}\left(\frac{H_{j}}{L_{j}}\right)^{\frac{-1}{\epsilon}}
\]

\end_inset

Solving the first order conditions for H and L 
\begin_inset Formula 
\[
L=Y_{j}\gamma_{Lj}^{\epsilon}z_{Lj}^{\epsilon-1}\frac{1}{w_{Lj}^{\epsilon}}
\]

\end_inset

and plugging them into the unit cost function 
\begin_inset Formula 
\[
1=w_{h}H+w_{L}L
\]

\end_inset


\begin_inset Formula 
\[
=w_{H}^{1-\epsilon}Y_{j}\gamma_{Hj}^{\epsilon}z_{Hj}^{\epsilon-1}+w_{L}^{1-\epsilon}Y_{j}\gamma_{Lj}^{\epsilon}z_{Lj}^{\epsilon-1}
\]

\end_inset


\begin_inset Formula 
\[
1=\left(\gamma_{Hj}^{\epsilon}z_{Hj}^{\epsilon-1}w_{H}^{1-\epsilon}+\gamma_{Lj}^{\epsilon}z_{Lj}^{\epsilon-1}w_{L}^{1-\epsilon}\right)^{\frac{1}{1-\epsilon}}
\]

\end_inset


\begin_inset Formula 
\[
w_{H}=\left(\frac{1-\gamma_{Lj}^{\epsilon}z_{Lj}^{\epsilon-1}w_{L}^{1-\epsilon}}{\gamma_{Hj}^{\epsilon}z_{Hj}^{\epsilon-1}}\right)^{\frac{1}{1-\epsilon}}
\]

\end_inset

or I can get wh as a function of the relative factor shares 
\begin_inset Formula 
\[
1=w_{H}\left(\gamma_{Hj}^{\epsilon}z_{Hj}^{\epsilon-1}+\gamma_{Lj}^{\epsilon}z_{Lj}^{\epsilon-1}\frac{w_{L}^{1-\epsilon}}{w_{H}^{1-\epsilon}}\right)^{\frac{1}{1-\epsilon}}
\]

\end_inset


\begin_inset Formula 
\[
w_{H}=\left(\gamma_{Hj}^{\epsilon}z_{Hj}^{\epsilon-1}+\gamma_{Lj}^{\epsilon}z_{Lj}^{\epsilon-1}\hat{w}_{j}^{\epsilon-1}\right)^{\frac{-1}{1-\epsilon}}
\]

\end_inset


\end_layout

\end_body
\end_document
