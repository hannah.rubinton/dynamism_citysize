
cap log close
clear
set more off

global QCEW "/Users/hannah/Google Drive File Stream/My Drive/rawdata/QCEW"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_qcew/output"




/************************
QCEW naics files start in 1990
*************************/

forvalues yr = 1990/2016 {
import delimited  "$QCEW/naics/`yr'.annual.singlefile.csv", clear varnames(1)

keep if agglvl_code==47 | agglvl_code==41 // MSA, private, 5-digit naics || MSA by ownership
keep  if own_code==5 //private
keep if industry_code=="52211" | industry_code =="10" // banking and aggregate

keep area_fips industry_code year annual_avg_emplvl annual_avg_estabs agglvl_code lq* disclosure_code
	
*use the total msa data to get a measurement of private total employment
gen temp = annual_avg_emplvl if agglvl_code==41
bys area_fips: egen tot_emp = total(temp)
drop temp

gen temp = annual_avg_estabs if agglvl_code==41 
bys area_fips: egen tot_estabs = total(temp)
drop temp

*gen industry shares 
cap replace annual_avg_emplvl = . if disclosure_code=="N"
gen sh_emp_banking = annual_avg_emplvl /tot_emp 
gen sh_estabs_banking = annual_avg_estabs / tot_estabs
gen bankemp_per_estabs = annual_avg_emp / (tot_estabs - annual_avg_estabs)
gen banks_per_estabs = annual_avg_estabs / (tot_estabs - annual_avg_estabs)
rename annual_avg_emplvl  bankemp 
rename annual_avg_estabs  banks



*drop the aggregated data, keep industryXmsa level data
drop if agglvl_code==41


keep year area_fips sh_emp_banking sh_estabs_banking bank*

*clean fips codes
replace area_fips = substr(area_fips,2,4)
destring area_fips, replace
rename area_fips cbsa 
replace cbsa=cbsa*10

if `yr' == 1990 {
	save "$data/qcew_banking_cbsa.dta", replace
}
else {
	append using "$data/qcew_banking_cbsa.dta", force
	save "$data/qcew_banking_cbsa.dta", replace 
}

}


