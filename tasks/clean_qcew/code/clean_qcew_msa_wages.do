
cap log close
clear
set more off

global xwalks "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_crosswalks/output"
global QCEW "/Users/hannah/Google Drive File Stream/My Drive/rawdata/QCEW"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_qcew/output"

/************************
QCEW naics files start in 1990
*************************/

forvalues yr = 1990/2016 {
import delimited  "$QCEW/naics/`yr'.annual.singlefile.csv", clear varnames(1)

keep if  agglvl_code==41  // cbsa, private, 2-digit naics || cbsa by ownership
keep  if own_code==5

keep area_fips year annual_avg_emplvl *annual_avg_wkly_wage* disclosure_code
	

*correct for disclosure
cap replace annual_avg_emplvl = . if disclosure_code=="N"
cap replace annual_avg_wkly_wage if disclosure_code=="N"

rename annual_avg_wkly_wage qcew_wkly_wage
rename oty_annual_avg_wkly_wage_chg qcew_wkly_wage_chg
rename oty_annual_avg_wkly_wage_pct_chg qcew_wkly_wage_pct_chg

*clean fips codes
replace area_fips = substr(area_fips,2,4) 
destring area_fips, replace
rename area_fips cbsa 
replace cbsa=cbsa*10 


if `yr' == 1990 {
	save "$data/qcew_wage_cbsa.dta", replace
}
else {
	append using "$data/qcew_wage_cbsa.dta", force
	save "$data/qcew_wage_cbsa.dta", replace 
}

}




/************************
QCEW sic files start in 1975
*************************/
forvalues yr = 1978/1989 {
import delimited  "$QCEW/sic/sic.`yr'.annual.singlefile.csv", clear varnames(1)

keep if  agglvl_code==27 // county, Industry Division sic or county, all covered
keep if own_code == 5 //private
drop if industry_code =="SIC_0K" // unclassified

keep area_fips year annual_avg_estabs_count annual_avg_emplvl annual_avg_wkly_wage disclosure_code

rename annual_avg_wkly_wage qcew_wkly_wage

*correct for disclosure
cap replace annual_avg_emplvl = . if disclosure_code=="N"
cap replace annual_avg_estabs_county = . if disclosure_code=="N"
cap replace annual_avg_wkly_wage if disclosure_code=="N"

*fips codes
rename area_fips countyfips
destring countyfips, replace
merge m:1 countyfips using "$xwalks/cleaned_countytocbsacrosswalk_2009definitions.dta" //map counties to cbsas
drop if _merge==2

destring disclosure_code, replace 
 
collapse (sum) annual_avg_estabs_count annual_avg_emplvl annual_avg_wkly_wage  disclosure_code, by(year cbsa cbsaname)
replace annual_avg_emplvl = . if disclosure_code >0
replace annual_avg_estabs_count = . if disclosure_code >0
replac annual_avg_wkly_wage = . if disclosure_code>0

append using "$data/qcew_wage_cbsa.dta"
save "$data/qcew_wage_cbsa.dta", replace 


}





drop _merge countyfips disclosure_code cbsaname lq

save "$data/qcew_wage_cbsa.dta", replace 

