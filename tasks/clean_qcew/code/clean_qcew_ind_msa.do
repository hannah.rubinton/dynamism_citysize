
cap log close
clear
set more off

global QCEW "/Users/hannah/Google Drive File Stream/My Drive/rawdata/QCEW"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_qcew/output"


/************************
QCEW naics files start in 1990
*************************/
/*
forvalues yr = 1990/2016 {
import delimited  "$QCEW/naics/`yr'.annual.singlefile.csv", clear varnames(1)

keep if agglvl_code==44 | agglvl_code==41 // cbsa, private, 2-digit naics || cbsa by ownership
keep  if own_code==5

keep area_fips industry_code year annual_avg_emplvl agglvl_code *annual_avg_wkly_wage* lq* disclosure_code
	
*use the total cbsa data to get a measurement of private total employment
gen temp = annual_avg_emplvl if agglvl_code==41
bys area_fips: egen tot_emp = total(temp)

*gen industry shares 
cap replace annual_avg_emplvl = . if disclosure_code=="N"
gen sh_emp_ind = annual_avg_emplvl /tot_emp 

cap replace annual_avg_wkly_wage if disclosure_code=="N"

rename annual_avg_wkly_wage qcew_wkly_wage
rename oty_annual_avg_wkly_wage_chg qcew_wkly_wage_chg
rename oty_annual_avg_wkly_wage_pct_chg qcew_wkly_wage_pct_chg

*drop the aggregated data, keep industryXcbsa level data
drop if agglvl_code==41
drop temp 

if `yr' == 1990 {
	save "$data/qcew_naics_cbsa.dta", replace
}
else {
	append using "$data/qcew_naics_cbsa.dta", force
	save "$data/qcew_naics_cbsa.dta", replace 
}

}

*/

/**********************
clean up the QCEW by NAICS industry data
***********************/
/*
use "$data/qcew_naics_cbsa.dta", clear 
keep area_fips industry_code year sh* lq* 
replace area_fips = substr(area_fips,2,4)
destring area_fips, replace
replace industry_code = substr(industry_code,1,2) 
destring industry_code, replace 

drop if industry_code ==99 // unclassified

reshape wide  sh* lq*, i(area_fips year) j(industry_code)

rename area_fips cbsa 
replace cbsa=cbsa*10

save "$data/qcew_cbsa_ind_composition.dta", replace

*/


/************************
QCEW sic files start in 1975
*************************/
forvalues yr = 1978/2000 {
import delimited  "$QCEW/sic/sic.`yr'.annual.singlefile.csv", clear varnames(1)

keep if agglvl_code==28 | agglvl_code==27 // county, Industry Division sic or county, all covered
keep if own_code == 5 //private
drop if industry_code =="SIC_0K" // unclassified

keep area_fips year annual_avg_estabs_count annual_avg_emplvl agglvl_code industry_code disclosure_code

*create variable of total emplvl for the county
gen temp = annual_avg_emplvl if agglvl_code==27
bys year area_fips: egen tot_emp = total(temp)
drop temp

gen temp = annual_avg_estabs_count if agglvl_code==27
bys year area_fips: egen tot_estabs = total(temp)
drop temp

drop if agglvl_code==27


if `yr' ==1978 {
	save "$data/qcew_sic_cbsa.dta", replace
}
else {
	append using "$data/qcew_sic_cbsa.dta"
	save "$data/qcew_sic_cbsa.dta", replace 
}

}


/**********************
clean up the QCEW by SIC industry data
***********************/
use "$data/qcew_sic_cbsa.dta", clear 

rename area_fips countyfips
destring countyfips, replace
merge m:1 countyfips using "$swalks/cleaned_countytocbsacrosswalk_2009definitions.dta" //map counties to cbsas

keep cbsa cbsa year annual_avg_emplvl tot_emp annual_avg_estabs_count tot_estabs disclosure_code industry_code
 
*make disclosure code a numeric variable
replace disclosure_code = "1" if disclosure_code=="N"
replace disclosure_code = "0" if disclosure_code==""
destring disclosure_code, replace 
 
collapse (sum) annual_avg_emplvl tot_emp annual_avg_estabs_count tot_estabs disclosure_code, by(year cbsa cbsaname industry_code)
replace annual_avg_emplvl = . if disclosure_code >0
replace tot_emp = . if disclosure_code>0
replace annual_avg_estabs_count = . if disclosure_code >0
replace tot_estabs = . if disclosure_code>0

gen sh_emp = annual_avg_emplvl / tot_emp
gen sh_estabs = annual_avg_estabs_count / tot_estabs
drop tot_emp tot_estabs

egen sic = group(industry_code) 
drop if sic==.
drop industry_code disclosure_code


reshape wide  sh* annual_avg_emplvl annual_avg_estabs_count, i(cbsa cbsa year) j(sic)
label var sh_emp1 "sh_emp agr"
label var sh_emp2 "sh_emp min"
label var sh_emp3 "sh_emp con" 
label var sh_emp4 "sh_emp man"
label var sh_emp5 "sh_emp trsp&pub ut."
label var sh_emp6 "sh_emp Who Tr"
label var sh_emp7 "sh_emp ret tr"
label var sh_emp8 "sh_emp FIRE" 
label var sh_emp9 "sh_emp srv" 
label var sh_emp10 "sh_emp pub admin"

save "$data/qcew_cbsa_sic_ind_composition.dta", replace


/************************
QCEW by cbsa files start in 1990
*************************

forvalues yr = 1975/2016 {
if `yr' <1990 {
	import delimited  "$QCEW/naics/`yr'.annual 10 Total, all industries.csv", clear varnames(1)
} 
else {
	import delimited  "$QCEW/naics/`yr'.annual.singlefile.csv", clear varnames(1)
}
keep if agglvl_code==41 // cbsa, all industries
keep if own_code==5 // private
drop disclosure_code 
cap destring industry_code, replace
if `yr' == 1975 {
	save "$data/qcew_cbsa.dta", replace
}
else {
	append using "$data/qcew_cbsa.dta"
	save "$data/qcew_cbsa.dta", replace 
}

}



/************************
QCEW by COUNTY files start in 1990
*************************/

forvalues yr = 1975/2016 {
if `yr' <1990 {
	import delimited  "$QCEW/naics/`yr'.annual 10 Total, all industries.csv", clear varnames(1)
} 
else {
	import delimited  "$QCEW/naics/`yr'.annual.singlefile.csv", clear varnames(1)
}
keep if agglvl_code==71 // county, all industries
keep if own_code==5 // private
drop disclosure_code 
cap destring industry_code, replace
if `yr' == 1975 {
	save "$data/qcew_county.dta", replace
}
else {
	append using "$data/qcew_county.dta"
	save "$data/qcew_county.dta", replace 
}

}

sort area_fips year
by area_fips: gen avg_wg_gr_5yr = ( oty_annual_avg_wkly_wage_pct_chg[_n] + oty_annual_avg_wkly_wage_pct_chg[_n-1] + ///
	oty_annual_avg_wkly_wage_pct_chg[_n-2] + oty_annual_avg_wkly_wage_pct_chg[_n-3] + oty_annual_avg_wkly_wage_pct_chg[_n-4])/5


	
	
	
	

