
cap log close
clear
set more off

/*
merge the alternative specifications iwth the full dataset
want to do some regressions of inc and inc gr on sr and pop and initial inc for 
meeting with Eduardo 


*/


global mergeddata "/Users/hannah/Dropbox/dynamism_citysize/tasks/merge_cbsa_data/output"
global acs "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_acs/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/dispersion"
global census "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_seer/output"

use "$acs/acs_cbsa_alt_spec.dta", clear

rename lincwage_college_cbsafe lincwage_c_ind_cbsafe
rename lincwage_nc_cbsafe lincwage_nc_ind_cbsafe
rename lincwage_college_fs_cbsafe lincwage_c_occ_cbsafe
rename lincwage_nc_fs_cbsafe lincwage_nc_occ_cbsafe


merge 1:1 cbsa year using "$mergeddata/sr_cbsa.dta"


drop lq*
drop job_*
drop sh_emp*
drop if year==.

replace sr = sr*100
replace fer = fer*100

gen lincwage_c = log(incwage_college) 
gen lincwage_nc = log(incwage_nc)
gen lpipc_bea = log(pipc_bea)


rename lincwage_college_cbsafe lincwage_c_cbsafe
rename lincwage_college_fs_cbsafe lincwage_c_fs_cbsafe 

*keep if inlist(year,1980,1990,2000,2007,2010,2013)
keep lincwage1 lincwage2 lincwage3 lincwage4 lincwage5 /// 
	lincwage_c lincwage_nc lincwage_m lincwage ///
	lincwage_cbsafe lincwage_c_cbsafe lincwage_nc_cbsafe ///
	lincwage_fs_cbsafe lincwage_c_fs_cbsafe lincwage_nc_fs_cbsafe ///
	lincwage_c_ind_cbsafe lincwage_nc_ind_cbsafe lincwage_c_occ_cbsafe lincwage_nc_occ_cbsafe ///
	lpipc_bea pop lpop wapop cbsa year sr fer esr eer jcr jdr sh_topcoded
reshape wide lincwage* lpipc_bea lpop pop wapop sr fer esr eer jcr jdr sh_topcoded, i(cbsa) j(year)


local dv sr fer esr eer jcr jdr

forvalues yr = 1980/2013 {
	local tm1 = `yr'-1
	local tp1 = `yr'+1
	foreach v of local dv {
		gen `v'_3yma`yr' = (`v'`yr' + `v'`tm1' + `v'`tp1')/3
	}
}




local incvars lincwage1 lincwage2 lincwage3 lincwage4 lincwage5 /// 
	lincwage_c lincwage_nc lincwage_m lincwage ///
	lpipc_bea  ///
	lincwage_cbsafe lincwage_c_cbsafe lincwage_nc_cbsafe ///
	lincwage_fs_cbsafe lincwage_c_fs_cbsafe lincwage_nc_fs_cbsafe ///
	lincwage_c_ind_cbsafe lincwage_nc_ind_cbsafe lincwage_c_occ_cbsafe lincwage_nc_occ_cbsafe 
	
local years 1980 1990 2000 
foreach v of local incvars {

/*
	foreach y of local years {
		local tp10 = `y' + 10
		gen fdl10_`v'`y' = `v'`tp10' - `v'`y'
		
		
	}
	
	*/
	
	*annualized growth rates so we can compare across periods of different time lenghts 
	*y_{t+10}=y_t(1+g)^10 ---> g=(y_{t+10}/y_t)^(1/10) 
	*gen agr1300_`v' = (`v'2013 / `v'2000)^(1/13)-1
	*gen agr1307_`v' = (`v'2013 / `v'2007)^(1/5)-1
	gen fdl_`v'2000 = (`v'2010 - `v'2000)/10*100
	gen fdl_`v'1995 = (`v'2005 - `v'1995)/10*100
	*gen agr0700_`v' = (`v'2007 / `v'2000)^(1/7)-1
	*gen agr0790_`v' = (`v'2007 / `v'1990)^(1/17)-1
	gen fdl_`v'1990 = (`v'2000 - `v'1990)/10*100
	gen fdl_`v'1980 = (`v'1990 - `v'1980)/10*100

	/*
	label var agr1300_`v' "2000-2013"
	label var agr1307_`v' "2007-2013"
	label var agr1000_`v' "2000-2010"
	label var agr0700_`v' "2000-2007"
	label var agr0790_`v' "1990-2007"
	label var agr0090_`v' "1990-2000"
	label var agr9080_`v' "1980-1990"
	*/
	
	
	
}
	gen fdl_sr_3yma2000 = (sr_3yma2010 - sr_3yma2000)
	gen fdl_sr_3yma1995 = (sr_3yma2005 - sr_3yma1995)
	gen fdl_sr_3yma1990 = (sr_3yma2000 - sr_3yma1990)
	gen fdl_sr_3yma1980 = (sr_3yma1990 - sr_3yma1980)
	
	
keep cbsa *1980 *1990 *1995 *2000 *2007 *2010
reshape long fdl_lincwage1 fdl_lincwage2 fdl_lincwage3 fdl_lincwage4 fdl_lincwage5 /// 
	fdl_lincwage_c fdl_lincwage_nc fdl_lincwage_m fdl_lincwage ///
	fdl_lpipc_bea  ///
	fdl_lincwage_cbsafe fdl_lincwage_c_cbsafe fdl_lincwage_nc_cbsafe ///
	fdl_lincwage_fs_cbsafe fdl_lincwage_c_fs_cbsafe fdl_lincwage_nc_fs_cbsafe ///
	fdl_lincwage_c_ind_cbsafe fdl_lincwage_nc_ind_cbsafe fdl_lincwage_c_occ_cbsafe fdl_lincwage_nc_occ_cbsafe ///
	lincwage1 lincwage2 lincwage3 lincwage4 lincwage5 /// 
	lincwage_c lincwage_nc lincwage_m lincwage ///
	fdl_sr_3yma ///
	lpipc_bea sr_3yma fer_3yma esr_3yma eer_3yma jcr_3yma jdr_3yma pop lpop wapop sh_topcoded ///
	lincwage_cbsafe lincwage_c_cbsafe lincwage_nc_cbsafe ///
	lincwage_fs_cbsafe lincwage_c_fs_cbsafe lincwage_nc_fs_cbsafe ///
	lincwage_c_ind_cbsafe lincwage_nc_ind_cbsafe lincwage_c_occ_cbsafe lincwage_nc_occ_cbsafe sr fer esr eer jcr jdr, i(cbsa) j(year 1980 1990 1995 2000 2007 2010)
/*

*everyone
eststo clear
eststo, title("ch inc"): reg fdl_lincwage i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage i.year i.year#c.lincwage
eststo, title("ch inc"): reg fdl_lincwage i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage i.year i.year#c.lpop i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage i.year i.year#c.lpop i.year#c.lincwage i.year#c.sr_3yma

	esttab using "$ctask/tables/table1_1115.tex", se r2 ///
		 title("income growth vs initial pop, income, dynamism") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace note mtitle
		

*college vs nc
eststo clear
eststo, title("ch inc"): reg fdl_lincwage_c i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage_c i.year i.year#c.lincwage_c
eststo, title("ch inc"): reg fdl_lincwage_c i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_c i.year  i.year#c.lpop i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_c i.year i.year#c.lpop i.year#c.lincwage_c i.year#c.sr_3yma

	esttab using "$ctask/tables/table2_1115.tex", se r2 ///
		 title("college income growth vs initial pop, income, dynamism") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace note mtitle
	
	*college vs nc
eststo, title("ch inc"): reg fdl_lincwage_nc i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage_nc i.year i.year#c.lincwage_nc
eststo, title("ch inc"): reg fdl_lincwage_nc i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_nc i.year i.year#c.lpop i.year#c.lincwage_nc i.year#c.sr_3yma

	esttab using "$ctask/tables/table2_nc_1115.tex", se r2 ///
		 title("nc income growth vs initial pop, income, dynamism") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace  mtitle
	
	
*college vs nc cbsafe
eststo clear
eststo, title("ch inc"): reg fdl_lincwage_c_cbsafe i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage_c_cbsafe i.year i.year#c.lincwage_c_cbsafe
eststo, title("ch inc"): reg fdl_lincwage_c_cbsafe i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_c_cbsafe i.year i.year#c.lpop i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_c_cbsafe i.year i.year#c.lpop i.year#c.lincwage_c_cbsafe i.year#c.sr_3yma

	esttab using "$ctask/tables/table3_1115.tex", se r2 ///
		 title("college income growth vs initial pop, income, dynamism, cbsafe") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace  mtitle
		 
*college vs nc cbsafe
eststo clear
eststo, title("ch inc"): reg fdl_lincwage_nc_cbsafe i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage_nc_cbsafe i.year i.year#c.lincwage_nc_cbsafe
eststo, title("ch inc"): reg fdl_lincwage_nc_cbsafe i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_nc_cbsafe i.year i.year#c.lpop i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_nc_cbsafe i.year i.year#c.lpop i.year#c.lincwage_nc_cbsafe i.year#c.sr_3yma

	esttab using "$ctask/tables/table3_nc_1115.tex", se r2 ///
		 title("nc income growth vs initial pop, income, dynamism, cbsafe") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace  mtitle
		
	
*college vs nc cbsafe industry controls
eststo clear
eststo, title("ch inc"): reg fdl_lincwage_c_ind_cbsafe i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage_c_ind_cbsafe i.year i.year#c.lincwage_c_ind_cbsafe
eststo, title("ch inc"): reg fdl_lincwage_c_ind_cbsafe i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_c_ind_cbsafe i.year i.year#c.lpop i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_c_ind_cbsafe i.year i.year#c.lpop i.year#c.lincwage_c_ind_cbsafe i.year#c.sr_3yma

	esttab using "$ctask/tables/table4_1115.tex", se r2 ///
		 title("college income growth vs initial pop, income, dynamism, cbsafe ind") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace note mtitle

*college vs nc cbsafe industry controls
eststo clear
eststo, title("ch inc"): reg fdl_lincwage_nc_ind_cbsafe i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage_nc_ind_cbsafe i.year i.year#c.lincwage_nc_ind_cbsafe
eststo, title("ch inc"): reg fdl_lincwage_nc_ind_cbsafe i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_nc_ind_cbsafe i.year i.year#c.lpop i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_nc_ind_cbsafe i.year i.year#c.lpop i.year#c.lincwage_nc_ind_cbsafe i.year#c.sr_3yma

	esttab using "$ctask/tables/table4_nc_1115.tex", se r2 ///
		 title("college income growth vs initial pop, income, dynamism, cbsafe ind") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace note mtitle
		 
		 
		 
*college vs nc cbsafe occupation controls
eststo clear
eststo, title("ch inc"): reg fdl_lincwage_c_occ_cbsafe i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage_c_occ_cbsafe i.year i.year#c.lincwage_c_occ_cbsafe
eststo, title("ch inc"): reg fdl_lincwage_c_occ_cbsafe i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_c_occ_cbsafe i.year i.year#c.lpop  i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_c_occ_cbsafe i.year i.year#c.lpop i.year#c.lincwage_c_occ_cbsafe i.year#c.sr_3yma

	esttab using "$ctask/tables/table5_1115.tex", se r2 ///
		 title("college vs nc income growth vs initial pop, income, dynamism, cbsafe occ") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace note mtitle
		
*college vs nc cbsafe occupation controls
eststo clear
eststo, title("ch inc"): reg fdl_lincwage_nc_occ_cbsafe i.year i.year#c.lpop
eststo, title("ch inc"): reg fdl_lincwage_nc_occ_cbsafe i.year i.year#c.lincwage_nc_occ_cbsafe
eststo, title("ch inc"): reg fdl_lincwage_nc_occ_cbsafe i.year i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_nc_occ_cbsafe i.year i.year#c.lpop i.year#c.sr_3yma
eststo, title("ch inc"): reg fdl_lincwage_nc_occ_cbsafe i.year i.year#c.lpop i.year#c.lincwage_nc_occ_cbsafe i.year#c.sr_3yma

	esttab using "$ctask/tables/table5_nc_1115.tex", se r2 ///
		 title("college vs nc income growth vs initial pop, income, dynamism, cbsafe occ") star(* .1 ** .05 *** .01) ///
		 keep(*lpop* *lincwage* *sr*) label replace note mtitle
		
	
		
	
	
	
*sk premium 
gen skprem = lincwage_c - lincwage_nc
gen skprem_cbsafe = lincwage_c_cbsafe - lincwage_nc_cbsafe
gen skprem_ind_cbsafe = lincwage_c_ind_cbsafe - lincwage_nc_ind_cbsafe
gen skprem_occ_cbsafe = lincwage_c_occ_cbsafe - lincwage_nc_occ_cbsafe

eststo clear 
eststo, title("skprem"): reg skprem i.year i.year#c.lpop 
eststo, title("skprem"): reg skprem i.year i.year#c.sr_3yma
eststo, title("skprem"): reg skprem i.year i.year#c.lpop i.year#c.sr_3yma
eststo, title("skprem"): reg skprem_cbsafe i.year i.year#c.lpop 
eststo, title("skprem"): reg skprem_cbsafe i.year i.year#c.sr_3yma
eststo, title("skprem"): reg skprem_cbsafe i.year i.year#c.lpop i.year#c.sr_3yma

	esttab using "$ctask/tables/table6_sp_1115.tex", se r2 ///
		 title("skill premium vs. size and dynamism") star(* .1 ** .05 *** .01) ///
		 keep(*lpop*  *sr*) label replace note mtitle
		
	
eststo clear
eststo, title("ind"): reg skprem_ind_cbsafe i.year i.year#c.lpop 
eststo, title("ind"): reg skprem_ind_cbsafe i.year i.year#c.sr_3yma
eststo, title("ind"): reg skprem_ind_cbsafe i.year i.year#c.lpop i.year#c.sr_3yma
eststo, title("occ"): reg skprem_occ_cbsafe i.year i.year#c.lpop 
eststo, title("occ"): reg skprem_occ_cbsafe i.year i.year#c.sr_3yma
eststo, title("occ"): reg skprem_occ_cbsafe i.year i.year#c.lpop i.year#c.sr_3yma
	esttab using "$ctask/tables/table7_sp_1115.tex", se r2 ///
		 title("skill premium vs. size and dynamism") star(* .1 ** .05 *** .01) ///
		 keep(*lpop*  *sr*) label replace note mtitle
		
	
scatter skprem lpop, by(year)
graph export "$ctask/plots/skprem_citysize.eps", replace
scatter skprem_cbsafe lpop, by(year)
graph export "$ctask/plots/skprem_cbsafe_citysize.eps", replace
scatter skprem_ind_cbsafe lpop, by(year)
graph export "$ctask/plots/skprem_ind_cbsafe_citysize.eps", replace
scatter skprem_occ_cbsafe lpop, by(year)
graph export "$ctask/plots/skprem_occ_cbsafe_citysize.eps", replace
