
cap log close
clear
set more off

global mergeddata "/Users/hannah/Dropbox/dynamism_citysize/tasks/merge_cbsa_data/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/dispersion"

use "$mergeddata/sr_cbsa.dta", clear

drop lq*
drop job_*
drop sh_emp*
drop if year==.

replace sr = sr*100

gen lincwage_college = log(incwage_college) 
gen lincwage_nc = log(incwage_nc)
gen lpipc_bea = log(pipc_bea)




*demean logs
*want to plot log wage distribution, but want to be able ot compare the distribution across years
*so we are just de-meaning the log distribution which is shifting it left or right not changing the shape 
reg lincwage i.year 
predict lincwage_dm, resid
reg lincwage_college i.year 
predict lincwage_college_dm, resid 
reg lincwage_nc i.year 
predict lincwage_nc_dm, resid 

eststo clear
estpost tabstat lincwage_cbsafe lincwage_dm lincwage_college_dm lincwage_nc_dm if inlist(year,1980,1990,2000,2007,2013), ///
	by(year) stats(mean sd p10 p25 p50 p75 p90) columns(variables) nototal
esttab using "$ctask/tables/dispersion_ss.tex", cells("lincwage_resid lincwage_dm lincwage_college_dm lincwage_nc_dm ") replace
 
	
rename lincwage_college_cbsafe lincwage_c_cbsafe
rename lincwage_college_fs_cbsafe lincwage_c_fs_cbsafe 
rename lincwage_college lincwage_c

*keep if inlist(year,1980,1990,2000,2007,2010,2013)
keep lincwage1 lincwage2 lincwage3 lincwage4 lincwage5 /// 
	lincwage_c lincwage_nc lincwage_m lincwage ///
	lincwage_cbsafe lincwage_c_cbsafe lincwage_nc_cbsafe ///
	lincwage_fs_cbsafe lincwage_c_fs_cbsafe lincwage_nc_fs_cbsafe ///
	lpipc_bea pop lpop wapop cbsa year sr sh_topcoded
reshape wide lincwage* lpipc_bea lpop pop wapop sr sh_topcoded, i(cbsa) j(year)


forvalues yr = 1980/2013 {
	local tm1 = `yr'-1
	local tp1 = `yr'+1
	gen sr_3yma`yr' = (sr`yr' + sr`tm1' + sr`tp1')/3
}




local incvars lincwage1 lincwage2 lincwage3 lincwage4 lincwage5 /// 
	lincwage_c lincwage_nc lincwage_m lincwage ///
	lpipc_bea sr_3yma ///
	lincwage_cbsafe lincwage_c_cbsafe lincwage_nc_cbsafe ///
	lincwage_fs_cbsafe lincwage_c_fs_cbsafe lincwage_nc_fs_cbsafe 
	
local years 1980 1990 2000 
foreach v of local incvars {
	foreach y of local years {
		local tp10 = `y' + 10
		gen fdl10_`v'`y' = `v'`tp10' - `v'`y'
		
		
	}
	
	
	/*
	*annualized growth rates so we can compare across periods of different time lenghts 
	*y_{t+10}=y_t(1+g)^10 ---> g=(y_{t+10}/y_t)^(1/10) 
	gen agr1300_`v' = (`v'2013 / `v'2000)^(1/13)-1
	gen agr1307_`v' = (`v'2013 / `v'2007)^(1/5)-1
	gen agr1000_`v' = (`v'2010 / `v'2000)^(1/10)-1
	gen agr0700_`v' = (`v'2007 / `v'2000)^(1/7)-1
	gen agr0790_`v' = (`v'2007 / `v'1990)^(1/17)-1
	gen agr0090_`v' = (`v'2000 / `v'1990)^(1/10)-1
	gen agr9080_`v' = (`v'1990 / `v'1980)^(1/10)-1

	label var agr1300_`v' "2000-2013"
	label var agr1307_`v' "2007-2013"
	label var agr1000_`v' "2000-2010"
	label var agr0700_`v' "2000-2007"
	label var agr0790_`v' "1990-2007"
	label var agr0090_`v' "1990-2000"
	label var agr9080_`v' "1980-1990"
	*/
	
	
	
}

/*
*kdenisty plots
local kdvars lincwage_resid lincwage_dm lincwage_college_dm lincwage_nc_dm 
foreach v of local kdvars {
	twoway (kdensity `v'1980) || (kdensity `v'2007) 
	graph export "$ctask/plots/kdensity_`v'_8007.eps", replace 
	
	*weighted
	twoway (kdensity `v'1980 [aw=pop1980]) || (kdensity `v'2007 [aw=pop2007]), note("weighted by population in each year")   
	graph export "$ctask/plots/kdensity_`v'_popw_8007.eps", replace
}


*convergence plots 
twoway (scatter agr1000_incwage_resid lincwage_resid2000, ms(oh) ) || (scatter agr0090_incwage_resid lincwage_resid1990, ms(oh) ) || /// 
	(scatter agr9080_incwage_resid lincwage_resid1980, ms(oh) ), xtitle("initial log(mean adjusted wage)") ytitle("annualized growth rates") 
	

	
	
*convergence coefficients

local cvars incwage_resid incwage incwage_college incwage_nc
foreach v of local cvars { 

	*convergence plots 
	twoway (scatter agr1000_`v' l`v'2000, ms(oh) ) || (scatter agr0090_`v' l`v'1990, ms(oh) ) || /// 
		(scatter agr9080_`v' l`v'1980, ms(oh) ), xtitle("initial `v'") ytitle("annualized growth rate of `v'") 
	graph export "$ctask/plots/convergence_`v'.eps", replace 
	
	
	eststo clear 
	eststo: reg agr1307_`v' l`v'2007
	eststo: reg agr1300_`v' l`v'2000
	eststo: reg agr1000_`v' l`v'2000
	eststo: reg agr0700_`v' l`v'2000
	eststo: reg agr0090_`v' l`v'1990
	eststo: reg agr9080_`v' l`v'1980

	esttab using "$ctask/tables/convergence_regs_`v'.tex", se r2 ///
		 title("annualized growth rates of `v' vs initial `v'") star(* .1 ** .05 *** .01) /// 
		label replace 	

		
	*weighted 
	*not clear you want to weight. This is getting at the avg experienced by most people, but understimates since it doesn't take into account migration 
	eststo clear 
	eststo: reg agr1307_`v' l`v'2007 [aw=pop2007]
	eststo: reg agr1300_`v' l`v'2000 [aw=pop2000]
	eststo: reg agr1000_`v' l`v'2000 [aw=pop2000]
	eststo: reg agr0700_`v' l`v'2000 [aw=pop2000]
	eststo: reg agr0090_`v' l`v'1990 [aw=pop1990]
	eststo: reg agr9080_`v' l`v'1980 [aw=pop1980]

	esttab using "$ctask/tables/convergence_regs_`v'_popw.tex", se r2 ///
		 title("annualized growth rates of `v' vs initial `v'") star(* .1 ** .05 *** .01) /// 
		label replace note("weighted by initial population")
		
		
}
	
	
*/	
	
*keep if inlist(year,1980,1990,2000,2005,2007,2008,2009,2010,2011,2012,2013,2014,2015)
keep cbsa *1980 *1990 *2000 *2005 *2007 *2008 *2009 *2010 *2011 *2012 *2012 *2013 *2014 *2015
	
reshape long lincwage1 lincwage2 lincwage3 lincwage4 lincwage5 /// 
	lincwage_c lincwage_nc lincwage_m lincwage ///
	lincwage_cbsafe lincwage_c_cbsafe lincwage_nc_cbsafe ///
	lincwage_fs_cbsafe lincwage_c_fs_cbsafe lincwage_nc_fs_cbsafe ///
	lpipc_bea ///
	fdl10_lincwage1 fdl10_lincwage2 fdl10_lincwage3 fdl10_lincwage4 fdl10_lincwage5 /// 
	fdl10_lincwage_c fdl10_lincwage_nc fdl10_lincwage_m fdl10_lincwage ///
	fdl10_lincwage_cbsafe fdl10_lincwage_c_cbsafe fdl10_lincwage_nc_cbsafe ///
	fdl10_lincwage_fs_cbsafe fdl10_lincwage_c_fs_cbsafe fdl10_lincwage_nc_fs_cbsafe ///
	fdl10_lpipc_bea ///
	pop lpop wapop sr_3yma sh_topcoded ///
	, i(cbsa ) j(year 1980 1990 2000 2005 2007 2008 2009 2010 2011 2012 2013 2014 2015)



	
	
reg fdl10_lincwage i.year i.year#c.lpop
reg fdl10_lincwage i.year i.year#c.lincwage
	
	
	
	
	
	
	
	
