
cap log close
clear
set more off

global mergeddata "/Users/hannah/Dropbox/dynamism_citysize/tasks/merge_cbsa_data/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/dispersion"

use "$mergeddata/sr_cbsa.dta", clear
drop lq*
drop job_*
drop sh_emp*


local dv sr fer esr eer jcr jdr 
sort cbsa year
foreach v of local dv {
	by cbsa: gen `v'_3yma = (`v'[_n] + `v'[_n-1] + `v'[_n+1])/3*100
	replace `v' = `v'_3yma 
	replace l`v' = log(`v'_3yma)
}


gen lincwage_college = log(incwage_college) 
gen lincwage_nc = log(incwage_nc)
gen lpipc_bea = log(pipc_bea)

local vars lincwage1 lincwage2 lincwage3 lincwage4 lincwage5 /// 
	lincwage_college lincwage_nc lincwage_m lincwage ///
	lincwage_cbsafe lincwage_college_cbsafe lincwage_nc_cbsafe ///
	lincwage_fs_cbsafe lincwage_college_fs_cbsafe lincwage_nc_fs_cbsafe ///
	lpipc_bea sr fer esr eer jcr jdr  /// 
	lsr lfer lesr leer ljcr ljdr 
	
foreach v of local vars {
	reg `v' i.year i.year#c.lpop
	matrix `v'pop = e(b) 
	matrix `v'popV = e(V)
}


/*
keep year
bys year: keep if _n==1
levelsof year, local(years)
foreach v of local vars {
	gen `v'_beta = .
	gen `v'_ci_lb = .
	gen `v'_ci_ub = .
	
foreach yr of local years {
	replace `v'_beta = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lpop")] if year==`yr'
	replace `v'_ci_lb = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lpop")] - 1.96*sqrt(`v'popV[rownumb(`v'popV,"`yr'.year#c.lpop"),colnumb(`v'popV,"`yr'.year#c.lpop")]) if year==`yr'
	replace `v'_ci_ub = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lpop")] + 1.96*sqrt(`v'popV[rownumb(`v'popV,"`yr'.year#c.lpop"),colnumb(`v'popV,"`yr'.year#c.lpop")]) if year==`yr'

}

}


/*
*bea plots
twoway line lpipc_bea_beta lpipc_bea_ci* year, pstyle(p1 p10 p10) graphregion(color(white)) legend(off) /// 
	title("Elasticity of personal income per capita to population") lpattern("1" "-" "-")
graph export "$ctask/plots/e_pipc_bea.eps", replace 


twoway (line lpipc_bea_beta lpipc_bea_ci* year, pstyle(p1 p1 p1) graphregion(color(white)) lpattern("1" "-" "-") yaxis(1)) ///
	(line sr_beta sr_ci* year, pstyle(p2 p2 p2) lpattern("1" "-" "-") yaxis(2)) ,  legend(off) /// 
	title("Elasticity of personal income per capita and start up rate to population") 
graph export "$ctask/plots/e_pipc_bea_sr.eps", replace 
	


	
*acs plots	
twoway line lincwage_beta lincwage_ci* year, pstyle(p1 p1 p1) legend(off) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") lpattern("1" "-" "-")
graph export "$ctask/plots/e_lincwage.eps", replace 	


twoway line lincwage_m_beta lincwage_m_ci* lincwage_beta lincwage_ci* ///
	year, pstyle(p1 p1 p1 p2 p2 p2) legend(off) graphregion(color(white)) /// 
	title("Log(mean(incwage)) vs mean(log(incwage))") ///
	legend(order(1 4) label(1 "log(mean(incwage))") label(4 "mean(log(incwage))")) lpattern("1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/meanoflog_vs_logofmean.eps", replace

twoway line lincwage_college_beta lincwage_college_ci* ///
	lincwage_nc_beta lincwage_nc_ci* year, pstyle(p1 p1 p1 p2 p2 p2) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4) label(1 "college") label(4 "no college")) lpattern("1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lincwage_college_vs_nc.eps", replace


*controls
twoway line lincwage_cbsafe_beta lincwage_cbsafe_ci* /// 
	lincwage_college_cbsafe_beta lincwage_college_cbsafe_ci* ///
	lincwage_nc_cbsafe_beta lincwage_nc_cbsafe_ci* year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3) graphregion(color(white)) /// 
	title("Elasticity of CBSA fes to population") /// 
	legend(order(1 4 7) label(1 "everyone") label(4 "college") label(7 "no college")) /// 
	lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lincwage_cbsafe.eps", replace

*more controls 
twoway line /// 
	lincwage_college_fs_cbsafe_beta lincwage_college_fs_cbsafe_ci* ///
	lincwage_nc_fs_cbsafe_beta lincwage_nc_fs_cbsafe_ci* year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3) graphregion(color(white)) /// 
	title("Elasticity of CBSA fes to population, full set of controls") /// 
	legend(order(1 4) label(1 "college") label(4 "no college")) /// 
	lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lincwage_fs_cbsafe.eps", replace

*more skill groups 
twoway line lincwage1_beta lincwage1_ci* /// 
	lincwage2_beta lincwage2_ci* ///
	lincwage3_beta lincwage3_ci* ///
	lincwage4_beta lincwage4_ci* ///
	lincwage5_beta lincwage5_ci* ///
	year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3 p4 p4 p4 p5 p5 p5 ) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4 7 10 13) label(1 "< HS") label(4 "HS") label(7 "some college") /// 
		label(10 "college") label(13 "graduate degree")) ///
		lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-" "1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lincwage_edcuccat.eps", replace





*Dynamism graphs 
twoway line lsr_beta lsr_ci* lfer_beta lfer_ci* ///
	year, pstyle(p1 p1 p1 p2 p2 p2) graphregion(color(white)) /// 
	title("Elasticity of firm start up and exit rate to city size") ///
	legend(order(1 4) label(1 "firm start up rate") label(4 "firm exit rate")) lpattern("1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/sr_fer.eps", replace



twoway line lesr_beta lesr_ci* leer_beta leer_ci* ///
	year, pstyle(p1 p1 p1 p2 p2 p2) graphregion(color(white)) /// 
	title("Elasticity of establishment start up and exit rate to city size") ///
	legend(order(1 4) label(1 "establishment start up rate") label(4 "establishment exit rate")) lpattern("1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/esr_eer.eps", replace


twoway line ljcr_beta ljcr_ci* ljdr_beta ljdr_ci* ///
	year, pstyle(p1 p1 p1 p2 p2 p2) graphregion(color(white)) /// 
	title("Elasticity of job creation and destruction rate to city size") ///
	legend(order(1 4) label(1 "job creation rate") label(4 "job destruction rate")) lpattern("1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/jcr_jdr.eps", replace








