
cap log close
clear
set more off

global mergeddata "/Users/hannah/Dropbox/dynamism_citysize/tasks/merge_cbsa_data/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/dispersion"

use "$mergeddata/sr_cbsa.dta", clear

drop lq*
drop job_*
drop sh_emp*
drop if year==.

**skill premium 
gen sk_prem = incwage_college / incwage_nc
gen sk_prem_res = incwage_c_res / incwage_nc_res
gen sk_prem_fs_res = incwage_c_fs_res / incwage_nc_fs_res
gen sk_prem_bysk_res = incwage_byskc_res / incwage_bysknc_res
local sklist sk_prem sk_prem_res sk_prem_fs_res sk_prem_bysk_res CtoH2


replace sr = sr*100

local incvars incwage incwage_college incwage_nc ///
	incwage_res incwage_fs_res incwage_byskc_res incwage_bysknc_res ///
	incwage_c_res incwage_nc_res incwage_c_fs_res incwage_nc_fs_res pipc_bea 
	
keep `incvars' `sklist' lpop pop wapop sr wapopgr_5yra sh_topcoded cbsa year
reshape wide `incvars' `sklist' lpop pop wapop sr wapopgr_5yra sh_topcoded, i(cbsa) j(year)
	
forvalues yr = 1980/2013 {
	local tm1 = `yr'-1
	local tp1 = `yr'+1
	gen sr_3yma`yr' = (sr`yr' + sr`tm1' + sr`tp1')/3
}
forvalues yr = 1980/2003 {
	local tp10 = `yr' + 10
	gen fdl10_sr`yr' = sr_3yma`tp10' - sr_3yma`yr'
}

local vars `incvars' `sklist'
local years 1980 1990 // 2000 
foreach v of local incvars {
	foreach y of local years {
		local tp10 = `y' + 10
		gen fdl10_`v'`y' = (log(`v'`tp10') - log(`v'`y'))/10*100
		gen agr10_`v'`y' = ((`v'`tp10'/`v'`y')^(1/10)-1)*100
	}
	gen fdl10_`v'2000 = (log(`v'2007) - log(`v'2000))/7*100
	gen agr10_`v'2000 = ((`v'2007/`v'2000)^(1/7)-1)*100
	
	
	local vars 	`vars'  fdl10_`v' agr10_`v'
	
	*gen agr0780_`v' = (`v'2007 / `v'1980)^(1/27)-1

	
}

local years 1980 1990 2000 
foreach v of local sklist {
	foreach y of local years {
		local tp10 = `y' + 10
		gen fdl10_`v'`y' = (`v'`tp10') - (`v'`y')
	}
	local vars 	`vars'  fdl10_`v' 

}


keep cbsa  *1980 *1990 *2000 *2005 *2007 *2008 *2009 *2010 *2011 *2012 *2012 *2013 *2014 *2015
	
reshape long `vars'  ///
	pop lpop wapop sr sr_3yma fdl10_sr wapopgr_5yra sh_topcoded ///
	, i(cbsa ) j(year 1980 1990 2000 2005 2007 2008 2009 2010 2011 2012 2013 2014 2015)

gen lsr_3yma = log(sr_3yma)

**regressions 
/*
I want to do three set of regressions 
1. evaluate the correlation between the level of income and dynamism 
2. evaluate the correlation between future growth rates of income and dynamism 
3. evaluate the correlation between fugure growth rates of income and future decline in dynamism 

For ease I'm going to create a separate table for each measure of income 
incwage
incwage_res
incwage_fs_res
incwage_byskc_res
incwage_bysknc_res
incwage_c_res
incwage_nc_res
incwage_c_fs_res
incwage_nc_fs_res
pipc_bea
*/

keep if inlist(year,1980,1990,2000, 2007,2013)
foreach v of local incvars {

gen l`v' =log(`v')

*table 1
eststo clear 
eststo: reg l`v' i.year i.year#c.lpop
eststo: reg l`v' i.year i.year#c.lsr
eststo: reg l`v' i.year i.year#c.lpop  i.year#c.lsr
esttab using "$ctask/tables/table1_`v'.tex", se r2 ///
	 title("log(`v') vs  dynamism, population") star(* .1 ** .05 *** .01) /// 
	 keep(*lpop* *lsr*) label replace mtitle
	

*table 2
eststo clear 
eststo: reg agr10_`v' i.year i.year#c.lpop
eststo: reg agr10_`v' i.year i.year#c.lincwage
eststo: reg agr10_`v' i.year i.year#c.lsr
eststo: reg agr10_`v' i.year i.year#c.lpop i.year#c.lincwage i.year#c.lsr
esttab using "$ctask/tables/table2_`v'.tex", se r2 ///
	 title("annualized growth rates of `v' vs initial income, dynamism, population") star(* .1 ** .05 *** .01) /// 
	 keep(*lpop* *lincwage* *lsr*) label replace mtitle

*table 3
eststo clear 
eststo: reg agr10_`v' i.year i.year#c.lpop
eststo: reg agr10_`v' i.year i.year#c.lincwage
eststo: reg agr10_`v' i.year i.year#c.fdl10_sr
eststo: reg agr10_`v' i.year i.year#c.lpop i.year#c.lincwage i.year#c.fdl10_sr
esttab using "$ctask/tables/table3_`v'.tex", se r2 ///
	 title("annualized growth rates of `v' vs initial income, change in dynamism, population") star(* .1 ** .05 *** .01) /// 
	 keep(*lpop* *lincwage* *fdl10_sr*) label replace mtitle

	

} 
	
	


local sklist sk_prem sk_prem_res sk_prem_fs_res sk_prem_bysk_res
foreach v of local sklist {
eststo clear
eststo: reg `v' i.year i.year#c.lpop 
eststo: reg `v' i.year i.year#c.lsr
eststo: reg `v' i.year i.year#c.lpop i.year#c.lsr 
esttab using "$ctask/tables/table4_`v'.tex", se r2 ///
	 title("skill premium `v' vs dynamism, population") star(* .1 ** .05 *** .01) /// 
	 keep(*lpop*  *lsr*) label replace 

}

	

*regressions for new note
eststo clear
eststo: reg lsr i.year i.year#c.wapopgr
eststo: reg lsr i.year i.year#c.lpop
eststo: reg lsr i.year i.year#c.sk_prem
eststo: reg lsr i.year i.year#c.lpop i.year#c.sk_prem
eststo: reg lsr i.year i.year#c.wapopgr i.year#c.lpop i.year#c.sk_prem 

esttab using "$ctask/tables/table1_1218.tex", se r2 ///
	 title("firm start up rate vs pop gr, pop, skill premium") star(* .1 ** .05 *** .01) /// 
	 keep(*lpop*  *wapopgr* *sk_prem*) label replace 
	
eststo clear 
eststo: reg CtoH2 i.year i.year#c.lpop
eststo: reg sk_prem i.year i.year#c.lpop 

esttab using "$ctask/tables/table2_1218.tex", se r2 ///
	 title("skill premium and skill intensity vs. city size") star(* .1 ** .05 *** .01) /// 
	 keep(*lpop* ) label replace 

eststo clear 
eststo: reg fdl10_sr i.year i.year#c.lpop
eststo: reg fdl10_sk_prem i.year i.year#c.lpop 
eststo: reg fdl10_CtoH2 i.year i.year#c.lpop 


esttab using "$ctask/tables/table3_1218.tex", se r2 ///
	 title("change in sr, skill premium, skill intensity vs. initial city size") star(* .1 ** .05 *** .01) /// 
	 keep(*lpop* ) label replace 

	
