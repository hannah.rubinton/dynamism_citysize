
cap log close
clear
set more off

global acs "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_acs/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/dispersion"
global census "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_seer/output"

use "$acs/acs_cbsa_alt_spec.dta", clear

merge 1:1 year cbsa using "$census/cbsa_pop.dta", nogen

gen lpop = log(pop)


gen lincwage_college = log(incwage_college) 
gen lincwage_nc = log(incwage_nc)

local vars lincwage1 lincwage2 lincwage3 lincwage4 lincwage5 /// 
	lincwage_college lincwage_nc  lincwage ///
	lincwage_cbsafe lincwage_college_cbsafe lincwage_nc_cbsafe ///
	lincwage_fs_cbsafe lincwage_college_fs_cbsafe lincwage_nc_fs_cbsafe 
	
foreach v of local vars {
	reg `v' i.year i.year#c.lpop
	matrix `v'pop = e(b) 
	matrix `v'popV = e(V)
}



keep year
bys year: keep if _n==1
levelsof year, local(years)
foreach v of local vars {
	gen `v'_beta = .
	gen `v'_ci_lb = .
	gen `v'_ci_ub = .
	
foreach yr of local years {
	replace `v'_beta = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lpop")] if year==`yr'
	replace `v'_ci_lb = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lpop")] - 1.96*sqrt(`v'popV[rownumb(`v'popV,"`yr'.year#c.lpop"),colnumb(`v'popV,"`yr'.year#c.lpop")]) if year==`yr'
	replace `v'_ci_ub = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lpop")] + 1.96*sqrt(`v'popV[rownumb(`v'popV,"`yr'.year#c.lpop"),colnumb(`v'popV,"`yr'.year#c.lpop")]) if year==`yr'

}

}



	
*acs plots	
twoway line lincwage_beta lincwage_ci* year, pstyle(p1 p1 p1) legend(off) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") lpattern("1" "-" "-")


twoway line lincwage_college_beta lincwage_college_ci* ///
	lincwage_nc_beta lincwage_nc_ci* year, pstyle(p1 p1 p1 p2 p2 p2) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4) label(1 "college") label(4 "no college")) lpattern("1" "-" "-" "1" "-" "-")


*controls industry dummies
twoway line /// 
	lincwage_college_cbsafe_beta lincwage_college_cbsafe_ci* ///
	lincwage_nc_cbsafe_beta lincwage_nc_cbsafe_ci* year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3) graphregion(color(white)) /// 
	title("Elasticity of CBSA fes to population, industry controls") /// 
	legend(order(1 4) label(1 "college") label(4 "no college")) /// 
	lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lincwage_fs_ind_cbsafe.eps", replace

*more controls  occ dummies
twoway line  /// 
	lincwage_college_fs_cbsafe_beta lincwage_college_fs_cbsafe_ci* ///
	lincwage_nc_fs_cbsafe_beta lincwage_nc_fs_cbsafe_ci* year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3) graphregion(color(white)) /// 
	title("Elasticity of CBSA fes to population, occupation controls") /// 
	legend(order(1 4) label(1 "college") label(4 "no college")) /// 
	lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lincwage_fs_occ_cbsafe.eps", replace

*more skill groups 
twoway line lincwage1_beta lincwage1_ci* /// 
	lincwage2_beta lincwage2_ci* ///
	lincwage3_beta lincwage3_ci* ///
	lincwage4_beta lincwage4_ci* ///
	lincwage5_beta lincwage5_ci* ///
	year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3 p4 p4 p4 p5 p5 p5 ) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4 7 10 13) label(1 "< HS") label(4 "HS") label(7 "some college") /// 
		label(10 "college") label(13 "graduate degree")) ///
		lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-" "1" "-" "-" "1" "-" "-")










