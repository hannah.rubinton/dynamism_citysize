

cap log close
clear
set more off


global LAUS "/Users/hannah/Google Drive File Stream/My Drive/rawdata/LAUS"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_laus/output"


/*****************
Program cleans labor force data
state data from: http://www.bls.gov/lau/rdscnp16.htm
(had to clean it a bit in excel and save as csv)

cbsa data from: http://www.bls.gov/lau/metrossa.htm


*some of the cbsa codes are necta codes. Can't replace with 
cbsa codes because there's not a unique mapping. Tried to make a 
crosswalk from county to necta but counties do not map uniquely into nectas
while cbsa's are created st counties map uniquely to nectas
*******************/

****cbsa
import delimited  using "$LAUS/ssamatab1.csv", clear varnames(4) rowrange(6:)

rename v3 cbsa
rename rate urate
rename laborforce clf
rename employment cemp
drop unemployment code fipscode month 

destring  clf cemp, replace ignore(",")

collapse clf cemp urate, by(cbsa year)  //collapse to get yearly averages


sort cbsa year 
by cbsa: gen clfgr = (clf[_n]-clf[_n-1])/clf[_n-1]*100
by cbsa: gen cempgr = (cemp[_n]-cemp[_n-1])/cemp[_n-1]*100

save "$data/laus_cbsa.dta", replace




****State
import excel  using "$LAUS/staadata.xlsx", clear cellrange(A9:J2181)
drop if strlen(A)>2
rename A statefip
rename C year
rename D clf
destring year, replace
collapse clf  , by( year)  //collapse to get yearly averages


sort year 
gen clfgr = (clf[_n]-clf[_n-1])/clf[_n-1]*100
gen clfgr5yra = ((clf[_n+2]/clf[_n-2])^(1/5)-1)*100

save "$data/laus_agg_clf.dta", replace





*****State
import excel using "$LAUS/staadata.xlsx", clear 
rename A state
rename B statename
rename C year
rename E clf
rename G cemp

keep  state statename year clf cemp 
keep if _n>=9

drop if length(state)>2 //drop cities 
destring state year clf cemp, replace

sort state year 
by state: gen clfgr = (clf[_n]-clf[_n-1])/clf[_n-1]*100
by state: gen cempgr = (cemp[_n]-cemp[_n-1])/cemp[_n-1]*100

save "$data/laus_state.dta", replace
