
%%try making H and L bigger in the 'experiment' if city 1 gets bigger but
%%city does not then you are going to have trouble matching the fact that
%%the city size premium didn't increase for low skill --- at least I think.


%set up parameters 
usecluster=0;
if usecluster ==1
    addpath('/home/hherman/matlab/compecon/CEtools')
    addpath('/scratch/network/hherman/lightspeed')
end
pm.sig=2.7;
pm.rho=1.5;
pm.gmH = [1.1,1];
pm.gmL = [1.1,1];
pm.psiH=[1,1];
pm.psiL=[0,0];
pm.delta=[.02,.02];

pm.lambda = [3.4,3.4];
pm.zeta = [1,1];
pm.alpha = [4,4];
pm.theta = .9;
pm.betaH = .7;
pm.betaL = .7;
pm.Ah1 = 1;
pm.Al1 = 1;
pm.eta = [4,4];
pm.feh = [0,0];
pm.fel = [0,0];
pm.fey = [.1,.1];
pm.fcl = [0,0];
pm.fch = [0,0];
pm.fcy = [.1,.1];
pm.fb = [1,1];
pm.H = .24;
pm.L = 1;



%% calibrate 1980 
pm.year=1980;
dif  = calibrate_bv_tc([.8,.8,.8,.8,1,1],pm)
%    if usecluster 
    %    matlabpool SGE3 64
%        clus=parcluster
%        parpool(clus)

%    else
%        matlabpool local
%    end
funcal = @(p) calibrate_bv_tc(p,pm)
%pout = broydenp(funcal,[1,1,1,1,1,1])
pout = fminunc(funcal,[.9,.9,.9,.9,1,1])
%    poolobj=gcp('nocreate');
%    delete(poolobj);   

pm.gmH = [1,.9];
pm.gmL = [.95,.90];
%pm.gmH = pm.gmH*1.1;
%  pm.gmH(1)= pm.gmH(1)*1.1
% pm.gmL(1)= pm.gmL(1)*1.1
% pm.gmH = [1,1];
% pm.gmL = [1,1];

 

%% solve for equilibrium 
%fun([.6,.6,1,1,1,1])
% fun = @(x) sum(eq_bv_tc(x,pm).^2)
% lb=[.1,.1,.01,.01];
% ub=[.29,.98,10,10]
% eq=fmincon(fun,[.25,.9,.2,.04],[],[],[],[],lb,ub)

% fun = @(x) eq_bv_tc(x,pm)
% 
% eq = fsolve(fun,[.2;.7])
% h1=eq(1);
% l1=eq(2);
% 
% 
% 
% 
% [out] = bv_tc(h1,l1,pm)
% 

%pm.psiH=[.5,.5];
%pm.psiL=[-.2,-.2];

%% calibration 
run_calibrate_solve_city

%% 1980 with calibrated moments 
pm.gmH = [city1_1980(1),city2_1980(1)];
pm.gmL = [city1_1980(2),1];
pm.fey = [city1_1980(3), city2_1980(2)];


wh = [city1_1980_wages(1),city2_1980_wages(1)];
wl = [city1_1980_wages(2),city2_1980_wages(2)];

l(1) = .78;
h(1) = l(1)*.26;
l(2) = 1-l(1);
h(2) = l(2)*.17;
pm.H = sum(h);
pm.L = sum(l);
c1 = pm.zeta(1)*(h(1)+l(1))^pm.alpha(1);
c2 = pm.zeta(2)*(h(2)+l(2))^pm.alpha(2);
pm.Ah1 = (log(wh(2)) - c2 + c1 - log(wh(1)));
pm.Al1 = (log(wl(2)) - c2 + c1 - log(wl(1)));

[out_1980] = bv_tc(h(1),l(1),pm)
out_1980.wh(1)/out_1980.wl(2)
out_1980.wh(2)/out_1980.wl(2)
out_1980.wl(1)/out_1980.wl(2)
out_1980.wl(2)/out_1980.wl(2)

pm_1980 = pm;

%% 2005 with calibrated moments 
pm.gmH = [city1_2005(1),city2_2005(1)];
pm.gmL = [city1_2005(2),city2_2005(2)];
pm.fey = [city1_2005(3), city2_2005(3)];
wh = [city1_2005_wages(1),city2_2005_wages(1)];
wl = [city1_2005_wages(2),city2_2005_wages(2)];

l(1) = .90;
h(1) = l(1)*.51;
l(2) = 1.19-l(1);
h(2) = l(2)*.28;
pm.H = sum(h);
pm.L = sum(l);
c1 = pm.zeta(1)*(h(1)+l(1))^pm.alpha(1);
c2 = pm.zeta(2)*(h(2)+l(2))^pm.alpha(2);
pm.Ah1 = (log(wh(2)) - c2 + c1 - log(wh(1)));
pm.Al1 = (log(wl(2)) - c2 + c1 - log(wl(1)));

[out_2005] = bv_tc(h(1),l(1),pm)

out_2005.wh(1)/out_1980.wl(2)
out_2005.wh(2)/out_1980.wl(2)
out_2005.wl(1)/out_1980.wl(2)
out_2005.wl(2)/out_1980.wl(2)

pm_2005 = pm; 


%% skill neutral shock to city 1 

pm.gmH(1) = pm.gmH(1).*1.1
pm.gmL(1) = pm.gmL(1).*1.1

fun = @(x) eq_bv_tc(x,pm)
eq = fsolve(fun,[h(1);l(1)])
h1=eq(1);
l1=eq(2);

[out_skneutralshock] = bv_tc(h1,l1,pm)



%% amentities shock
pm = pm_2005;
pm.Ah1 = pm_2005.Ah1*1.1;
pm.Al1 = pm_2005.Al1*1.1;


fun = @(x) eq_bv_tc(x,pm)

eq = fsolve(fun,[h(1);l(1)])
h1=eq(1);
l1=eq(2);

[out_amenities] = bv_tc(h1,l1,pm)



%% shock amenities, entry costs and labor supply almost as in 
%data but hold productivity fixed 
pm = pm_1980;
pm.H = pm_2005.H;
pm.L = pm_2005.L;
pm.Ah1 = pm.Ah1*4;
pm.Al1 = pm.Al1*4;
pm.fey = pm.fey*3;

fun = @(x) eq_bv_tc(x,pm)

eq = fsolve(fun,[h(1);l(1)])
h1=eq(1);
l1=eq(2);

[out_holdprod1980] = bv_tc(h1,l1,pm)



