

%% Calibration
%{
calibrate city 2 first. gmL(2) is normalized to 1 and so is wl2=1 
1. look for gmH(2) that gives wh2/wl2 = 1.54
2. look for the gmH(1) and gmL(1) that give the wh1/wl2 and wl1/wl2 given
the wl2 solved for in step 1
3. then repeat step 2 for city 1 and 2 in 2005. This time the wages are
normalized relative to wl2 in 1980 so we are looking for 4 gammas 

%}


%city 2 1980 
funsc = @(in) alt_calibrate_solve_city(in,1980,2,pm)
city2_1980 = fminsearch(funsc,[.6;.05])
[~,city2_1980_wages]=funsc(city2_1980)
pm.wl2 = city2_1980_wages(2); 

%city 1 1980 
funsc = @(in) alt_calibrate_solve_city(in,1980,1,pm)
city1_1980 = fminsearch(funsc,[.6,1,.15])
[~,city1_1980_wages]=funsc(city1_1980)


%city 1 2005
funsc = @(in) alt_calibrate_solve_city(in,2005,1,pm)
city1_2005 = fminsearch(funsc,[.9,.8,.35])
[~,city1_2005_wages]=funsc(city1_2005)

%city 2 2005
funsc = @(in) alt_calibrate_solve_city(in,2005,2,pm)
city2_2005 = fminsearch(funsc,[.8,1,.1])
[~,city2_2005_wages]=funsc(city2_2005)
