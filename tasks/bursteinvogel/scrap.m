% %labor mkt clearing across cities 
% H2 = pm.H - H1;
% L2 = pm.L - L1; 
% 
% %utility equalization 
% %congestion 
% c1 = pm.zeta(1)*(H1+L1)^pm.alpha(1);
% c2 = pm.zeta(2)*(H2+L2)^pm.alpha(2);
% u = @(c) (c^(1-pm.theta))/(1-pm.theta);
% %worker utility 
% uh1 = pm.Ah1*u(wh1) - c1;
% ul1 = pm.Al1*u(wl1) - c1;
% wh2 = ((uh1 + c2)*(1-pm.theta))^(1/(1-pm.theta)); 
% wl2 = ((ul1 + c2)*(1-pm.theta))^(1/(1-pm.theta)); 

% %cobb douglas
% uh1 = pm.Ah1*(pm.betaH*wh1)^pm.betaH*((1-pm.betaH)*(wh1/r1))^(1-pm.betaH);
% ul1 = pm.Al1*(pm.betaL*wl1)^pm.betaL*((1-pm.betaL)*(wl1/r1))^(1-pm.betaL);
% wh2 = uh1/(pm.betaH^pm.betaH*(1-pm.betaL)^(1-pm.betaL)*r2^(pm.betaH-1));
% wl2 = ul1/(pm.betaL^pm.betaL*(1-pm.betaL)^(1-pm.betaL)*r2^(pm.betaL-1));

%  funp = @(z) pc(z).^(1-pm.sig).*g(z)/(zstar^(-pm.lambda(j)));
%  M = integral(funp,zstar,Inf)^-1;

% %average revenue by integrating the revenue function 
rev1 = @(z) (ucf1(z)./ucf1(zstar1)).^(1-pm.sig).*pm.sig.*FC1;
rev2 = @(z) (ucf2(z)./ucf2(zstar2)).^(1-pm.sig).*pm.sig.*FC2;

%get M from building mkt clearing 
% % M1 = r1^(1/(pm.eta(1)-1))/pm.eta(1)/pm.fb(1);
% % M2 = r2^(1/(pm.eta(2)-1))/pm.eta(2)/pm.fb(1);
 



% cons1 = pm.betaH*wh1*H1 + pm.betaL*wl1*L1;
% cons2 = pm.betaH*wh2*H2 + pm.betaL*wl2*L2; 
% BP1 = r1*r1^(1/(pm.eta(1)-1)) - r1^(pm.eta(1)/(pm.eta(1)-1)); %building profits - these will be zero 
% BP2 = r2*r2^(1/(pm.eta(2)-1)) - r2^(pm.eta(2)/(pm.eta(2)-1));
% BC1 = r1^(pm.eta(1)/(pm.eta(1)-1)); %cost of builidng should be paid in final good 
% BC2 = r2^(pm.eta(2)/(pm.eta(2)-1));
 pi_avg1 = Piavg1(zstar1);
 pi_avg2 = Piavg2(zstar2);
 rev_avg1 = integral(@(z)rev1(z).*g1(z),zstar1,Inf)/(zstar1^(-pm.lambda(1)));
 rev_avg2 = integral(@(z)rev2(z).*g2(z),zstar2,Inf)/(zstar2^(-pm.lambda(2)));
 
 
 
 % M1 = (cons1 + BP1 + BC1)/(rev_avg1  - pm.fcy(1) - pm.fey(1)*pm.delta/(zstar1^(-pm.lambda(1))) - pi_avg1);
% M2 = (cons2 + BP2 + BC2)/(rev_avg2  - pm.fcy(2) - pm.fey(2)*pm.delta/(zstar2^(-pm.lambda(2))) - pi_avg2);
%Y1test = rev_avg1*M1 - cons1 - BP1 - BC1 - M1*pi_avg1 - M1*pm.fcy(1) - pm.delta/(zstar1^(-pm.lambda(1)))*pm.fey(1)

% M1 = (wh1*H1 + wl1*L1)/(rev_avg1);
% M2 = (wh2*H2 + wl2*L2)/(rev_avg2); 
%   funp1 = @(z) pc1(z).^(1-pm.sig).*g1(z)/(zstar1^(-pm.lambda(1)))*M1;
%   P1 = integral(funp1,zstar1,Inf)^(1/(1-pm.sig));
%   funp2 = @(z) pc2(z).^(1-pm.sig).*g2(z)/(zstar2^(-pm.lambda(2)))*M2;
%   P2 = integral(funp2,zstar2,Inf)^(1/(1-pm.sig)); 
% 

%calculate M from one fo the market clearing conditions 


% % hicksian demand 
% lamh1 = @(z)pm.gmH(1).*(z).^(pm.psi*(pm.rho-1)/pm.rho);
% lamh2 = @(z)pm.gmH(2).*(z).^(pm.psi*(pm.rho-1)/pm.rho);
% hq1_dMsq = @(z)(lamh1(z)./wh1.*ucf1((z))).^pm.rho*rev_avg1.*pc1(z).^(-pm.sig).*g1(z)/(zstar1^(-pm.lambda(1)));
% a = integral(hq1_dMsq,zstar1,Inf);
% b = pm.fch(1) + pm.feh(1)*pm.delta/(zstar1^(-pm.lambda(1)));
% c = -H1;
% M1 = (-b + sqrt(b^2 - 4*a*c))/2/a;
% 
% hq2_dMsq = @(z)(lamh2(z)./wh2.*ucf2((z))).^pm.rho*rev_avg2.*pc2(z).^(-pm.sig).*g2(z)/(zstar2^(-pm.lambda(2)));
% a = integral(hq2_dMsq,zstar2,Inf);
% b = pm.fch(2) + pm.feh(2)*pm.delta/(zstar2^(-pm.lambda(1)));
% c = -H2;
% M2 = (-b + sqrt(b^2 - 4*a*c))/2/a;
% 



% out.Bs1 = r1^(1/(pm.eta(1)-1));
% out.Bs2 = r2^(1/(pm.eta(2)-1));
% out.Bd1 = M1*pm.fb(1) + H1*wh1/r1*(1-pm.betaH) + L1*wl1/r1*(1-pm.betaL);
% out.Bd2 = M2*pm.fb(2) + H2*wh2/r2*(1-pm.betaH) + L2*wl2/r2*(1-pm.betaL);

%agg Y
out.Ys1 = integral(@(z) q1(z).^((pm.sig-1)/pm.sig).*g1(z)*M1/(zstar1^(-pm.lambda(1))),zstar1,Inf)^(pm.sig/(pm.sig-1));
out.Ys2 = integral(@(z) q2(z).^((pm.sig-1)/pm.sig).*g2(z)*M2/(zstar2^(-pm.lambda(2))),zstar2,Inf)^(pm.sig/(pm.sig-1));

out.Yd1 = (wh1*H1 + wl1*L1);
out.Yd2 = (wh2*H2 + wl2*L2);