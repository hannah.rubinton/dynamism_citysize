function [out] = bv_tc(h1,l1,pm)

%optsfs = optimoptions(@fsolve,'Display','off','Diagnostics','off');
options = optimoptions(@fsolve,'Display','final','Diagnostics','off','FunctionTolerance',1e-10,'StepTolerance',1e-10);
wlb = [.001,.001];
wub = [10,10];

%solve eq in city 1 
%solve for wages in city 1
funw = @(in)(solve_city(in(1),in(2),h1,l1,in(3),1,pm));
%eqwages1 = fmincon(funw,[1,.5],[],[],[],[],wlb,wub,[],options);
eqwages1 = fsolve(funw,[1.2,.8,1],options);
wh1 = eqwages1(1);
wl1 = eqwages1(2);
r1 = eqwages1(3);
[excess_demand1,cout1] = solve_city(wh1,wl1,h1,l1,r1,1,pm)

h2 = pm.H - h1;
l2 = pm.L - l1;


%solve eq in city 2 
%solve for wages in city 2
funw = @(in)(solve_city(in(1),in(2),h2,l2,in(3),2,pm));
%eqwages2 = fmincon(funw,[1,.5],[],[],[],[],wlb,wub,[],options);
eqwages2 = fsolve(funw,[1.2,.8,1],options);
wh2 = eqwages2(1);
wl2 = eqwages2(2);
r2 = eqwages2(3);
[excess_demand2,cout2] = solve_city(wh2,wl2,h2,l2,r2,2,pm)


%congestion 
c1 = pm.zeta(1)*(h1+l1)^pm.alpha(1);
c2 = pm.zeta(2)*(h2+l2)^pm.alpha(2);
u = @(c) (c^(1-pm.theta))/(1-pm.theta);
u = @(c) log(c);
out.Uh1 = pm.Ah1 + u(wh1) - c1;
out.Ul1 = pm.Al1 + u(wl1) - c1;
out.Uh2 = u(wh2) - c2;
out.Ul2 = u(wl2) - c2;



out.Bd1 = cout1.M*pm.fb(1);
out.Bd2 = cout2.M*pm.fb(2);
out.Bs1 = r1^(1/(pm.eta(1)-1));
out.Bs2 = r2^(1/(pm.eta(2)-1));

out.H1 = h1;
out.H2 = h2;
out.L1 = l1;
out.L2 = l2;

out.M = [cout1.M, cout2.M]; 
out.wh = [wh1, wh2];
out.wl = [wl1,wl2];
out.r = [r1,r2];
out.zstar = [cout1.zstar, cout2.zstar];
out.gmh_avg = [cout1.gmh_avg, cout2.gmh_avg];
out.gml_avg = [cout1.gml_avg, cout2.gml_avg];

Me1 = cout1.M*pm.delta(1)/(cout1.zstar^(-pm.lambda(1)))
Me2 = cout2.M*pm.delta(2)/(cout2.zstar^(-pm.lambda(2)))

out.sr1 = Me1/cout1.M;
out.sr2 = Me2/cout2.M;
out.pexit1 = (cout1.M*pm.delta(1) + Me1*(1-cout1.zstar^(-pm.lambda(1))))/(cout1.M+Me1);
out.pexit2 = (cout2.M*pm.delta(2) + Me2*(1-cout2.zstar^(-pm.lambda(2))))/(cout2.M+Me2);

out.cityszpremia = [out.wh(1)/out.wh(2), out.wl(1)/out.wl(2)];
out.citysz = [h1+l1, h2+l2];
out.skpremia = [out.wh(1)/out.wl(1), out.wh(2)/out.wl(2)];
out.skintensity = [h1/l1, h2/l2];

end
