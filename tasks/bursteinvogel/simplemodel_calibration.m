%calibrate simple model 


pm.sig=2.7;
pm.rho=1.5;
pm.psiH=.5;
pm.psiL=-.2;
pm.delta=[.02,.02];

pm.lambda = [3.5,3.5];
pm.zeta = [1,1];
pm.alpha = [4,4];
pm.theta = .9;
pm.betaH = .7;
pm.betaL = .7;
pm.Ah1 = 1;
pm.Al1 = 1;
pm.eta = [4,4];
pm.feh = [0,0];
pm.fel = [0,0];
pm.fey = [.1,.1];
pm.fcl = [0,0];
pm.fch = [0,0];
pm.fcy = [.1,.1];
pm.fb = [1,1];



%calibrate 1980 
wh1_wl1 = 1.55;
wh2_wl2 = 1.54;
wh1_wh2 = 1.21;
wl1_wl2 = 1.46;
HL1 = .26;
HL2 = .17;
l1 = .78;
h1 = l1*HL1;
l2 = 1-l1;
h2 = l2*HL2;

gml2_1980 = 1;
%from eqn for sk premium we immediateley get gmh2
gmh2_1980 = wh2_wl2*(h2/l2)^(1/pm.sig);
%Y2 from productin function 
Y2 = (gmh2_1980*h2^((pm.sig-1)/pm.sig)+l2^((pm.sig-1)/pm.sig))^(pm.sig/(pm.sig-1));
%use market clearing to solve for wages 
wl2 = Y2 / (l2 + wh2_wl2*h2);
wh2 = wh2_wl2*wl2;

%use data on citysize wg premium to solve for wl1 wh1
wl1 = wl1_wl2*wl2;
wh1 = wh1_wh2*wh2;

%wages to caculate Y1 
Y1 = wh1*h1 + wl1*l1;
gmh1_gml1 = wh1_wl1*(h1/l1)^(1/pm.sig); 
%use Y to solve for gml1
gml1_1980 =  Y1^((pm.sig-1)/pm.sig) / (l1^((pm.sig-1)/pm.sig)+gmh1_gml1*h1^((pm.sig-1)/pm.sig));
gmh1_1980 = gmh1_gml1*gml1_1980;

%amenities 
c1 = pm.zeta(1)*(h1+l1)^pm.alpha(1);
c2 = pm.zeta(2)*(h2+l2)^pm.alpha(2);
Ah1_1980 = log(wh2) - c2 + c1 - log(wh1);
Al1_1980 = log(wl2) - c2 + c1 - log(wl1);




%% calibrate 2005
wh1_2005 = 2.61*wl2;
wh2_2005 = 1.79*wl2;
wl1_2005 = 1.21*wl2;
wl2_2005 = 1.04*wl2;
HL1 = .51;
HL2 = .28;
l1 = .90;
h1 = l1*.51;
l2 = 1.19-l1;
h2 = l2*.28;

Y1 = wh1_2005*h1 + wl1_2005*l1;
Y2 = wh2_2005*h2 + wl2_2005*l2; 
gmh1_gml1 = (wh1_2005/wl1_2005)*(h1/l1)^(1/pm.sig); 
gmh2_gml2 = (wh2_2005/wl2_2005)*(h2/l2)^(1/pm.sig); 

%use Y to solve for gml1
gml1_2005 =  Y1^((pm.sig-1)/pm.sig) / (l1^((pm.sig-1)/pm.sig)+gmh1_gml1*h1^((pm.sig-1)/pm.sig));
gmh1_2005 = gmh1_gml1*gml1_2005;
%use Y to solve for gml2
gml2_2005 =  Y2^((pm.sig-1)/pm.sig) / (l2^((pm.sig-1)/pm.sig)+gmh2_gml2*h2^((pm.sig-1)/pm.sig));
gmh2_2005 = gmh2_gml2*gml2_2005;


%amenities 
c1 = pm.zeta(1)*(h1+l1)^pm.alpha(1);
c2 = pm.zeta(2)*(h2+l2)^pm.alpha(2);
Ah1_2005 = log(wh2_2005) - c2 + c1 - log(wh1_2005);
Al1_2005 = log(wl2_2005) - c2 + c1 - log(wl1_2005);