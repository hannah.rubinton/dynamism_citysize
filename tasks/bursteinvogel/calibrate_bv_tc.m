function [loss] = calibrate_bv_tc(pin, pm)
%solving two city version of simple model
%parameters 
if any(pin<0)
  loss=Inf;
  return
end

if pm.year == 1980 
    %period 1: 1980
    %moments.grW1980 = 0; 
    moments.WL2_WL2 = 1;
    moments.WH2_WL2 = 1.54;
    moments.WH1_WL2 = 1.86;
    moments.WL1_WL2 = 1.18;
    moments.HL1 = .26;
    moments.HL2 = .17;
	%alternative bc doesn't seem to handle low H well
	moments.HL1 = .4;
	moments.HL2 = .25;

    moments.relativecitysize = 22;
    moments.pexit = [10,10];
    
    pm.gammaL=[1,pin(2)];
    pm.gammaH=[pin(3),pin(4)];
    pm.Ah1 = pin(5);
    pm.Al1 = pin(6);
%    pm.eta = [pin(7); pin(8)];
  % params.al1 = pin(7);
  %  params.al2 = pin(8);
else 
    %period 2: 2005-2007
    %moments.grW1980 =  27; 
    moments.WL2_WL2 = 1.04;
    moments.WH2_WL2 = 1.79;
    moments.WH1_WL2 = 2.61;
    moments.WL1_WL2 = 1.21;
    moments.HL1 = .51;
    moments.HL2 = .28;
    moments.pexit = [12,10];
    moments.relativecitysize = 23;
    
    pm.gammaL=[pin(1),pin(2)];
    pm.gammaH=[pin(3),pin(4)];
    pm.AH1 = pin(5);
    pm.AL1 = pin(6);
 %   pm.eta = [pin(7); pin(8)];

end 


options = optimset('Display','off','Diagnostics','off');
% fun = @(x) eq_bv_tc(x,pm);
% eq=fsolve(fun,[.25;.8;.2;.035],options)
fun = @(x) eq_bv_tc(x,pm)
lb=[.1,.1,.01,.01];
ub=[.29,.98,10,10]
eq=fsolve(fun,[.2,.7,1,.5])

h1 = eq(1);
l1 = eq(2);
r1 = eq(3);
r2 = eq(4);
h2 = pm.H-h1;
l2 = pm.L-l1;
[out] = bv_tc(h1,l1,r1,r2,pm);

if abs(out.Uh1 - out.Uh2) > .01 
   loss = Inf;
   return;
end



if pm.year==1980
    model.WL2_WL2 = out.wl2/out.wl2;
    model.WH2_WL2 = out.wh2/out.wl2;
    model.WH1_WL2 = out.wh1/out.wl2;
    model.WL1_WL2 = out.wl1/out.wl2;
    model.pexit = [out.pexit1, out.pexit1];
else 
  model.WL2_WL2 = out.wl2/pm.wl2;
    model.WH2_WL2 = out.wh2/pm.wl2;
    model.WH1_WL2 = out.wh1/pm.wl2;
    model.WL1_WL2 = out.wl1/pm.wl2;  
    model.pexit = [out.pexit(1), out.pexit(2)];
end
model.HL1 = h1/l1;
model.HL2 = h2/l2;
model.relsz = (h1+l1)/(h2+l2);
avgW = (h1*out.wh1 + h2*out.wh2 + l1*out.wl1 + l2*out.wl2)/(pm.L+pm.H) ;

if pm.year == 1980 
   
    loss = sum([(moments.WH2_WL2 - model.WH2_WL2)^2;
            (moments.WH1_WL2 - model.WH1_WL2)^2;
            (moments.WH1_WL2 - model.WH1_WL2)^2;
            (moments.WL1_WL2 - model.WL1_WL2)^2;
            10*(moments.HL1 - model.HL1)^2;
           10* (moments.HL2 - model.HL2)^2;]);
           % (moments.pexit(1) - model.pexit(1))^2;
           % (moments.pexit(2) - model.pexit(2))^2;]);
           % (moments.relativecitysize - model.relsz)^2]);
   %{
    loss = [(moments.WH2_WL2 - model.WH2_WL2);
            (moments.WH1_WL2 - model.WH1_WL2);
            (moments.WH1_WL2 - model.WH1_WL2);
            (moments.WL1_WL2 - model.WL1_WL2);
            (moments.HL1 - model.HL1);
            (moments.HL2 - model.HL2);];
  %          (moments.pexit(1) - model.pexit(1));
  %          (moments.pexit(2) - model.pexit(2));];
           % (moments.relativecitysize - model.relsz)^2]);
	%}
else 

    %model.grW1980 = (avgW - pm.avgW1980)/pm.avgW1980*100; 
    loss = [(moments.WL2_WL2 - model.WL2_WL2)^2;
            (moments.WH2_WL2 - model.WH2_WL2)^2;
            (moments.WH1_WL2 - model.WH1_WL2)^2;
            (moments.WH1_WL2 - model.WH1_WL2)^2;
            (moments.WL1_WL2 - model.WL1_WL2)^2;
            10*(moments.HL1 - model.HL1)^2;
            10*(moments.HL2 - model.HL2)^2;];
           % (moments.pexit(1) - model.pexit(1))^2;
           % (moments.pexit(2) - model.pexit(2))^2;]);
          %  (moments.relativecitysize - model.relsz)^2]);    

end 



