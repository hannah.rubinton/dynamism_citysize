function excess_demand = calibrate_solve_city(in,year,city,pm)


%moments from the data 1980
if year==1980
    wl = [1.18,1];
    wh = [1.86,1.54];
    l(1) = .78;
    h(1) = l(1)*.26;
    l(2) = 1-l(1);
    h(2) = l(2)*.17;
    c1 = pm.zeta(1)*(h(1)+l(1))^pm.alpha(1);
    c2 = pm.zeta(2)*(h(2)+l(2))^pm.alpha(2);
    Ah1 = (log(wh(2)) - c2 + c1 - log(wh(1)));
    Al1 = (log(wl(2)) - c2 + c1 - log(wl(1)));
end

%moments from the data 1980
if year==2005
    wl = [1.21,1.04];
    wh = [2.61,1.79];
    l(1) = .90;
    h(1) = l(1)*.51;
    l(2) = 1.19-l(1);
    h(2) = l(2)*.28;
    c1 = pm.zeta(1)*(h(1)+l(1))^pm.alpha(1);
    c2 = pm.zeta(2)*(h(2)+l(2))^pm.alpha(2);
    Ah1 = (log(wh(2)) - c2 + c1 - log(wh(1)));
    Al1 = (log(wl(2)) - c2 + c1 - log(wl(1)));
end


pm.gmH(city) = in(1);
pm.gmL(city) = in(2);
r = in(3); 
j=city;


[excess_demand, ~] = solve_city(wh(j),wl(j),h(j),l(j),r,j,pm)




