function [ diff] = eq_bv_tc(x,pm)
   
    h1 = x(1);
    l1 = x(2);


    if h1<=0 || l1<=0 
        diff = [Inf; Inf]
        return;
    end
    
    
    
    
    [out] = bv_tc(h1,l1,pm)
   
    diff = [(out.Uh1 - out.Uh2); 
        (out.Ul1 - out.Ul2)];
   
        
        
end