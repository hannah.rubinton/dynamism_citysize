function [targeterror,wages] = alt_calibrate_solve_city(in,year,city,pm)


%moments from the data 1980
if year==1980
    moments.wl = [1.18,1];
    moments.wh = [1.86,1.54];
    moments.l(1) = .78;
    moments.h(1) = moments.l(1)*.26;
    moments.l(2) = 1-moments.l(1);
    moments.h(2) = moments.l(2)*.17;
    moments.pexit(1) = .13;
    moments.pexit(2) = .13;
end

%moments from the data 2005
if year==2005
    moments.wl = [1.21,1.04];
    moments.wh = [2.61,1.79];
    moments.l(1) = .90;
    moments.h(1) = moments.l(1)*.51;
    moments.l(2) = 1.19-moments.l(1);
    moments.h(2) = moments.l(2)*.28;
    moments.pexit(1) = .12;
    moments.pexit(2) = .10; 
end

if city==1 && year==1980
    pm.gmH(1) = in(1);
    pm.gmL(1) = in(2);
    pm.fey(1) = in(3);
elseif city==2 && year==1980 
    pm.gmH(2) = in(1);
    pm.gmL(2) = 1; 
    pm.fey(2) = in(2);
elseif city==1 && year==2005
    pm.gmH(1) = in(1);
    pm.gmL(1) = in(2);
    pm.fey(1) = in(3);
else 
    pm.gmH(2) = in(1);
    pm.gmL(2) = in(2);
    pm.fey(2) = in(3);
end
 



%solve for wages in city
options = optimoptions(@fsolve,'Display','off','Diagnostics','off','FunctionTolerance',1e-10,'StepTolerance',1e-10);
funw = @(w)(solve_city(w(1),w(2),moments.h(city),moments.l(city),w(3),city,pm));
eqwages = fsolve(funw,[1.2,.8,1],options);
wh = eqwages(1);
wl = eqwages(2);
r1 = eqwages(3);

wages=[wh,wl];

[~,cout] = funw(eqwages);


if city==1 && year==1980
    targeterror = sum([(moments.wh(city) - wh/pm.wl2)^2;
                   (moments.wl(city) - wl/pm.wl2)^2;
                   (moments.pexit(city) - cout.pexit)^2;]);
           
elseif city==2 && year==1980 
    targeterror = sum([(moments.wh(city) - wh/wl)^2
                    (moments.pexit(city) - cout.pexit)^2;]);
    
elseif city==1 && year==2005
    targeterror = sum([(moments.wh(city) - wh/pm.wl2)^2;
                   (moments.wl(city) - wl/pm.wl2)^2;
                   (moments.pexit(city) - cout.pexit)^2;]);
else 
    targeterror = sum([(moments.wh(city) - wh/pm.wl2)^2;
                   (moments.wl(city) - wl/pm.wl2)^2;
                   (moments.pexit(city) - cout.pexit)^2;]);
end

end

