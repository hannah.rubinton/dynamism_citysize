function [excess_demand, cout] = solve_city(wh,wl,h,l,r,j,pm)

if wh<=0 || wl<=0
    excess_demand = [Inf, Inf];
    return;
end

%fixed costs
FC = pm.fcy(j) +  wh*pm.fch(j) + wl*pm.fcl(j) + r*pm.fb(j);


%Unit cost function 
ucf = @(z) (pm.gmH(j)^pm.rho.*z.^(pm.psiH(j)*(pm.rho-1)).*wh^(1-pm.rho)...
    + pm.gmL(j)^pm.rho.*z.^(pm.psiL(j)*(pm.rho-1)).*wl^(1-pm.rho)).^(1/(1-pm.rho));

sp=.01;
zgrid = exp([0:sp:5]);
zmax = max(zgrid);
g = @(z) pm.lambda(j).*z.^(-pm.lambda(j)-1);

%integrate over pi to get average profits 
Piavg = @(zstar) integral(@(z)FC.*((ucf(z)./ucf(zstar)).^(1-pm.sig)-1).*g(z),zstar,Inf);

fec = @(zstar) Piavg(zstar)/pm.delta(j) - pm.fey(j) - wh*pm.feh(j) - wl*pm.fel(j);


% %plot free entry condition
% for idx = 1:size(zgrid,2)
%    fec_z(idx) = fec(zgrid(idx)); 
% end
% plot(zgrid,fec_z)


%find the z that maximizes the unit cost function 
%for the soln to free entry only searc to the right of the max ucf z
[~,idx_zmax] = max(ucf(zgrid));


if fec(zgrid(idx_zmax))>0 && fec(zmax)<0 
    %zstar1 = bisect(fec1,zgrid(idx_zmax1),zmax);
    %zstar1 = fsolve(fec1,zgrid(idx_zmax1)+2);
    fecmin = @(zstar) fec(zstar)^2;
    options = optimset('Display','off');
    zstar = fmincon(fecmin,zgrid(idx_zmax)+2,[],[],[],[],zgrid(idx_zmax),[],[],options);
else 
    zstar = zgrid(idx_zmax);
end

assert(zgrid(idx_zmax)-.0001<zstar,'exit is not on monotone part of cost function')



%with no rent agg Revenue is equal to total wages paid to labor (including
%for entry)
%use the price index to solve for M

pc = @(z) ucf(z)*pm.sig/(pm.sig-1);
P=1;


%landlord profits
pib = r^(pm.eta(j)/(pm.eta(j)-1))*((pm.eta(j)-1)/pm.eta(j));
cb = r^(pm.eta(j)/(pm.eta(j)-1))*(1/pm.eta(j));

% %average revenue by integrating the revenue function 
rev = @(z) (ucf(z)./ucf(zstar)).^(1-pm.sig).*pm.sig.*FC;
avg_rev = integral(@(z)rev(z).*g(z),zstar,Inf)/(zstar^(-pm.lambda(j)));
M = (wh*h+wl*l + pib + cb)/(avg_rev - pm.fcy(j) - pm.delta(j)/(zstar^(-pm.lambda(j)))*pm.fey(j));

Rev = M*avg_rev;

%calculate labor demand by integrating over the hicksian demand functions 
q = @(z) Rev*P^(pm.sig-1).*pc((z)).^(-pm.sig);


% hicksian demand 
lamh = @(z)pm.gmH(j).*(z).^(pm.psiH(j)*(pm.rho-1)/pm.rho);
laml = @(z)pm.gmL(j).*(z).^(pm.psiL(j)*(pm.rho-1)/pm.rho);

ld = @(z)(laml(z)./wl.*ucf(z)).^pm.rho;
hd = @(z)(lamh(z)./wh.*ucf(z)).^pm.rho;

%demand for labor for variable costs 
Hd_vc = integral(@(z)hd(z).*q(z).*g(z),zstar,Inf)/(zstar^(-pm.lambda(j)))*M;
Ld_vc = integral(@(z)ld(z).*q(z).*g(z),zstar,Inf)/(zstar^(-pm.lambda(j)))*M;

%average gammas 
cout.gmh_avg = integral(@(z)lamh(z).*hd(z).*q(z).*g(z),zstar,Inf)/(zstar^(-pm.lambda(j)))*M/Hd_vc;
cout.gml_avg = integral(@(z)laml(z).*hd(z).*q(z).*g(z),zstar,Inf)/(zstar^(-pm.lambda(j)))*M/Hd_vc;


%demand for labor fixed costs 
Hd_fc = M*pm.fch(j);
Ld_fc = M*pm.fcl(j);
Hd_ec =M*pm.delta(j)/(zstar^(-pm.lambda(j)))*(pm.feh(j));
Ld_ec =M*pm.delta(j)/(zstar^(-pm.lambda(j)))*(pm.fel(j));

funp = @(z) pc(z).^(1-pm.sig).*g(z)/(zstar^(-pm.lambda(j)))*M;
cout.Ptest = integral(funp,zstar,Inf)^(1/(1-pm.sig));

cout.H = Hd_vc + Hd_fc + Hd_ec;
cout.L = Ld_vc + Ld_fc + Ld_ec;
cout.M=M;
cout.Rev = Rev;
cout.zstar = zstar;

Me = cout.M*pm.delta(j)/(cout.zstar^(-pm.lambda(j)));

cout.pexit = (cout.M*pm.delta(j) + Me*(1-cout.zstar^(-pm.lambda(j))))/(cout.M+Me);

cout.Bd = M*pm.fb(j);
Bs = r^(1/(pm.eta(j)-1));

excess_demand = [10*(cout.H-h), 10*(cout.L-l),cout.Bd-Bs];

end