

j=1

%Unit cost function 
ucf = @(z) (pm.gmH(j)^pm.rho.*z.^(pm.psiH*(pm.rho-1)).*wh^(1-pm.rho)...
    + pm.gmL(j)^pm.rho.*z.^(pm.psiL*(pm.rho-1)).*wl^(1-pm.rho)).^(1/(1-pm.rho));


%sk premium 

zgrid = linspace(1,10,100)
plot(zgrid,ucf(zgrid,psih,-psih,gmh,gml,wh,wl))
