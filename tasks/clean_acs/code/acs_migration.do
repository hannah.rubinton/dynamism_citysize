
set more off
cap log close
*ssc install estout, replace
net from "/home/hherman/estout/"
clear
set matsize 10000
set maxvar 10000


global rawdata "/scratch/network/hherman/rawdata"
global xwalks "/home/hherman/dynamism_citysize/tasks/clean_crosswalks/output"
global data "/scratch/network/hherman/dynamism_citysize/data"
global output "/home/hherman/dynamism_citysize/tasks/clean_acs/output"


/**************************
Use ACS dat
***************************/

*merge from PUMAs and County groups to CBSA

*need to get the cntygp98 variable
use "$rawdata/ACS/usa_00019.dta" if year==1980, clear
*use "$rawdata/ACS/usa_00019.dta" if year==1980 & runiform()<.01, clear
*runiform()<.01
rename cntygp98 countygroup
joinby statefip countygroup using "$xwalks/puma_cbsa_census80.dta"
save "$data/acs_puma.dta", replace

use "$rawdata/ACS/usa_00019.dta" if year==1990, clear
*use "$rawdata/ACS/usa_00019.dta" if year==1990  & runiform()<.01, clear
*gen sample = runiform()
*keep if sample<.01
joinby statefip puma using "$xwalks/puma_cbsa_census90.dta"
tab year
append using "$data/acs_puma.dta"
save "$data/acs_puma.dta", replace


use "$rawdata/ACS/usa_00019.dta" if (multyear>=2000 & multyear<=2011 & multyear~=.) | year==2000 | year==2005, clear
*use "$rawdata/ACS/usa_00019.dta" if (multyear>=2000 & multyear<=2011 & multyear~=.) | year==2000 | year==2005 & runiform()<.01	, clear
joinby statefip puma using "$xwalks/puma_cbsa_census00.dta" 
append using "$data/acs_puma.dta"
save "$data/acs_puma.dta", replace 

use "$rawdata/ACS/usa_00019.dta" if multyear>=2012 & multyear~=., clear
*use "$rawdata/ACS/usa_00019.dta" if multyear>=2012 & multyear~=. & runiform()<.01, clear
joinby statefip puma using "$xwalks/puma_cbsa_census10.dta" 
append using "$data/acs_puma.dta"
tab year
save "$data/acs_puma.dta", replace 



*autor and dorn use person weight * hours weight * commuting zone adjusted weight
*for now just use perwt*hours
gen wgt = perwt*afactor
drop perwt afactor hhwt serial datanum gq multyear pernum

/*
*concatenate state fips and county fip
tostring statefip countyfips, replace force
*county code should be three digits 
replace countyfips = "0" + countyfips if length(countyfips)==1
replace countyfips = "0" + countyfips if length(countyfips)==2
replace countyfips = statefip + countyfips
destring countyfips statefip, replace
*/
drop puma statefip countygroup


*limit to working age, labor force
drop if age<16 | age>64
keep if labforce==2
drop if classwkrd ==29 // drop unpaid family work (should be the same as out of lf) 
drop if empstatd == 13 | empstatd== 14 | empstatd == 15 //drop armed forces
gen unemployed = (empstat==2)
drop if uhrswork<30 //only full time 

drop labforce empstatd empstat 


*use autor and dorns occupation classification
*replace occ=occ1990 if year>=1990 & year<=2000 //AD match files use the occ1990 codes for after 1980
*2003 uses the ACS codes which are 4 digits, should be 3, trim the last digit
*divide by 10 then use a floor function to get rid of the decimal
replace occ = floor(occ/10) if year>=2003


gen occ_ad = .
*local years 1950 1970 1980 1990 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 
*local years 1950 1970 1980 1990 2000 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 
levelsof year, local(years)
foreach yr in `years' {
	if `yr'>=2000 {
		merge m:1 occ using "$rawdata/crosswalks/occ2005_occ1990dd.dta"
		//for some reason the occ codes in my dataset correspond to the ACS occ 
		//codes instead of the 2000 occ codes. 
	}
	else {
		merge m:1 occ using "$rawdata/crosswalks/occ`yr'_occ1990dd.dta"
	}
	replace occ_ad = occ1990dd if year==`yr'
	tab _merge if year==`yr'
	tab occ1990dd if _merge~=3
	drop occ1990dd _merge

}
tab occ_ad
replace occ = occ_ad
replace ind = ind1990
gen occ1990dd = occ
do AutorDorn_occ_classifications.do //create a categorical variable for occupations using the classifications from Autor and Dorn
drop occ_ad occ occ1990dd

/*
*create broad industry classifications
do ind_classifications.do
gen sh_other_nontrd_srv = (ind_other_non_tradable_service == 1) 
gen sh_manufacturing = (ind_manufacturing == 1)
gen sh_wholesale = (ind_wholesale == 1 )
gen sh_fire = (ind_fire == 1)
gen sh_prof_srv = (ind_professional_service == 1)
gen sh_health = (ind_health==1)
gen sh_legal = (ind_legal==1) 
gen sh_education = (ind_education==1)
gen sh_retail = (ind_retail==1)
gen sh_pubadmin = (ind_pubadmin==1)

gen ind_cat2=.
replace ind_cat2 = 1 if (ind_other_non_tradable_service == 1) 
replace ind_cat2 = 2 if  (ind_manufacturing == 1)
replace ind_cat2 = 3 if (ind_wholesale == 1 )
replace ind_cat2 = 4 if (ind_fire == 1)
replace ind_cat2 = 5 if (ind_professional_service == 1)
replace ind_cat2 = 6 if (ind_health==1)
replace ind_cat2 = 7 if (ind_legal==1) 
replace ind_cat2 = 8 if (ind_education==1)
replace ind_cat2 = 9 if (ind_retail==1)
replace ind_cat2 = 10 if  (ind_pubadmin==1)
*/

*clean weeks and hours data
gen weeks = . //leave it missing if it's less; it will be too inaccurate 
replace weeks = 51 if wkswork2 == 6
replace weeks = 48.5 if wkswork2 == 5
replace weeks = 43.5 if wkswork2 ==4
replace uhrswork = . if uhrswork==00 // NA

*use hourly wage as in autor and dorn
replace incwage = . if incwage>=999998
replace inctot = . if inctot>=999998
replace ftotinc = . if ftotinc>=999998
*replace incwage = incwage * cpi99
*replace inctot = inctot * cpi99
*replace ftotinc = ftotinc * cpi99
gen hwage = incwage/(uhrswork*weeks)
gen hours = uhrswork*weeks
gen lincwage = log(incwage)
gen lhwage = log(hwage)

gen rftotinc = ftotinc - mortamt1 if mortamt1>1
replace rftotinc = ftotinc - rent if mortamt1<=1 & rent>1 & rent<9998
replace rent = . if rent <=1 | rent>=9998
replace mortamt1 =. if mortamt1 <=1 
gen lrftotinc = log(rftotinc)
gen lrent = log(rent)
gen lmortamt1 = log(mortamt1)
drop uhrswork weeks ftotinc wkswork2 mortamt2 taxincl insincl rentgrs cpi99

*by year occ: sum incwage
gen age2 = age*age
gen educcat = 1 if  educd<=50 //lt hs
replace educcat = 2 if educd>=60 & educd<=64 //hs
replace educcat = 3 if (educd>63 & educd<=83) | educd==90   //some college or associates
replace educcat = 4 if educd==101 | educd==100 | (educd>=110 & educd<=113) //bachelors
replace educcat = 5 if educd>=114 

gen sh_lths = (educcat==1)
gen sh_hs = (educcat==2)
gen sh_sc = (educcat==3)
gen sh_c = (educcat>=4)

drop educd

gen sh_prof = (occ_cat2 ==1 ) 
gen sh_admin_retail = (occ_cat2 == 2)
gen sh_lowskserv = (occ_cat2 == 3)
gen sh_bluecollar = (occ_cat2 == 4)
/*
gen sh_age16_24 = (age>=16 & age<=24)
gen sh_age25_34 = (age>=25 & age<=34)
gen sh_age35_44 = (age>=35 & age<=44)
gen sh_age45_54 = (age>=45 & age<=54)
gen sh_age55_64 = (age>=55 & age<=64)
gen sh_age65pl = (age>=65)
*/
/*
gen age_cat = .
replace age_cat=1 if (age>=16 & age<=24)
replace age_cat=2 if (age>=25 & age<=34)
replace age_cat=3 if (age>=35 & age<=44)
replace age_cat=4 if (age>=45 & age<=54)
replace age_cat=5 if (age>=55 & age<=64)
replace age_cat=6 if (age>=65)
*/
*gen sh_young = (age_cat<=3)

*gen sh_white = 1 if race==1
*replace sh_white = 0 if sh_white==.
gen race_cat = . 
replace race_cat=1 if inlist(raced,100,120)
replace race_cat=2 if inlist(raced,110,130,140,150)
replace race_cat=3 if inlist(raced,200,210)
replace race_cat=4 if inlist(raced,400,699)
drop raced race

*incwage college and no college
gen incwage_c = .
replace incwage_c = incwage if educcat>=4
gen incwage_nc = .
replace incwage_nc = incwage if educcat<4
*hwage college and no college
gen hwage_c = .
replace hwage_c = hwage if educcat>=4
gen hwage_nc = .
replace hwage_nc = hwage if educcat<4
*gen lincwage_c = log(incwage_c)
*gen lincwage_nc = log(incwage_nc)
separate lincwage, by(educcat)
separate incwage, by(educcat)


*migration
gen sh_mig5 = 0 if migrate5==1 | migrate5==2
replace sh_mig5 = 1 if migrate5==3
gen sh_mig1 = 0 if migrate1==1 | migrate1==2
replace sh_mig1 = 1 if migrate1==3
drop migrate1 migrate5 migrate1d migrate5d

*top coding 
bys year: egen topinc = max(incwage)
gen sh_topcoded = (incwage == topinc)

fvset base 35620 cbsa
local vars hwage incwage
levelsof year, local(years)
levelsof cbsa, local(cbsas)
foreach var of local vars {
	gen `var'_resid = .
	gen `var'_fc_resid = .
	gen `var'_c_resid = .
	gen `var'_nc_resid = .

	foreach yr of local years {
		reg l`var' i.educcat age age2 i.sex i.race_cat i.cbsa [aw=wgt] if year ==`yr'
		predict res, resid
		replace `var'_resid = res if year==`yr'
		drop res

		*full set of controls
		reg l`var'  i.educcat age age2 i.occ_cat i.sex i.race_cat i.cbsa [aw=wgt] if year==`yr'
		predict res, resid
		replace `var'_fc_resid = res if year==`yr'
		drop res
		
		*residuals for college educated	
		reg l`var'  age age2 i.sex i.race_cat i.cbsa [aw=wgt] if year ==`yr' & educcat>=4
		predict res, resid
		replace `var'_c_resid = res if year==`yr'
		drop res

		*residuals for no college educated	
		reg l`var'  age age2 i.sex i.race_cat i.cbsa [aw=wgt] if year ==`yr' & educcat<4
		predict res, resid
		replace `var'_nc_resid = res if year==`yr'
		drop res
	

	}
}




gen pop=1
collapse (mean) sh_* lincwage* lhwage  ///
	incwage*_resid hwage*_resid ///	
	 hwage_nc hwage_c hwage incwage incwage_c incwage_nc  urate=unemployed   ///
	rftotinc lrftotinc lrent rent mortamt1 lmortamt1 ///
	(median) incwage_med=incwage incwage_c_med=incwage_c incwage_nc_med=incwage_nc /// 
	hwage_med=hwage hwage_c_med=hwage_c hwage_nc_med=hwage_nc /// 
	(sd) sd_lincwage=lincwage sd_lincwage1=lincwage1 sd_lincwage2=lincwage2 ///
	sd_lincwage3=lincwage3 sd_lincwage4=lincwage4 sd_lincwage5=lincwage5 ///
	 sd_incwage_resid=incwage_resid sd_incwage_fc_resid=incwage_fc_resid ///
	sd_incwage_c_resid=incwage_c_resid sd_incwage_nc_resid=incwage_nc_resid ///
	(sd) sd_hwage_resid=hwage_resid sd_hwage_fc_resid=hwage_fc_resid ///
	sd_hwage_c_resid=hwage_c_resid sd_hwage_nc_resid=hwage_nc_resid ///
	sd_rftotinc=lrftotinc sd_rent=lrent sd_mortamt1=lmortamt1 ///
	(sum) clf_acs=pop (rawsum) N=pop [aw=wgt], by(year cbsa)
	 



saveold "$output/acs_cbsa_migration.dta", replace version(13)

