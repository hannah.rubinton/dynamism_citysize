
set more off
cap log close
*ssc install estout, replace
net from "/home/hherman/estout/"
clear
set matsize 10000
set maxvar 10000


global rawdata "/scratch/network/hherman/rawdata"
global xwalks "/home/hherman/dynamism_citysize/tasks/clean_crosswalks/output"
global data "/scratch/network/hherman/dynamism_citysize/data"
global output "/home/hherman/dynamism_citysize/tasks/clean_acs/output"
global citysizecat "/home/hherman/dynamism_citysize/tasks/citysizecat/output"

/**************************
Use ACS dat
***************************/

*merge from PUMAs and County groups to CBSA

*need to get the cntygp98 variable
use "$rawdata/ACS/usa_00018.dta" if year==1980, clear
*use "$rawdata/ACS/usa_00018.dta" if year==1980 & runiform()<.01, clear
*runiform()<.01
rename cntygp98 countygroup
joinby statefip countygroup using "$xwalks/puma_cbsa_census80.dta"
save "$data/acs_puma.dta", replace

use "$rawdata/ACS/usa_00018.dta" if year==1990, clear
*use "$rawdata/ACS/usa_00018.dta" if year==1990  & runiform()<.01, clear
*gen sample = runiform()
*keep if sample<.01
joinby statefip puma using "$xwalks/puma_cbsa_census90.dta"
tab year
append using "$data/acs_puma.dta"
save "$data/acs_puma.dta", replace


use "$rawdata/ACS/usa_00018.dta" if (multyear>=2000 & multyear<=2011 & multyear~=.) | year==2000 | year==2005, clear
*gen sample = runiform()
*keep if sample<.01
joinby statefip puma using "$xwalks/puma_cbsa_census00.dta" 
append using "$data/acs_puma.dta"
save "$data/acs_puma.dta", replace 

use "$rawdata/ACS/usa_00018.dta" if multyear>=2012 & multyear~=., clear
*gen sample = runiform()
*keep if sample<.01
joinby statefip puma using "$xwalks/puma_cbsa_census10.dta" 
append using "$data/acs_puma.dta"
tab year
save "$data/acs_puma.dta", replace 



*autor and dorn use person weight * hours weight * commuting zone adjusted weight
*for now just use perwt*hours
gen wgt = perwt*afactor
drop perwt afactor

/*
*concatenate state fips and county fip
tostring statefip countyfips, replace force
*county code should be three digits 
replace countyfips = "0" + countyfips if length(countyfips)==1
replace countyfips = "0" + countyfips if length(countyfips)==2
replace countyfips = statefip + countyfips
destring countyfips statefip, replace
*/
drop puma statefip countygroup


*limit to working age, labor force
drop if age<16 | age>64
keep if labforce==2
drop if classwkrd ==29 // drop unpaid family work (should be the same as out of lf) 
drop if empstatd == 13 | empstatd== 14 | empstatd == 15 //drop armed forces
gen unemployed = (empstat==2)
drop if uhrswork<30  //only full time 

drop labforce empstatd empstat 


*use autor and dorns occupation classification
*replace occ=occ1990 if year>=1990 & year<=2000 //AD match files use the occ1990 codes for after 1980
*2003 uses the ACS codes which are 4 digits, should be 3, trim the last digit
*divide by 10 then use a floor function to get rid of the decimal
replace occ = floor(occ/10) if year>=2003


gen occ_ad = .
*local years 1950 1970 1980 1990 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 
*local years 1950 1970 1980 1990 2000 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 
levelsof year, local(years)
foreach yr in `years' {
	if `yr'>=2000 {
		merge m:1 occ using "$rawdata/crosswalks/occ2005_occ1990dd.dta"
		//for some reason the occ codes in my dataset correspond to the ACS occ 
		//codes instead of the 2000 occ codes. 
	}
	else {
		merge m:1 occ using "$rawdata/crosswalks/occ`yr'_occ1990dd.dta"
	}
	replace occ_ad = occ1990dd if year==`yr'
	tab _merge if year==`yr'
	tab occ1990dd if _merge~=3
	drop occ1990dd _merge

}
tab occ_ad
replace occ = occ_ad
gen occ1990dd = occ
do AutorDorn_occ_classifications.do //create a categorical variable for occupations using the classifications from Autor and Dorn
drop occ_ad occ // occ1990dd


*create broad industry classifications
replace ind = ind1990
do ind_classifications.do
/*
gen sh_other_nontrd_srv = (ind_other_non_tradable_service == 1) 
gen sh_manufacturing = (ind_manufacturing == 1)
gen sh_wholesale = (ind_wholesale == 1 )
gen sh_fire = (ind_fire == 1)
gen sh_prof_srv = (ind_professional_service == 1)
gen sh_health = (ind_health==1)
gen sh_legal = (ind_legal==1) 
gen sh_education = (ind_education==1)
gen sh_retail = (ind_retail==1)
gen sh_pubadmin = (ind_pubadmin==1)
*/
gen ind_cat2=.
replace ind_cat2 = 1 if (ind_other_non_tradable_service == 1) 
replace ind_cat2 = 2 if  (ind_manufacturing == 1)
replace ind_cat2 = 3 if (ind_wholesale == 1 )
replace ind_cat2 = 4 if (ind_fire == 1)
replace ind_cat2 = 5 if (ind_professional_service == 1)
replace ind_cat2 = 6 if (ind_health==1)
replace ind_cat2 = 7 if (ind_legal==1) 
replace ind_cat2 = 8 if (ind_education==1)
replace ind_cat2 = 9 if (ind_retail==1)
replace ind_cat2 = 10 if  (ind_pubadmin==1)


*clean weeks and hours data
gen weeks = . //leave it missing if it's less; it will be too inaccurate 
replace weeks = 51 if wkswork2 == 6
replace weeks = 48.5 if wkswork2 == 5
replace weeks = 43.5 if wkswork2 ==4
replace uhrswork = . if uhrswork==00 // NA

*use hourly wage as in autor and dorn
replace incwage = . if incwage>=999998
replace inctot = . if inctot>=999998
replace ftotinc = . if ftotinc>=999998
replace incwage = incwage * cpi99
*replace inctot = inctot * cpi99
*replace ftotinc = ftotinc * cpi99
gen hwage = incwage/(uhrswork*weeks)
gen hours = uhrswork*weeks
gen lincwage = log(incwage)
gen lhwage = log(hwage)

drop uhrswork weeks ftotinc wkswork2

*by year occ: sum incwage
gen age2 = age*age
gen educcat = 1 if  educd<=50 //lt hs
replace educcat = 2 if educd>=60 & educd<=64 //hs
replace educcat = 3 if (educd>63 & educd<=83) | educd==90   //some college or associates
replace educcat = 4 if educd==101 | educd==100  //bachelors
replace educcat = 5 if educd>=110 //graduate degree or more than 4 years college
replace educcat = . if missing(educd)

gen sh_lths = (educcat==1)
gen sh_hs = (educcat==2)
gen sh_sc = (educcat==3)
gen sh_college = (educcat>=4)

drop educd

gen sh_prof = (occ_cat2 ==1 ) 
gen sh_admin_retail = (occ_cat2 == 2)
gen sh_lowskserv = (occ_cat2 == 3)
gen sh_bluecollar = (occ_cat2 == 4)

gen race_cat = . 
replace race_cat=1 if inlist(raced,100,120)
replace race_cat=2 if inlist(raced,110,130,140,150)
replace race_cat=3 if inlist(raced,200,210)
replace race_cat=4 if inlist(raced,400,699)
drop raced race

*incwage college and no college
gen incwage_college = incwage if inrange(educcat,4,5)
gen incwage_nc = incwage if inrange(educcat,1,3)
*hwage college and no college
gen hwage_college = hwage if inrange(educcat,4,5)
gen hwage_nc  = hwage if inrange(educcat,1,3)

separate lincwage, by(educcat)
separate incwage, by(educcat)

gen skillgroup = 1 if inrange(educcat,4,5)
replace skillgroup = 0 if inrange(educcat,1,3)

*top coding 
bys year: egen topinc = max(incwage)
gen sh_topcoded = (incwage == topinc)

fvset base 35620 cbsa
local vars hwage incwage
levelsof year, local(years)
levelsof cbsa, local(cbsas)

foreach var of local vars {

	gen `var'_res = .
	gen `var'_fs_res = .
	gen `var'_byskc_res = .
	gen `var'_bysknc_res = .
	gen `var'_noeduc_res = .
	gen `var'_fs_noeduc_res = .
	forvalues j=1/5 {
		gen `var'_byeduccat`j'_res = .
	}
	
	foreach yr of local years {
		** calcualte cbsa fixed effects 
		reg l`var' i.skillgroup age age2 i.sex i.race_cat i.cbsa [aw=wgt] if year ==`yr'
		matrix `var'`yr' = e(b)

		*full set of controls
		reg l`var'  i.skillgroup age age2 i.occ1990dd i.ind1990 i.sex i.race_cat i.cbsa [aw=wgt] if year==`yr'
		matrix `var'`yr'_fs = e(b)
		
		*for college educated	
		reg l`var'  age age2 i.sex i.race_cat i.cbsa [aw=wgt] if year ==`yr' & inrange(educcat,4,5)
		matrix `var'`yr'_college = e(b)


		*for no college educated	
		reg l`var'  age age2 i.sex i.race_cat i.cbsa [aw=wgt] if year ==`yr' & inrange(educcat,1,3)
		matrix `var'`yr'_nc = e(b)
	
		*full set of controls college 
		reg l`var'   age age2 i.occ1990dd i.ind1990 i.sex i.race_cat i.cbsa [aw=wgt] if year==`yr' & inrange(educcat,4,5)
		matrix `var'`yr'_college_fs = e(b)


		*full set of controls no college 
		reg l`var'   age age2 i.occ1990dd i.ind1990 i.sex i.race_cat i.cbsa [aw=wgt] if year==`yr' & inrange(educcat,1,3)
		matrix `var'`yr'_nc_fs = e(b)

		
		** adjusted wage method
		reg l`var' i.skillgroup age age2 i.sex i.race_cat [aw=wgt] if year==`yr'
		predict temp, res
		replace `var'_res = exp(temp) if year==`yr'
		drop temp
		
		** adjusted wage method, no education variable
		reg l`var' age age2 i.sex i.race_cat [aw=wgt] if year==`yr'
		predict temp, res
		replace `var'_noeduc_res = exp(temp) if year==`yr'
		drop temp
		
		** adjusted wage method occ and industry
		reg l`var' i.skillgroup age age2 i.sex i.race_cat i.occ1990dd i.ind1990 [aw=wgt] if year==`yr'
		predict temp, res
		replace `var'_fs_res = exp(temp) if year==`yr'
		drop temp
		
		** adjusted wage method occ and industry, no education variable
		reg l`var'  age age2 i.sex i.race_cat i.occ1990dd i.ind1990 [aw=wgt] if year==`yr'
		predict temp, res
		replace `var'_fs_noeduc_res = exp(temp) if year==`yr'
		drop temp
		
		** adjusted wages by skill group
		reg l`var'  age age2 i.sex i.race_cat i.occ1990dd i.ind1990 [aw=wgt] if year==`yr' & inrange(educcat,4,5)
		predict temp, res
		replace `var'_byskc_res = exp(temp) if year==`yr' & inrange(educcat,4,5)
		drop temp 
		
		reg l`var'  age age2 i.sex i.race_cat i.occ1990dd i.ind1990 [aw=wgt] if year==`yr' & inrange(educcat,1,3)
		predict temp, res
		replace `var'_bysknc_res = exp(temp) if year==`yr' & inrange(educcat,1,3)
		drop temp 
		
		forvalue j=1/5 {
			reg l`var'  age age2 i.sex i.race_cat i.occ1990dd i.ind1990 [aw=wgt] if year==`yr' & educcat==`j'
			predict temp, res
			replace `var'_byeduccat`j'_res = exp(temp) if year==`yr' & educcat==`j'
			drop temp 
		}
	}
}
** adjusted wages for college and no college 
*incwage college and no college
gen incwage_c_res =  incwage_noeduc_res if inrange(educcat,4,5)
gen incwage_nc_res = incwage_noeduc_res if inrange(educcat,1,3)
*hwage college and no college
gen hwage_c_res =  hwage_noeduc_res if inrange(educcat,4,5)
gen hwage_nc_res =  hwage_noeduc_res if inrange(educcat,1,3)
*incwage college and no college
gen incwage_c_fs_res = incwage_fs_noeduc_res if inrange(educcat,4,5)
gen incwage_nc_fs_res =  incwage_fs_noeduc_res if inrange(educcat,1,3)
*hwage college and no college
gen hwage_c_fs_res = hwage_fs_noeduc_res if inrange(educcat,4,5)
gen hwage_nc_fs_res =  hwage_fs_noeduc_res if inrange(educcat,1,3)



saveold "$data/acs_3perc_cleaned.dta", replace version(13)

use  "$data/acs_3perc_cleaned.dta", clear
gen pop=1
collapse (mean) sh_* lincwage* lhwage  ///
	 hwage* incwage*  urate=unemployed   ///
	(median) incwage_med=incwage incwage_college_med=incwage_college incwage_nc_med=incwage_nc /// 
	hwage_med=hwage hwage_college_med=hwage_college hwage_nc_med=hwage_nc /// 
	(sum) clf_acs=pop (rawsum) N=pop [aw=wgt], by(year cbsa)
	 

*get fixed effects from regressions 
levelsof cbsa, local(cbsas)
levelsof year, local(years)
local vars hwage incwage
foreach var of local vars {
gen l`var'_cbsafe  = 0 if cbsa == 35620 //new york is base cbsa
gen l`var'_college_cbsafe  = 0 if cbsa == 35620 //new york is base cbsa
gen l`var'_nc_cbsafe  = 0 if cbsa == 35620 //new york is base cbsa
gen l`var'_fs_cbsafe  = 0 if cbsa == 35620 //new york is base cbsa
gen l`var'_college_fs_cbsafe  = 0 if cbsa == 35620 //new york is base cbsa
gen l`var'_nc_fs_cbsafe  = 0 if cbsa == 35620 //new york is base cbsa
foreach c of local cbsas {
foreach yr of local years {

	replace l`var'_cbsafe = `var'`yr'[1,colnumb("`var'`yr'","`c'.cbsa")] if year==`yr' & cbsa==`c'
	replace l`var'_college_cbsafe = `var'`yr'_college[1,colnumb("`var'`yr'_college","`c'.cbsa")] if year==`yr' & cbsa==`c'
	replace l`var'_nc_cbsafe = `var'`yr'_nc[1,colnumb("`var'`yr'_nc","`c'.cbsa")] if year==`yr' & cbsa==`c'

	replace l`var'_fs_cbsafe = `var'`yr'_fs[1,colnumb("`var'`yr'_fs","`c'.cbsa")] if year==`yr' & cbsa==`c'
	replace l`var'_college_fs_cbsafe = `var'`yr'_college_fs[1,colnumb("`var'`yr'_college_fs","`c'.cbsa")] if year==`yr' & cbsa==`c'
	replace l`var'_nc_fs_cbsafe = `var'`yr'_nc_fs[1,colnumb("`var'`yr'_nc_fs","`c'.cbsa")] if year==`yr' & cbsa==`c'
	


}
}
}



saveold "$output/acs_cbsa.dta", replace version(13)



**collapse data by city size category 
use  "$data/acs_3perc_cleaned.dta", clear
gen pop=1
merge m:1 cbsa year using "$citysizecat/citysizecat.dta", nogen keep(match master) 
collapse (mean) sh_* lincwage* lhwage  ///
	 hwage* incwage*  urate=unemployed   ///
	(median) incwage_med=incwage incwage_college_med=incwage_college incwage_nc_med=incwage_nc /// 
	hwage_med=hwage hwage_college_med=hwage_college hwage_nc_med=hwage_nc /// 
	(sum) clf_acs=pop (rawsum) N=pop [aw=wgt], by(year citysizecat)


saveold "$output/acs_citysizecat.dta", replace version(13)





**collapse data by 1980 city size category 
use  "$data/acs_3perc_cleaned.dta", clear
gen pop=1
merge m:1 cbsa year using "$citysizecat/citysizecat.dta", nogen keep(match master) 
collapse (mean) sh_* lincwage* lhwage  ///
	 hwage* incwage*  urate=unemployed   ///
	(median) incwage_med=incwage incwage_college_med=incwage_college incwage_nc_med=incwage_nc /// 
	hwage_med=hwage hwage_college_med=hwage_college hwage_nc_med=hwage_nc /// 
	(sum) clf_acs=pop (rawsum) N=pop [aw=wgt], by(year citysizecat1980)


saveold "$output/acs_citysizecat1980.dta", replace version(13)




