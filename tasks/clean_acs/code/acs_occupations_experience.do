
set more off
cap log close
*ssc install estout, replace
net from "/home/hherman/estout/"
clear
set matsize 10000
set maxvar 10000


global rawdata "/scratch/network/hherman/rawdata"
global xwalks "/home/hherman/dynamism_citysize/tasks/clean_crosswalks/output"
global data "/scratch/network/hherman/dynamism_citysize/data"
global output "/home/hherman/dynamism_citysize/tasks/clean_acs/output"
global citysizecat "/home/hherman/dynamism_citysize/tasks/citysizecat/output"


**ACS 
**calculate returns to experience by occupation 
**are returns correlated with changing occupation shares 

use  "$data/acs_3perc_cleaned.dta", clear


keep if inlist(year,1980,1990,2000,2007,2010,2013)

separate lhwage, by(educcat)
gen lincwage_college = log(incwage_college)
gen lincwage_nc = log(incwage_nc)
gen lhwage_college = log(hwage_college)
gen lhwage_nc = log(hwage_nc)


levelsof occ1990dd, local(occs)
levelsof year, local(years)
local vars hwage incwage  incwage_nc incwage_college //  hwage_nc hwage_college

foreach var of local vars {
	gen `var'_bage = .
	gen `var'_bage2 = .
foreach occ of local occs {
	sum l`var' if  occ1990dd==`occ'
	if r(N) > 30 {

		reg l`var' i.year#c.age i.year#c.age2 i.year i.educcat#i.year i.sex#i.year i.race_cat#i.year if occ1990dd==`occ' [aw=wgt]
		foreach yr of local years {
			cap replace `var'_bage = _b[`yr'.year#c.age] if year==`yr' & occ1990dd==`occ'
			cap replace `var'_bage2 = _b[`yr'.year#c.age2] if year==`yr' & occ1990dd==`occ'
		}
	}	
}
}

gen pop=1
collapse (sum) pop (mean) sh* incwage incwage1 incwage2 incwage3 incwage4 incwage5 /// 
	incwage_college incwage_nc incwage_c_res incwage_nc_res incwage_c_fs_res incwage_nc_fs_res  ///
	incwage_byskc_res incwage_bysknc_res ///	
	hwage_college hwage_nc hwage_c_res hwage_nc_res hwage_c_fs_res hwage_nc_fs_res  ///
	hwage_byskc_res hwage_bysknc_res ///	
	 (first) *_bage*, by(occ1990dd year)

save "$output/occupation_returns_to_experience.dta", replace 


