/************************************
Similar to what Autor and Dorn did
I want to classify the industry codes into broad categories of tradable - non-tradable 
*************************************/


/*************************************
Industry Dummies: natural resources 
**************************************/
gen ind_agriculture = 0   // crops, livestock, agriculture service, forestry, fishing/hunting
replace ind_agriculture = 1 if inlist(ind1990,10,11,30,31,32)

gen ind_mining = 0 
replace ind_mining= 1 if inlist(ind1990,40,41,42,50)


/*************************************
Non tradable services
12=veterianry services
20=landscape and horticultural services 

401 urban transit
402 taxi
412 post office 

722 services to dwellings and other buildings
740 detective and protective service
742 auto rental and leasing
750 parking and carwash
751 auto repair
752 electrical repair shops
760 miscellaneious repair services
801 video rental
802 bowling
**************************************/
gen ind_other_non_tradable_service = 0
replace ind_other_non_tradable_service = 1 if inlist(ind1990,12,20,401,402,412,722,740,742,750,751,752,760,801,802)
replace ind_other_non_tradable_service = 1 if ind1990>=761 & ind1990<=791 // personal services and entertainment services 
replace ind_other_non_tradable_service = 1 if ind1990>=873 & ind1990<=881

*health care
gen ind_health = 0
replace ind_health = 1 if ind1990>=812 & ind1990<=840

*legal 
gen ind_legal = 0 
replace ind_legal = 1 if ind1990==841

*education including child care, social services and museums
gen ind_education = 0 
replace ind_education = 1 if ind1990>=842 & ind1990<=872

*retail trade 
gen ind_retail = 0 
replace ind_retail = 1 if ind1990>=580 & ind1990<=691

*public administration 
gen ind_pubadmin = 0 
replace ind_pubadmin = 1 if ind1990>=900 & ind1990<=932

gen ind_non_tradable_service = 0
replace ind_non_tradable_service = 1 if ind_health==1 | ind_legal==1 | ind_education==1 | ind_retail==1 /// 
	| ind_pubadmin==1 | ind_other_non_tradable_service==1



*utilities 
gen ind_utilities = 0 
replace ind_utilities = 1 if inlist(ind1990,450,451,452,470,471,472)

/*************************************
Tradables
**************************************/
*manuracturing
gen ind_manufacturing = 0 
replace ind_manufacturing = 1 if ind1990>=100 & ind1990<=392

*communication services 
gen ind_communication = 0 
replace ind_communication = 1 if inlist(ind1990,440,441,442)

*wholesale trade 
gen ind_wholesale = 0 
replace ind_wholesale = 1 if ind1990>=500 & ind1990<=571


*fire
gen ind_fire = 0 
replace ind_fire = 1 if ind1990>=700 & ind1990<=712

*professional services
gen ind_professional_service = 0 //721=advertising, 800=theaters and motion picture, 810=miscellaneous entertainment
								///882=engineering/architecture/surveying, 890=accounting, 891=research, 892=management, 893=miscellaneious
replace ind_professional_service = 1 if inlist(ind1990,721,800,810,882,890,891,892,893)

/*************************************
Unsure



400 railroad
410 trucking ????
411 warehousing ????
420 water transportation 
421 air transportation 
422 pipe lines, except natural gas
432 services incidental to transportation 


**************************************/


*generate categorical variable 
gen ind_cat = . 
replace ind_cat = 1 if ind_non_tradable_service == 1
replace ind_cat = 2 if ind_manufacturing == 1
replace ind_cat = 3 if ind_wholesale == 1 
replace ind_cat = 4 if ind_fire == 1 
replace ind_cat = 5 if ind_professional_service == 1 
replace ind_cat = 6 if ind_utilities == 1 
replace ind_cat = 7 if ind_agriculture == 1 | ind_mining==1
label define ind_cat_label ///
	1 "non-tradable service" ///
	2 "manufacturing" ///
	3 "wholesale" ///
	4 "fire" ///
	5 "professional services"  /// 
	6 "non-tradable - utilities" ///
	7 "tradable- natural resource" 
	
label values ind_cat ind_cat_label
