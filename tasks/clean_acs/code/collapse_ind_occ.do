set more off
cap log close
*ssc install estout, replace
net from "/home/hherman/estout/"
clear
set matsize 10000
set maxvar 10000


global rawdata "/scratch/network/hherman/rawdata"
global xwalks "/home/hherman/dynamism_citysize/tasks/clean_crosswalks/output"
global data "/scratch/network/hherman/dynamism_citysize/data"
global output "/home/hherman/dynamism_citysize/tasks/clean_acs/output"
global citysizecat "/home/hherman/dynamism_citysize/tasks/citysizecat/output"


program define collapseby
	args citysizecat indocc educ
	use  "$data/acs_3perc_cleaned.dta", clear
	gen pop=1
	if "`citysizecat'" ~= "cbsa" {
		merge m:1 cbsa year using "$citysizecat/citysizecat.dta"
	}
	collapse (mean) sh_* lincwage lhwage  ///
		 hwage incwage incwage_res incwage_fs_res hwage_res hwage_fs_res urate=unemployed   /// 
		 incwage_fs_noeduc_res hwage_fs_noeduc_res ///
		 (sum) clf_acs=pop (rawsum) N=pop [aw=wgt], by(year `citysizecat' `indocc' `educ')
	saveold "$output/acs_`citysizecat'_`indocc'_`educ'.dta", replace version(13)
end	 

**********************
*Collapse by industry cbsa
**********************
collapseby cbsa ind1990 skillgroup
collapseby cbsa occ1990dd skillgroup
collapseby citysizecat ind1990 skillgroup
collapseby citysizecat occ1990dd skillgroup
collapseby citysizecat1980 ind1990 skillgroup
collapseby citysizecat1980 occ1990dd skillgroup
collapseby citysizecat1980 ind1990 educcat
collapseby citysizecat1980 occ1990dd educcat
