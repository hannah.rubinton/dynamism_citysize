
set more off
cap log close
*ssc install estout, replace
net from "/home/hherman/estout/"
clear
set matsize 10000
set maxvar 10000


global rawdata "/scratch/network/hherman/rawdata"
global xwalks "/home/hherman/dynamism_citysize/tasks/clean_crosswalks/output"
global data "/scratch/network/hherman/dynamism_citysize/data"
global output "/home/hherman/dynamism_citysize/tasks/clean_acs/output"
global temp "/scratch/network/hherman/regional/data/"

/**************************
Use ACS dat
***************************/
/*
*merge from PUMAs and County groups to CBSA

*need to get the cntygp98 variable
use "$rawdata/ACS/usa_00017.dta", clear
keep if year==1980
*gen sample = runiform()
*keep if sample<.01
rename cntygp98 countygroup
joinby statefip countygroup using "$xwalks/puma_cbsa_census80.dta"
save "$data/acs_puma.dta", replace

use "$rawdata/ACS/usa_00017.dta", clear
keep if year==1990
*gen sample = runiform()
*keep if sample<.01
joinby statefip puma using "$xwalks/puma_cbsa_census90.dta"
tab year
append using "$data/acs_puma.dta"
save "$data/acs_puma.dta", replace

use "$rawdata/ACS/usa_00017.dta", clear
keep if (multyear>=2000 & multyear<=2011 & multyear~=.) | year==2000
*gen sample = runiform()
*keep if sample<.01
joinby statefip puma using "$xwalks/puma_cbsa_census00.dta" 
append using "$data/acs_puma.dta"
save "$data/acs_puma.dta", replace 

use "$rawdata/ACS/usa_00017.dta", clear
keep if multyear>=2012 & multyear~=.
tab year
*gen sample = runiform()
*keep if sample<.01
joinby statefip puma using "$xwalks/puma_cbsa_census10.dta" 
append using "$data/acs_puma.dta"
tab year
save "$data/acs_puma.dta", replace 



*autor and dorn use person weight * hours weight * commuting zone adjusted weight
*for now just use perwt*hours
gen wgt = perwt*afactor
gen pop=1


*concatenate state fips and county fip
tostring statefip countyfips, replace force
*county code should be three digits 
replace countyfips = "0" + countyfips if length(countyfips)==1
replace countyfips = "0" + countyfips if length(countyfips)==2
replace countyfips = statefip + countyfips
destring countyfips statefip, replace




*limit to working age, labor force
drop if age<16 | age>64
keep if labforce==2
drop if classwkrd ==29 // drop unpaid family work (should be the same as out of lf) 
drop if empstatd == 13 | empstatd== 14 | empstatd == 15 //drop armed forces
gen unemployed = (empstat==2)
drop if uhrswork<30 //only full time 

*use autor and dorns occupation classification
*replace occ=occ1990 if year>=1990 & year<=2000 //AD match files use the occ1990 codes for after 1980
*2003 uses the ACS codes which are 4 digits, should be 3, trim the last digit
*divide by 10 then use a floor function to get rid of the decimal
replace occ = floor(occ/10) if year>=2003


tab year
gen occ_ad = .
*local years 1950 1970 1980 1990 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 
*local years 1950 1970 1980 1990 2000 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 
levelsof year, local(years)
foreach yr in `years' {
	if `yr'>=2000 {
		merge m:1 occ using "$data/crosswalks/occ2005_occ1990dd.dta"
		//for some reason the occ codes in my dataset correspond to the ACS occ 
		//codes instead of the 2000 occ codes. 
	}
	else {
		merge m:1 occ using "$data/crosswalks/occ`yr'_occ1990dd.dta"
	}
	replace occ_ad = occ1990dd if year==`yr'
	tab _merge if year==`yr'
	tab occ1990dd if _merge~=3
	drop occ1990dd _merge

}
tab occ_ad
replace occ = occ_ad
replace ind = ind1990
gen occ1990dd = occ
do AutorDorn_occ_classifications.do //create a categorical variable for occupations using the classifications from Autor and Dorn



*create broad industry classifications
do ind_classifications.do
gen sh_other_nontrd_srv = (ind_other_non_tradable_service == 1) 
gen sh_manufacturing = (ind_manufacturing == 1)
gen sh_wholesale = (ind_wholesale == 1 )
gen sh_fire = (ind_fire == 1)
gen sh_prof_srv = (ind_professional_service == 1)
gen sh_health = (ind_health==1)
gen sh_legal = (ind_legal==1) 
gen sh_education = (ind_education==1)
gen sh_retail = (ind_retail==1)
gen sh_pubadmin = (ind_pubadmin==1)

gen ind_cat2=.
replace ind_cat2 = 1 if (ind_other_non_tradable_service == 1) 
replace ind_cat2 = 2 if  (ind_manufacturing == 1)
replace ind_cat2 = 3 if (ind_wholesale == 1 )
replace ind_cat2 = 4 if (ind_fire == 1)
replace ind_cat2 = 5 if (ind_professional_service == 1)
replace ind_cat2 = 6 if (ind_health==1)
replace ind_cat2 = 7 if (ind_legal==1) 
replace ind_cat2 = 8 if (ind_education==1)
replace ind_cat2 = 9 if (ind_retail==1)
replace ind_cat2 = 10 if  (ind_pubadmin==1)


*clean weeks and hours data
gen weeks = . //leave it missing if it's less; it will be too inaccurate 
replace weeks = 51 if wkswork2 == 6
replace weeks = 48.5 if wkswork2 == 5
replace weeks = 43.5 if wkswork2 ==4
replace uhrswork = . if uhrswork==00 // NA

*use hourly wage as in autor and dorn
replace incwage = . if incwage>=999998
replace inctot = . if inctot>=999998
replace ftotinc = . if ftotinc>=999998
replace incwage = incwage * cpi99
replace inctot = inctot * cpi99
replace ftotinc = ftotinc * cpi99
gen hwage = incwage/(uhrswork*weeks)
gen hours = uhrswork*weeks
gen lincwage = log(incwage)
gen lhwage = log(hwage)

sort year occ
*by year occ: sum incwage
gen age2 = age*age
gen educcat = 1 if  educd<=50 //lt hs
replace educcat = 2 if educd>=60 & educd<=64 //hs
replace educcat = 3 if (educd>63 & educd<=83) | educd==90   //some college or associates
replace educcat = 4 if educd==101 | educd==100 | (educd>=110 & educd<=113) //bachelors
replace educcat = 4 if educd>=114 

gen sh_lths = (educcat==1)
gen sh_hs = (educcat==2)
gen sh_sc = (educcat==3)
gen sh_college = (educcat==4)


gen sh_prof = (occ_cat2 ==1 ) 
gen sh_admin_retail = (occ_cat2 == 2)
gen sh_lowskserv = (occ_cat2 == 3)
gen sh_bluecollar = (occ_cat2 == 4)

gen sh_age16_24 = (age>=16 & age<=24)
gen sh_age25_34 = (age>=25 & age<=34)
gen sh_age35_44 = (age>=35 & age<=44)
gen sh_age45_54 = (age>=45 & age<=54)
gen sh_age55_64 = (age>=55 & age<=64)
gen sh_age65pl = (age>=65)

gen age_cat = .
 
replace age_cat=1 if (age>=16 & age<=24)
replace age_cat=2 if (age>=25 & age<=34)
replace age_cat=3 if (age>=35 & age<=44)
replace age_cat=4 if (age>=45 & age<=54)
replace age_cat=5 if (age>=55 & age<=64)
replace age_cat=6 if (age>=65)

gen sh_young = (age_cat<=3)

gen sh_white = 1 if race==1
replace sh_white = 0 if sh_white==.

*incwage college and no college
gen incwage_college = .
replace incwage_college = incwage if educcat==4
gen incwage_nc = .
replace incwage_nc = incwage if educcat<4
*hwage college and no college
gen hwage_college = .
replace hwage_college = hwage if educcat==4
gen hwage_nc = .
replace hwage_nc = hwage if educcat<4


*top coding 
by year: egen topinc = max(incwage)
gen sh_topcoded = (incwage == topinc)

local vars hwage incwage
levelsof year, local(years)
levelsof cbsa, local(cbsas)
foreach var of local vars {

	gen l`var'_resid = .
	gen `var'_resid = .
	gen l`var'_cbsafe = .

	foreach yr of local years {
		reg l`var' i.educcat age age2 [aw=wgt] if year ==`yr'
		predict res, resid 
		replace l`var'_resid = res if year ==`yr'
		replace  `var'_resid = exp(res) if year==`yr'
		drop res

		reg l`var' i.cbsa i.educcat age age2 [aw=wgt] if year ==`yr'
		foreach cbsa of local cbsas {
			cap replace l`var'_cbsafe = _b[`cbsa'.cbsa] if year == `yr' & cbsa==`cbsa'
		}
	}
}



save "$data/acs_3perc_cleaned.dta", replace
*/
use  "$temp/acs_3perc_cleaned.dta", clear
rename msa cbsa
collapse (mean) sh_* hwage_nc hwage_college hwage incwage incwage_college incwage_nc  urate=unemployed  *resid *msafe  /// 
	(median) incwage_med=incwage incwage_college_med=incwage_college incwage_nc_med=incwage_nc /// 
	hwage_med=hwage hwage_college_med=hwage_college hwage_nc_med=hwage_nc /// 
	(sum) clf_acs=pop (rawsum) N=pop [aw=wgt], by(year cbsa)
	
	
	
*create variable of 1980 manufacturing share
gen temp = sh_manufacturing if year==1980
bys cbsa: egen sh_manufacturing_1980 = total(temp) 
replace sh_manufacturing_1980 = . if sh_manufacturing_1980==0
drop temp

save "$output/acs_cbsa.dta", replace 






