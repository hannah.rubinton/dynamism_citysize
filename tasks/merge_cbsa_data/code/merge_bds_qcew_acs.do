






set more off
cap log close
*ssc install estout, replace
*net from "/home/hherman/estout/"
clear


global output  "../../merge_cbsa_data/output"
global bds "../../clean_bds/output"
global qcew "../../clean_qcew/output"
global acs "../../clean_acs/output"
global hpi "../../clean_hpi/output"
global laus "../../clean_laus/output"
global seer "../../clean_seer/output"
global citysizecat "../../citysizecat/output"
global crosswalks "../../clean_crosswalks/output"
global bea "../../clean_bea/output"

/**********************************
Use cleaned BDS data for SR and job reallocation, QCEW for industry composition, LAUS for clf 
run some initial regressions of start up and exit rate
on state and msa size
***********************************/

*use bds data cleaned in clean_bds.do
use "$bds/bds_cbsa.dta", clear

*merge clf data
merge 1:1 year cbsa using "$laus/laus_cbsa.dta"
*drop if _merge ==2
drop _merge
rename urate ur_laus


*merge industry data from qcew
merge 1:1 cbsa year using "$qcew/qcew_cbsa_ind_composition.dta"
*drop if _merge ==2
drop _merge

/*
*merge industry data from qcew
merge 1:1 cbsa year using "$qcew/qcew_cbsa_sic_ind_composition.dta"
drop if _merge ==2
drop _merge
*/


*merge acs demographics 
merge 1:1 cbsa year using "$acs/acs_cbsa.dta"
*drop if _merge ==2
drop _merge
*drop sh_age65pl
*local rnvars sh_lths sh_hs sh_sc sh_college sh_age* sh_white N 
*foreach var in `rnvars' {
*	rename `var' acs_`var' //some variables have the same name in cps data
*}


*merge census population estimates 
* this is the data downloaded from seer 
merge 1:1 cbsa year using "$seer/cbsa_pop.dta"
*drop if _merge==2
drop _merge



*merge HPI data
merge 1:1 cbsa year using "$hpi/hpi_cbsa.dta"
*drop if _merge==2
drop _merge


*merge bank data from qcew
merge 1:1 cbsa year using "$qcew/qcew_banking_cbsa.dta"
*drop if _merge==2
drop _merge

*merge estab entry rate 
merge 1:1 cbsa year using "$bds/bds_e_cbsa.dta"
*drop if _merge==2
drop _merge



*merge in metro vs micro leve
merge m:1 cbsa using "$crosswalks/cbsa_vs_msa.dta"
drop _merge


*merge in city size category 
merge 1:1 cbsa year using "$citysizecat/citysizecat.dta"
drop _merge

*merge in bea data

merge 1:1 cbsa year using "$bea/cbsa_bea.dta"
drop _merge 

*gen statefip = substr(cbsa,1,2)
*gen alhi = 1 if statefip ==02 | statefip==15 | statefip==. //alaska and hawaii 


/***************************************************************
Create Variables 
****************************************************************/



gen lclf = log(clf)
gen lcemp = log(cemp)
gen lclf_acs = log(clf_acs)
gen lpop = log(pop)


gen lincwage_m = log(incwage) 
gen lhwage_m = log(hwage)
gen lincwage_med = log(incwage_med) 
gen lhwage_med = log(hwage_med)


*gen variable of ratio of college to highschool 
gen CtoH = sh_college / (sh_hs + sh_lths)
gen CtoH2 = sh_college / (sh_hs + sh_lths + sh_sc)


*banks per firms
gen banks_per_firm = banks / firms
gen bankemp_per_firm = bankemp / firms

*establishment share of large firms
gen estabsh_sz3 = estabs_szg3 / estabs

/*
*drop certain industries because they cause large drops in sample size when controlling for industry composition 
*they don't make a difference in parameter estimates. The only one I'm concerned about is warehousing so double 
*check that this doesn't change things 
drop *11 //agriculture
drop *21 //mining 
drop *22 //utilities 
drop *48 //transporting and warehousing 
*/
drop if cbsa==.







*also drop MSAs with less than 100 observations in ACS 
*want to make sure we are using a consistent panel
gen tag = (N<100)
bys cbsa: egen temp = total(tag)
gen toosmall = temp>0
drop temp tag
tab toosmall if year==1980
*drop if toosmall==1



***Drop all that aren't in census
gen tag = (N==. & year==1980)
bys cbsa: egen temp = total(tag)
gen nocensus = temp>0
drop temp tag
tab nocensus if year==1980
*drop if nocensus==1

*only keep if whole time series
sort cbsa year
by cbsa: gen Nyears=_N 
tab Nyears
*drop if Nyears<38


*calculate msa's share of total firms
bys year: egen tot_firms = total(firms)
gen firmsh = firms / tot_firms
*calc agg. sr using weighted average of msa srs
gen temp = sr * firmsh
bys year: egen agg_sr = total(temp)
drop temp

**create some variables of the change since 1980 
local chvars sh_college sr firms lclf_acs lemp firmsh
foreach var in `chvars' {
	gen temp = `var' if year==1980 
	bys cbsa: egen `var'_1980 = total(temp)
	drop temp 
	gen dl_`var'_1980 = `var' - `var'_1980
	gen gr_`var'_1980 = (`var' - `var'_1980)/`var'_1980*100
}


***Save Dataset so that we can run some regressions on the server
save "$output/sr_cbsa.dta", replace
