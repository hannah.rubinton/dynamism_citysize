


cap log close
clear
set more off


global xwalks "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_crosswalks/output"
global census "/Users/hannah/Google Drive File Stream/My Drive/rawdata/intercensal_pop"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_seer/output"


use "$census/seer.dta", clear 
rename population pop
rename cfips countyfips
rename stfip statefip
destring countyfips, replace 
destring statefip, replace 

merge m:1 countyfips using "$xwalks/cleaned_countytocbsacrosswalk_2009definitions.dta"
keep if _merge==3

*for each cbsa you want to drop any countys that aren't in the sample 
*the whole time the cbsa is in the sample
*eg Kappa HI starts in 2000, but it isn't adding to data that starts earlier so we can keep it
bys cbsa countyfips: gen obs = _N 
by cbsa: egen maxobs = max(obs)
keep if obs == maxobs 



collapse (sum) pop, by(year cbsa)

sort cbsa year
*10yr growth annualized g solves 
*pop[_n-10]*(1+g)^10=pop[_n]
by cbsa: gen popgr_10yra = (pop[_n]/pop[_n-10])^(1/10)-1 if year[_n] - year[_n-10] == 10

*5yr growth annualized
by cbsa: gen popgr_5yra = (pop[_n]/pop[_n-5])^(1/5)-1 if year[_n] - year[_n-5] == 5
by cbsa: gen l5_popgr_5yra = popgr_5yra[_n-5] if year[_n] - year[_n-5] == 5

label variable pop "population, census" 
label variable popgr_10yra "population growth, 10 yr annualized"
label variable popgr_5yra "population growth, 5 yr annualized"
label variable l5_popgr_5yra "5 year lagged population growth, 5 yr annualized"
drop if cbsa==.
save "$data/cbsa_pop.dta", replace



/****************
Age groups
*****************/
set more off
infile using "$census/us.1969_2016.19ages.adjusted.dct", using("$census/us.1969_2016.19ages.adjusted.txt") clear

gen agegrp2=.
replace agegrp2=1 if (agegrp ==06) //20 to 24
replace agegrp2=2 if (agegrp>=7 & agegrp<=11) //25 to 54
replace agegrp2=3 if (agegrp>=12 & agegrp<=13) //55-64
collapse (sum) pop, by(year countyfips agegrp2)
drop if agegrp2==.

reshape wide pop , i(year countyfips) j(agegrp2)


merge m:1 countyfips using "$xwalks/cleaned_countytocbsacrosswalk_2009definitions.dta"
keep if _merge==3

*for each cbsa you want to drop any countys that aren't in the sample 
*the whole time the cbsa is in the sample
*eg Kappa HI starts in 2000, but it isn't adding to data that starts earlier so we can keep it
bys cbsa countyfips: gen obs = _N 
by cbsa: egen maxobs = max(obs)
keep if obs == maxobs 

collapse (sum) pop*, by(year cbsa)


rename pop1 pop_age20_24
rename pop2 pop_age25_54
rename pop3 pop_age55_64
gen papop = pop_age25_54 
gen wapop = pop_age20_24 + pop_age25_54 + pop_age55_64

sort cbsa year
*10yr growth annualized g solves 
*pop[_n-10]*(1+g)^10=pop[_n]
by cbsa: gen papopgr_10yra = (papop[_n]/papop[_n-10])^(1/10)-1 if year[_n] - year[_n-10] == 10
by cbsa: gen wapopgr_10yra = (wapop[_n]/wapop[_n-10])^(1/10)-1 if year[_n] - year[_n-10] == 10
*5yr growth annualized
by cbsa: gen papopgr_5yra = (papop[_n]/papop[_n-5])^(1/5)-1 if year[_n] - year[_n-5] == 5
by cbsa: gen wapopgr_5yra = (wapop[_n]/wapop[_n-5])^(1/5)-1 if year[_n] - year[_n-5] == 5
by cbsa: gen l5_papopgr_5yra = papopgr_5yra[_n-5] if year[_n] - year[_n-5] == 5
by cbsa: gen l5_wapopgr_5yra = wapopgr_5yra[_n-5] if year[_n] - year[_n-5] == 5

label variable pop_age20_24 "population ages 20-24, census"
label variable pop_age25_54 "population ages 25-54, census"
label variable pop_age55_64 "population ages 55-64, census"
label variable wapop "working age 20-64 population, census" 
label variable wapopgr_10yra "working age 25-64 population growth, 10 yr annualized, census"
label variable wapopgr_5yra "working age 25-64 population growth, 5 yr annualized, census"
label variable l5_wapopgr_5yra "5 year lagged working age population growth, 5 yr annualized, census"
label variable papop "prime age population, census" 
label variable papopgr_10yra "prime age population growth, 10 yr annualized, census"
label variable papopgr_5yra "prime agepopulation growth, 5 yr annualized, census"
label variable l5_papopgr_5yra "5 year lagged prime age population growth, 5 yr annualized, census"

drop if cbsa==.

save "$data/cbsa_popagegrps.dta", replace




*merge the with the pop data
merge 1:1 year cbsa using "$data/cbsa_pop.dta"
drop _merge //not matched are 2016
save "$data/cbsa_pop.dta", replace


