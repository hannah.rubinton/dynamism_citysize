cap log close
clear
set more off

global xwalks "/Users/hannah/Google Drive File Stream/My Drive/rawdata/crosswalks"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_crosswalks/output"


/*****************
1990 geographic equivalency files 
downloaded from 
https://usa.ipums.org/usa/volii/ctygrp.shtml

Use raw equivalency files to create a crosswalk from 
county group to CBSA that has an adjustment factor for the 
percent of CG X that maps to CBSA Y
*******************/



import delimited "$xwalks/cg98stat.csv", clear varnames(1)

*create state county fips code to merge in the county to cbsa crosswalk
rename state statefip 
rename countyfipscode countyfips
tostring statefip countyfip, replace 
replace countyfip = "0" + countyfip if length(countyfip)==1
replace countyfip = "0" + countyfip if length(countyfip)==2
replace countyfip = statefip + countyfip
destring statefip countyfip, replace


collapse (sum) countypop (mean) countygrouppop, by(state countygroup countyfips)
rename countypop pop80

merge m:1 countyfip using "$data/cleaned_countytocbsacrosswalk_2009definitions.dta"
drop if _merge==2

collapse (sum) pop80 (mean) countygrouppop, by(statefip countygroup cbsa)

*adjustment factor
gen afactor = pop80 / countygrouppop

keep statefip countygroup cbsa afactor
save "$data/puma_cbsa_census80.dta", replace 


/*****************
1990 geographic equivalency files 
downloaded from 
https://usa.ipums.org/usa/volii/puma.shtml

Use raw equivalency files to create a crosswalk from 
puma to CBSA that has an adjustment factor for the 
percent of puma X that maps to CBSA Y
*******************/

import delimited "$xwalks/1990_PUMAs_5pct.csv", clear varnames(1)


*create state county fips code to merge in the county to cbsa crosswalk
rename state statefip 
rename countyfipscode countyfips
tostring statefip countyfip, replace 
replace countyfip = "0" + countyfip if length(countyfip)==1
replace countyfip = "0" + countyfip if length(countyfip)==2
replace countyfip = statefip + countyfip
destring statefip countyfip, replace


collapse (sum) areapop (mean) pumapop, by(state puma countyfips)
rename areapop pop90
rename pumapop puma_pop

merge m:1 countyfip using "$data/cleaned_countytocbsacrosswalk_2009definitions.dta"

collapse (sum) pop90 (mean) puma_pop, by(statefip puma cbsa)

*adjustment factor
gen afactor = pop90 / puma_pop

keep statefip puma cbsa afactor
save "$data/puma_cbsa_census90.dta", replace 




/*****************
2000 geographic equivalency files 
downloaded from 
https://usa.ipums.org/usa/volii/2000pumas.shtml


Use raw equivalency files to create a crosswalk from 
puma to CBSA that has an adjustment factor for the 
percent of puma X that maps to CBSA Y
*******************/

infile using "$xwalks/puma_census2000_dictionary.dct", using("$xwalks/2000PUMAsASCII.txt") clear

keep if SL<=781 //puma and puma-county levels
gen temp = pop00 if SL==781
bys statefip puma: egen puma_pop=total(temp)
drop temp MSA SL
keep if countyfip~=. 

*create state county fips code to merge in the county to cbsa crosswalk
tostring statefip countyfip, replace 
replace countyfip = "0" + countyfip if length(countyfip)==1
replace countyfip = "0" + countyfip if length(countyfip)==2
replace countyfip = statefip + countyfip
destring statefip countyfip, replace


merge m:1 countyfip using "$data/cleaned_countytocbsacrosswalk_2009definitions.dta"
collapse (sum) pop00 (mean) puma_pop, by(statefip puma cbsa)

*adjustment factor
gen afactor = pop00 / puma_pop

keep statefip puma cbsa afactor
save "$data/puma_cbsa_census00.dta", replace 





/*****************
2010 geographic equivalency files 
downloaded from 
https://www2.census.gov/geo/docs/reference/puma/
though use ftp://ftp2.census.gov/geo/docs/reference/puma with wget to download


important documentation is here:
https://www2.census.gov/geo/pdfs/reference/puma/


Use raw equivalency files to create a crosswalk from 
puma to CBSA that has an adjustment factor for the 
percent of puma X that maps to CBSA Y
*******************/
clear

local state 01 02 04 05 06 08 09 10 11 12 13 15 16 17 18 19 20 21 22 23 24 ///
	25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 44 45 46 47 48 49 ///
	50 51 53 54 55 56 72

foreach st in `state' {
infile using "$xwalks/puma_census2010_dictionary.dct", using("$xwalks/puma/PUMSEQ10_`st'.txt") clear



keep if SL<=796 // state-puma and state-puma-county records
keep SL statefip puma countyfip pop hu areaname

gen temp = pop10 if SL==796
bys statefip puma: egen puma_pop=total(temp)
drop temp
keep if countyfip~=. 


if "`st'"=="01" {
	save "$data/puma_county_equiv.dta", replace 
} 
else {
	append using "$data/puma_county_equiv.dta"
	save "$data/puma_county_equiv.dta", replace 
}

} //end loop over states


*create state county fips code to merge in the county to cbsa crosswalk
tostring statefip countyfip, replace 
replace countyfip = "0" + countyfip if length(countyfip)==1
replace countyfip = "0" + countyfip if length(countyfip)==2
replace countyfip = statefip + countyfip
destring statefip countyfip, replace


merge m:1 countyfip using "$data/cleaned_countytocbsacrosswalk_2009definitions.dta"
collapse (sum) pop10 hu10 (mean) puma_pop  , by(statefip puma cbsa)

*adjustment factor
gen afactor = pop10 / puma_pop

keep statefip puma cbsa afactor
save "$data/puma_cbsa_census10.dta", replace 
