
cap log close
clear
set more off

global xwalks "/Users/hannah/Google Drive File Stream/My Drive/rawdata/crosswalks"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_crosswalks/output"


/******************************************************
cross walk comes from 
https://www.census.gov/geographies/reference-files/time-series/demo/metro-micro/delineation-files.html
don't use the nber crosswalk because it's using the mdiv codes and won't map to the BDS
********************************************************/


import excel using "$xwalks/countytocbsacrosswalk_2009definitions.xls", clear
drop if _n<=4 | _n>=1867

rename A cbsa
rename K countyfips
keep cbsa  countyfips

destring cbsa countyfips, replace 
save "$data/cleaned_countytocbsacrosswalk_2009definitions.dta", replace 




import excel using "$xwalks/countytocbsacrosswalk_2009definitions.xls", clear
drop if _n<=4 | _n>=1867

rename A cbsa
rename D cbsaname
rename E level
keep cbsa cbsaname  level 
destring cbsa level, replace 

sort cbsa 
by cbsa: keep if _n==1

save "$data/cbsa_vs_msa.dta", replace 




