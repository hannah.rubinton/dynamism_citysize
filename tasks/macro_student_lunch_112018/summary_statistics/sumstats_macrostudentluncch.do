
*summary statistics for presentation 


global mergeddata "/Users/hannah/Dropbox/dynamism_citysize/tasks/merge_cbsa_data/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/macro_student_lunch_112018/summary_statistics"
use "$mergeddata/sr_cbsa.dta", clear
drop lq*
drop sh_emp*

drop if sr>=.18 & !missing(sr)


replace sr = sr*100
replace fer = 100*fer

local dv sr jcr esr fer jdr eer 
sort cbsa year 
foreach v of local dv {

	by cbsa: gen `v'_3yr = (`v'[_n] + `v'[_n-1] + `v'[_n+1] )/3
	gen temp = `v'_3yr if year==1980
	by cbsa: egen `v'_3yr_1980 = total(temp)
	gen dl80_`v' = `v'_3yr - `v'_3yr_1980 if  `v'_3yr_1980>0
	drop temp
	
}

gen temp = lpop if year==1980
by cbsa: egen lpop_1980 = total(temp)
replace lpop_1980 = . if lpop_1980 ==0

*scatter dl80_sr lpop if year==2005


eststo clear 
eststo: reg dl80_sr lpop_1980 if year==2005
eststo: reg dl80_fer lpop_1980 if year==2005
eststo: reg dl80_esr lpop_1980 if year==2005
eststo: reg dl80_eer lpop_1980 if year==2005
eststo: reg dl80_jcr lpop_1980 if year==2005
eststo: reg dl80_jdr lpop_1980 if year==2005

esttab using "$ctask/dlsr.tex", se r2 keep(*lpop*) title("Change in dynamism vs. 1980 city size")  replace 



/*

drop if lpop<10
*firm sr 
twoway (scatter sr_3yr lpop if year==1980, mcolor("205 100 100") ms(oh))  || /// 
	(lfit sr_3yr lpop if year==1980,  lcolor("205 100 100")) ///  
	|| (scatter sr_3yr lpop if year==2005, mcolor("100 100 205") ms(oh))  || /// 
	(lfit sr_3yr lpop if year==2005, lcolor("100 100 205")), ///
	legend(lab(1 "1980") lab(3 "2005")) xtitle("log(population)") ytitle("Firm Start Up Rate") graphregion(color(white))
graph export "$ctask/sr_pop.eps", replace 


* firm er 
twoway (scatter fer_3yr lpop if year==1980, mcolor("205 100 100") ms(oh))  || /// 
	(lfit fer_3yr lpop if year==1980,  lcolor("205 100 100")) ///  
	|| (scatter fer_3yr lpop if year==2005, mcolor("100 100 205") ms(oh))  || /// 
	(lfit fer_3yr lpop if year==2005, lcolor("100 100 205")), ///
	legend(lab(1 "1980") lab(3 "2005")) xtitle("log(population)") ytitle("Firm Exit Rate") graphregion(color(white))
graph export "$ctask/fer_pop.eps", replace 


*firm esr 
twoway (scatter esr_3yr lpop if year==1980, mcolor("205 100 100") ms(oh))  || /// 
	(lfit esr_3yr lpop if year==1980,  lcolor("205 100 100")) ///  
	|| (scatter esr_3yr lpop if year==2005, mcolor("100 100 205") ms(oh))  || /// 
	(lfit esr_3yr lpop if year==2005, lcolor("100 100 205")), ///
	legend(lab(1 "1980") lab(3 "2005")) xtitle("log(population)") ytitle("Establishment Start Up Rate") graphregion(color(white))
graph export "$ctask/esr_pop.eps", replace 


* firm eer 
twoway (scatter eer_3yr lpop if year==1980, mcolor("205 100 100") ms(oh))  || /// 
	(lfit eer_3yr lpop if year==1980,  lcolor("205 100 100")) ///  
	|| (scatter eer_3yr lpop if year==2005, mcolor("100 100 205") ms(oh))  || /// 
	(lfit eer_3yr lpop if year==2005, lcolor("100 100 205")), ///
	legend(lab(1 "1980") lab(3 "2005")) xtitle("log(population)") ytitle("Establishment Exit Rate") graphregion(color(white))
graph export "$ctask/eer_pop.eps", replace 

*jcr
twoway (scatter jcr_3yr lpop if year==1980, mcolor("205 100 100") ms(oh))  || /// 
	(lfit jcr_3yr lpop if year==1980,  lcolor("205 100 100")) ///  
	|| (scatter jcr_3yr lpop if year==2005, mcolor("100 100 205") ms(oh))  || /// 
	(lfit jcr_3yr lpop if year==2005, lcolor("100 100 205")), ///
	legend(lab(1 "1980") lab(3 "2005")) xtitle("log(population)") ytitle("Job Creation Rate") graphregion(color(white))
graph export "$ctask/jcr_pop.eps", replace 


*jdr 
twoway (scatter jdr_3yr lpop if year==1980, mcolor("205 100 100") ms(oh))  || /// 
	(lfit jdr_3yr lpop if year==1980,  lcolor("205 100 100")) ///  
	|| (scatter jdr_3yr lpop if year==2005, mcolor("100 100 205") ms(oh))  || /// 
	(lfit jdr_3yr lpop if year==2005, lcolor("100 100 205")), ///
	legend(lab(1 "1980") lab(3 "2005")) xtitle("log(population)") ytitle("Job Destruction Rate") graphregion(color(white))
graph export "$ctask/jdr_pop.eps", replace 



*skill premium
gen skpremium = incwage_college/incwage_nc 
twoway (scatter skpremium lpop if year==1980, mcolor("205 100 100") ms(oh))  || /// 
	(lfit skpremium lpop if year==1980,  lcolor("205 100 100")) ///  
	|| (scatter skpremium lpop if year==2005, mcolor("100 100 205") ms(oh))  || /// 
	(lfit skpremium lpop if year==2005, lcolor("100 100 205")), ///
	legend(lab(1 "1980") lab(3 "2005")) xtitle("log(population)") ytitle("Skill premium") graphregion(color(white))
graph export "$ctask/skp_pop.eps", replace 


*/




/*
drop if citysizecat==.
gen hasdynv = 1 if sr~=.

collapse (sum) firms_ageg1 firms emp estabs firmdeath_firms job_creation job_destruction ///
	estabs_entry estabs_exit hasdynv (count) N=cbsa (mean) pop wapop, by(year citysizecat)

keep if inlist(year,1980,2005)
	
gen sr = firms_ageg1 / firms*100
gen jcr = job_creation / emp*100
gen esr = estabs_entry / estabs*100
gen fer = firmdeath_firms / firms*100
gen jdr = job_destruction / emp*100
gen eer = estabs_exit / estabs*100

local dv sr jcr esr fer jdr eer 
sort citysizecat year 
foreach v of local dv {

	by citysizecat: gen dl_`v' = `v'[_n] - `v'[_n-1]
	
}

keep year citysizecat `dv' dl* pop wapop N hasdynv



keep if inlist(year,1980,2005)
gen period = 1980 if year>=1978 & year<=1980
replace period =2005 if year>=2005 & year<=2005

collapse (mean) sr jcr esr fer jdr eer pop wapop N, by(period citysizecat)


drop pop wapop N
reshape wide `dv', i(citysizecat) j(period)


foreach v of local dv {
	gen dl_`v' = `v'2005 - `v'1980
	}
	
	
mkmat esr1980 esr2005 dl_esr eer1980 eer2005 dl_eer, matrix(estabs) ///
	rownames(citysizecat)
esttab matrix(estabs) using "$ctask/estab_ss.tex", replace alignment(cccccc) ///
	title("establishment start up and exit rate summary statisitcs by city size category")
	
mkmat sr1980 sr2005 dl_sr fer1980 fer2005 dl_fer, matrix(firms) ///
	rownames(citysizecat)
esttab matrix(firms) using "$ctask/firm_ss.tex", replace alignment(cccccc) ///
	title("firm start up and exit rate summary statisitcs by city size category")
	
mkmat jcr1980 jcr2005 dl_jcr jdr1980 jdr2005 dl_jdr, matrix(jobs) ///
	rownames(citysizecat)
esttab matrix(jobs) using "$ctask/jobs_ss.tex", replace alignment(ccccccc) ///
	title("job creation and destruction rate summary statisitcs by city size category")
	
	
