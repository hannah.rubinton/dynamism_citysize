




cap log close
clear
set more off


global seer "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_seer/output"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/citysizecat/output"



use "$seer/cbsa_pop.dta", clear 

keep year cbsa wapop pop


local vars  pop 
local perc 50 75 90 95 98 99 

foreach v of local vars {

	foreach p of local perc {
		by year: egen `v'`p' = pctile(`v'), p(`p')
	
	}
	

	gen citysizecat = .
	replace citysizecat = 1 if `v' <= `v'50 & ~missing(`v')
	replace citysizecat = 2 if `v'50<`v' & `v'<=`v'75 & ~missing(`v')
	replace citysizecat = 3 if `v'75<`v' & `v'<=`v'90 & ~missing(`v')
	replace citysizecat = 4 if `v'90<`v' & `v'<=`v'95 & ~missing(`v')
	replace citysizecat = 5 if `v'95<`v' & `v'<=`v'98 & ~missing(`v')
	replace citysizecat = 6 if `v'98<`v' & `v'<=`v'99 & ~missing(`v')
	replace citysizecat = 7 if `v'99<`v' & ~missing(`v')
	

}




keep pop citysizecat cbsa year 

gen temp = citysizecat if year==1980
bys cbsa: egen citysizecat1980 = total(temp), missing

keep cbsa year citysizecat*

label define csc 1 "<=p50" ///
	2 "p51 to p75" ///
	3 "p76 to p90" ///
	4 "p91 to p95" ///
	5 "p96 to p98" ///
	6 "p99" ///
	7 ">p99"
label values citysizecat csc
label values citysizecat1980 csc
save "$data/citysizecat.dta", replace 

