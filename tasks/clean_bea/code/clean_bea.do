


cap log close
clear
set more off

global bea "/Users/hannah/Google Drive File Stream/My Drive/rawdata/GMP_BEA"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_bea/output"
global xwalks "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_crosswalks/output"

/*
import delimited using "../input/CA1_1969_2016__ALL_AREAS.csv", varnames(1)
drop if linecode==.

local i = 8
forvalues yr=1969/2016 {
	rename v`i'  v`yr'
	local i = `i' +1
}

drop table industryclass region description

reshape long v, i(geoname linecode) j(year)

reshape wide v, i(geoname year) j(linecode) 

rename v1 pi_bea 
label variable pi_bea "personal income" 
rename v2 pop_bea
label variable pop_bea "population"
rename v3 pipc_bea
label variable pipc_bea "personal income per capita" 

destring pi_bea pop_bea pipc_bea, replace ignore("(NA)")
destring geofips, replace
rename geofips countyfips
merge m:1 countyfips using "$xwalks/cleaned_countytocbsacrosswalk_2009definitions.dta"

drop pipc_bea

gen cbsacount = 1

collapse (sum) pi_bea pop_bea cbsacount, by(year cbsa)

gen pipc_bea = pi_bea/pop_bea
label variable pipc_bea "personal income per capita" 
drop cbsacount

drop if cbsa==.

save "$data/cbsa_bea.dta", replace 

*/



*convergence analysis 
use "$data/cbsa_bea.dta", clear
drop if year==.
reshape wide pi_bea pipc_bea pop_bea, i(cbsa) j(year)

*three year moving averages
forvalues t = 1970/2015 {
	local tm1 = `t'-1 
	local tp1 = `t'+1 
	gen pipc_3yma`t' = pipc_bea`t' + pipc_bea`tm1' + pipc_bea`tp1'

}

forvalues t = 1970/2005 {

	local tp10 = `t'+10
	local tp5 = `t'+5
	
	gen agr10_pipc_3yma`t' = (pipc_3yma`tp10'/pipc_3yma`t')^(1/10)-1
	gen agr5_pipc_3yma`t' = (pipc_3yma`tp5'/pipc_3yma`t')^(1/5)-1


}



reshape long pi_bea pipc_bea pop_bea pipc_3yma agr10_pipc_3yma agr5_pipc_3yma, i(cbsa) j(year)















