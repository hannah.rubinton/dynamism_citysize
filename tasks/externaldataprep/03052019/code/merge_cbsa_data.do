


cap log close
clear
set more off

cd /Users/hannah/Dropbox/dynamism_citysize/tasks/externaldataprep/03052019

*bea data
use "../../clean_bea/output/cbsa_bea.dta", clear

*merge in ACS data
merge 1:1 cbsa year using "../../clean_acs/output/acs_cbsa.dta"
drop _merge 
drop if cbsa==.

*merge in seer pop 
merge 1:1 cbsa year using "../../clean_seer/output/cbsa_pop.dta"
drop _merge
merge 1:1 cbsa year using "../../clean_seer/output/cbsa_popagegrps.dta"
drop _merge 

*merge in HPI 
merge 1:1 cbsa year using "../../clean_hpi/output/hpi_cbsa.dta"
drop _merge

*merge in citysize categories
merge 1:1 cbsa year using "../../citysizecat/output/citysizecat.dta" 
drop _merge 

*merge in cbsa names 
merge m:1 cbsa using "../../clean_crosswalks/output/cbsa_vs_msa.dta"
drop _merge

label variable cbsa "cbsa"
label variable pi_bea "personal income, bea" 
label variable pop_bea "population, bea"
label variable pipc_bea "personal income per capita, bea" 
label variable sh_lths "share less than high school, ACS" 
label variable sh_hs "share high school, ACS"
label variable sh_sc "share some college, ACS" 
label variable sh_college "share college, ACS" 
label variable sh_prof "share management, prof, tech, ACS" 
label variable sh_admin_retail "admin support and retail sales, ACS"
label variable sh_lowskserv "low skill service, ACS"
label variable sh_bluecollar "blue collar, ACS"
label variable sh_topcoded "share topcoded income, ACS" 
label variable hwage "avg hourly wage, ACS" 
label variable hwage_college "avg hourly wage, college, ACS" 
label variable hwage_nc "avg hourly wage, no college, ACS" 
label variable incwage "avg income and wages, ACS" 
label variable incwage_college "avg income and wages, college, ACS" 
label variable incwage_nc "avg income and wages, no college, ACS" 
label variable incwage_med "median income and wages, ACS" 
label variable incwage_college_med "median income and wages, college, ACS" 
label variable incwage_nc_med "median income and wages, no college, ACS" 
label variable hwage_med "median hourly wage, ACS" 
label variable hwage_college_med "median hourly wage, college, ACS" 
label variable hwage_nc_med "median hourly wage, no college, ACS" 
label variable N "sample size from ACS"
label variable urate "unemployment rate, ACS"
label variable clf_acs "civilian labor force, ACS" 
label variable cbsaname "cbsa name"
label variable level "micro vs. metropolitan"
label variable HPI "housing price index, fhfa"
label variable citysizecat "city size category, population" 
label variable citysizecat1980 "city size category, 1980 population" 
save "./datasets/cbsa_characteristics.dta", replace 


