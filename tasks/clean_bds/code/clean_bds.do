




cap log close
clear
set more off

global BDS "/Users/hannah/Google Drive File Stream/My Drive/rawdata/BDS"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_bds/output"

/*****************
Program cleans BDS data
https://www.census.gov/ces/dataproducts/bds/data_firm.html
*******************/

local geog msa //set equal to state or msa

*****Clean overall firm  data by msa
if "`geog'" == "state" {
import delimited "$BDS/bds_f_st_release.csv", clear varnames(1)
}
else {
import delimited "$BDS/bds_f_`geog'_release.csv", clear varnames(1)
}

rename year2 year

tempfile bds
save `bds', replace



*****Clean msa by age data

if "`geog'" == "state" {
import delimited "$BDS/bds_f_agest_release.csv", clear varnames(1)
}
else {
import delimited "$BDS/bds_f_age`geog'_release.csv", clear varnames(1)
}
rename year2 year

encode fage4, gen(age)

gen ageg = .
replace ageg = 1 if age==1 // startups
replace ageg = 2 if inrange(age,2,6) & year>=1982 // young, 1-5
replace ageg = 3 if age==7 & year>=1983 // medium, 6-10
replace ageg = 4 if age >=8 & year>=1987 // mature, 11+

label define _ageg ///
  1 "Age 0" ///
  2 "Age 1-5" ///
  3 "Age 6-10" ///
  4 "Age 11+"
  
lab values ageg _ageg
drop if ageg==. //left censored age

collapse (sum) firms estabs emp denom estabs_entry ///
  estabs_exit job_creation job_creation_births job_destruction ///
  job_destruction_deaths job_destruction_continuers net_job_creation ///
  firmdeath_firms firmdeath_estabs firmdeath_emp, by(year `geog' ageg)
  
local variables firms estabs emp denom estabs_entry ///
  estabs_exit job_creation job_creation_births job_destruction ///
  job_destruction_deaths job_destruction_continuers net_job_creation ///
  firmdeath_firms firmdeath_estabs firmdeath_emp 
foreach var in `variables' {
	rename `var' `var'_ageg
} 

reshape wide firms* estabs* emp* denom*  ///
   job_creation* job_destruction* net_job_creation* ///
  firmdeath*, i(year `geog') j(ageg)
  
merge 1:1 year `geog' using `bds'
drop _merge
save `bds', replace


*******Clean msa by size  
  
if "`geog'" == "state" {
import delimited "$BDS/bds_f_szst_release.csv", clear varnames(1)
}
else {
import delimited "$BDS/bds_f_sz`geog'_release.csv", clear varnames(1)
}
rename year2 year 
  
encode fsize, gen(size)  
gen sizeg = .
replace sizeg = 1 if size>=1 & size<=4
replace sizeg = 2 if size>=5 & size<=7
replace sizeg = 3 if size>=8 

label define _sizeg ///
	1 "1 to 49" ///
	2 "50 to 499" ///
	3 "500+"
label values sizeg _sizeg

collapse (sum) firms estabs emp denom estabs_entry ///
  estabs_exit job_creation job_creation_births job_destruction ///
  job_destruction_deaths job_destruction_continuers net_job_creation ///
  firmdeath_firms firmdeath_estabs firmdeath_emp, by(year `geog' sizeg)

  
  
 
local variables firms estabs emp denom estabs_entry ///
  estabs_exit job_creation job_creation_births job_destruction ///
  job_destruction_deaths job_destruction_continuers net_job_creation ///
  firmdeath_firms firmdeath_estabs firmdeath_emp 
foreach var in `variables' {
	rename `var' `var'_szg
} 

reshape wide firms* estabs* emp* denom*  ///
   job_creation* job_destruction* net_job_creation* ///
  firmdeath*, i(year `geog') j(sizeg)
  
merge 1:1 year `geog' using `bds'
drop _merge
save `bds', replace

  
  
*create some new variables 
*create some variables from teh bds data
gen sr  = firms_ageg1 / firms 
gen fer = firmdeath_firms / firms
sort `geog' year
by `geog': gen denom_firms = (firms + firms[_n-1])/2
gen sr_geo = firms_ageg1 /denom_firms 
by `geog': gen empgr = (emp[_n] - emp[_n-1])/emp[_n-1] * 100
by `geog': gen empgr_5yr = (emp[_n] - emp[_n-5])/emp[_n-5] * 100


gen lemp = log(emp)
 gen lsr = log(sr)
gen lfer = log(fer)
gen ljcr = log(job_creation_rate)
gen ljdr = log(job_destruction_rate)
rename job_creation_rate jcr
rename job_destruction_rate jdr


*for now drop the Bad MSAs from the BDS
gen tag = (sr<.03)
bys `geog': egen temp = total(tag)
gen badmsa = temp>0 
drop temp tag
tab badmsa if year==1980
*drop if badmsa==1

/*
*3 year moving averages
sort `geog' year
local variables sr fer jcr jdr emp firms
foreach var in `variables' {
	by `geog': gen `var'_3yr = (`var' + `var'[_n-1] + `var'[_n+1])/3 if year<=2000
	by `geog': replace `var'_3yr = (`var' + `var'[_n-1] + `var'[_n-2])/3 if year>2000
	by `geog': replace `var'_3yr = (`var' + `var'[_n-1])/2 if year==2014
	gen l`var'_3yr = log(`var'_3yr)
}
*/ 

if "`geog'" == "state" {
	save "$data/bds_`geog'.dta", replace
	}
else {
	rename msa cbsa
	save "$data/bds_cbsa.dta", replace
}





