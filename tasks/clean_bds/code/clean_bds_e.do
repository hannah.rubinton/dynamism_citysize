



cap log close
clear
set more off

global BDS "/Users/hannah/Google Drive File Stream/My Drive/rawdata/BDS"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_bds/output"


/*****************
Program cleans BDS data
https://www.census.gov/ces/dataproducts/bds/data_firm.html
*******************/


*****Clean overall firm  data by msa

import delimited "$BDS/bds_e_msa_release.csv", clear varnames(1)

rename year2 year

rename estabs_entry_rate esr
rename estabs_exit_rate eer
rename job_creation_rate ejcr
rename job_destruction_rate ejdr

keep year msa esr eer ejcr ejdr 



gen lesr = log(esr)
gen leer = log(eer)
gen lejcr = log(ejcr)
gen lejdr = log(ejdr)

/*
*3 year moving averages
sort msa year
local variables esr eer ejcr ejdr
foreach var in `variables' {
	by msa: gen `var'_3yr = (`var' + `var'[_n-1] + `var'[_n+1])/3 if year<=2000
	by msa: replace `var'_3yr = (`var' + `var'[_n-1] + `var'[_n-2])/3 if year>2000
	by msa: replace `var'_3yr = (`var' + `var'[_n-1])/2 if year==2014
	gen l`var'_3yr = log(`var'_3yr)
}
*/
rename msa cbsa
save "$data/bds_e_cbsa.dta", replace







