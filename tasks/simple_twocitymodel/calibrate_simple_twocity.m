function [loss] = calibrate_simple_twocity(pin, params)
%solving two city version of simple model
%parameters 
if any(pin<=0)
    loss = -Inf;
    return
end

if params.year == 1980 
    %period 1: 1980
    %moments.grW1980 = 0; 
    moments.WL2_WL2 = 1;
    moments.WH2_WL2 = 1.54;
    moments.WH1_WL2 = 1.86;
    moments.WL1_WL2 = 1.18;
    moments.HL1 = .26;
    moments.HL2 = .17;
    moments.relativecitysize = 22;
    
    params.gammaL=[1,pin(2)];
    params.gammaH=[pin(3),pin(4)];
    params.AH1 = pin(5);
    params.AL1 = pin(6);
  % params.al1 = pin(7);
  %  params.al2 = pin(8);
else 
    %period 2: 2005-2007
    %moments.grW1980 =  27; 
    moments.WL2_WL2 = 1.04;
    moments.WH2_WL2 = 1.79;
    moments.WH1_WL2 = 2.61;
    moments.WL1_WL2 = 1.21;
    moments.HL1 = .51;
    moments.HL2 = .28;
    moments.relativecitysize = 23;
    
    params.gammaL=[pin(1),pin(2)];
    params.gammaH=[pin(3),pin(4)];
    params.AH1 = pin(5);
    params.AL1 = pin(6);
   % params.al1 = pin(7);
   % params.al2 = pin(8);
end 



fun = @(x) eq_simple_model_twocity(x,params)
eq=fsolve(fun,[params.H/2,.6])


H1 = eq(1);
L1 = eq(2);
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2, WH2,CH1,CH2,CL1,CL2, UL1,UH1,UL2,UH2] = simple_model_twocity(H1,L1,params);

if params.year==1980
    model.WL2_WL2 = WL2/WL2;
    model.WH2_WL2 = WH2/WL2;
    model.WH1_WL2 = WH1/WL2;
    model.WL1_WL2 = WL1/WL2;
else 
  model.WL2_WL2 = WL2/params.WL2;
    model.WH2_WL2 = WH2/params.WL2;
    model.WH1_WL2 = WH1/params.WL2;
    model.WL1_WL2 = WL1/params.WL2;  
end
model.HL1 = H1/L1;
model.HL2 = H2/L2;
model.relsz = (H1+L1)/(H2+L2);
avgW = (H1*WH1 + H2*WH2 + L1*WL1 + L2*WL2)/(params.L+params.H) ;

if params.year == 1980 

    loss = sum([(moments.WH2_WL2 - model.WH2_WL2)^2;
            (moments.WH1_WL2 - model.WH1_WL2)^2;
            (moments.WH1_WL2 - model.WH1_WL2)^2;
            (moments.WL1_WL2 - model.WL1_WL2)^2;
            10*(moments.HL1 - model.HL1)^2;
            (moments.HL2 - model.HL2)^2;]);
           % (moments.relativecitysize - model.relsz)^2]);
    
else 
    model.grW1980 = (avgW - params.avgW1980)/params.avgW1980*100; 
    loss = sum([(moments.WL2_WL2 - model.WL2_WL2)^2;
            (moments.WH2_WL2 - model.WH2_WL2)^2;
            (moments.WH1_WL2 - model.WH1_WL2)^2;
            (moments.WH1_WL2 - model.WH1_WL2)^2;
            (moments.WL1_WL2 - model.WL1_WL2)^2;
            10*(moments.HL1 - model.HL1)^2;
            10*(moments.HL2 - model.HL2)^2;]);
          %  (moments.relativecitysize - model.relsz)^2]);    

end 



