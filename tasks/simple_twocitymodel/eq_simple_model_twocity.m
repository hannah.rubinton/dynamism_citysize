function [ diff] = eq_simple_model_twocity(x,params)
   
H1 = x(1);
L1 = x(2);
[ WL1, WH1, WL2, WH2,CH1,CH2,CL1,CL2, UL1,UH1,UL2,UH2] = simple_model_twocity(H1,L1,params);
    
diffUH = UH1 - UH2;
diffUL = UL1 - UL2; 
diff = [diffUH, diffUL];

end