function [ WL1, WH1, WL2, WH2,CH1,CH2,CL1,CL2, UL1,UH1,UL2,UH2] = simple_model_twocity(H1,L1,params)
    
    %parameters 
    zH=params.zH; 
    zL=params.zL; 
    ep=params.ep;
    gmL=params.gammaL;
    gmH=params.gammaH;
    th=params.theta;
    L=params.L;
    H=params.H;
    AH1=params.AH1;
    AL1=params.AL1;

    H2 = H-H1;
    L2 = L-L1;
    Y1 = (gmL(1)*(zL(1)*L1)^((ep-1)/ep)+gmH(1)*(zH(1)*H1)^((ep-1)/ep))^(ep/(ep-1));
    Y2 = (gmL(2)*(zL(2)*L2)^((ep-1)/ep)+gmH(2)*(zH(2)*H2)^((ep-1)/ep))^(ep/(ep-1));
    WH1 = Y1^(1/ep)*gmH(1)*zH(1)^((ep-1)/ep)*H1^(-1/ep);
    WL1 = Y1^(1/ep)*gmL(1)*zL(1)^((ep-1)/ep)*L1^(-1/ep);
    WH2 = Y2^(1/ep)*gmH(2)*zH(2)^((ep-1)/ep)*H2^(-1/ep);
    WL2 = Y2^(1/ep)*gmL(2)*zL(2)^((ep-1)/ep)*L2^(-1/ep);
    %congestion 
    CH1 = params.psiH*(H1+L1)^params.al1;
    CH2 = params.psiH*(H2+L2)^params.al2;
    CL1 = params.psiL*(H1+L1)^params.al1;
    CL2 = params.psiL*(H2+L2)^params.al2;
    %u = @(c) (c^(1-th))/(1-th);
    u = @(c) log(c);
    %worker utility 
    UH1 = u(WH1) + AH1 - CH1;
    UH2 = u(WH2) - CH2;
    UL1 = u(WL1) + AL1 - CL1;
    UL2 = u(WL2) - CL2; 
    
end
    
    
    
    
    
    