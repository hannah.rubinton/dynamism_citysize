clear
%solving two city version of simple model
%parameters 

params.zH=[1,1];
params.zL=[1,1];
<<<<<<< HEAD
params.psiH=.5;
params.psiL=.5; %if negative then they like big cities 
params.al1 = 1.2;
params.al2 = 1.2;
params.ep=3;
params.gammaL=[1,.93];
params.gammaH=[1.006,.79];
params.theta=.5;
params.L=1;
params.H=.24;
params.AH1=1.05;
params.AL1=1.1;
=======
params.psiH=1;
params.psiL=1; %if negative then they like big cities 
params.al1 = 4;
params.al2 = 4;
params.ep=2.7;
params.gammaL=[1,1];
params.gammaH=[1,1];
params.theta=.5;
params.L=1;
params.H=.24;
params.AH1=1;
params.AL1=1;
>>>>>>> 81c4a8278a7256927ad67793e0f5f955e6149523
params.year=1980;
params.WL2 =1; 



fun = @(x) eq_simple_model_twocity(x,params)
eq=fsolve(fun,[params.H/2,.6])



%{
%% calibrate 1980 
pout = calibrate_simple_twocity([1,1,1,1,1,1],params)
funcal = @(p) calibrate_simple_twocity(p,params)
pout = fminunc(funcal,[1,1,1,1,1,1])
params.gammaL=[pout(1),pout(2)];
params.gammaH=[pout(3),pout(4)];
params.AH1 = pout(5);
params.AL1 = pout(6);
%params.al1 = pout(7);
%params.al2 = pout(8);

fun = @(x) eq_simple_model_twocity(x,params)
eq=fsolve(fun,[params.H/2,.6])


H1 = eq(1)
L1 = eq(2)
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2, WH2,CH1,CH2,CL1,CL2, UL1,UH1,UL2,UH2] = simple_model_twocity(H1,L1,params);

model1980.sk_premium = [WH1/WL1, WH2/WL2];
model1980.sizepremium = [WH1/WH2, WL1/WL2];
model1980.citysize = [L1+H1, L2+H2];
model1980.Hshare = [H1/(L1+H1), H2/(L2+H2)];
model1980.avgW = (H1*WH1 + H2*WH2 + L1*WL1 + L2*WL2)/(params.L+params.H) ;
model1980.WL2_WL2 = WL2/WL2;
model1980.WH2_WL2 = WH2/WL2;
model1980.WH1_WL2 = WH1/WL2;
model1980.WL1_WL2 = WL1/WL2;
model1980.H1share = H1/L1;
model1980.H2share = H2/L2;
params1980 = params;

%% calibrate 2005
params.year=2005;
params.L=1.19
params.H=.54;
params.avgW1980 = model1980.avgW;
params.WL2 = WL2;
pout = calibrate_simple_twocity([1,1,1,1,1,1],params)
funcal = @(p) calibrate_simple_twocity(p,params)
pout = fminunc(funcal,[1,1,1,1,1,1])
params.gammaL=[pout(1),pout(2)];
params.gammaH=[pout(3),pout(4)];
params.AH1 = pout(5);
params.AL1 = pout(6);
%params.al1 = pout(7);
%params.al2 = pout(8);

fun = @(x) eq_simple_model_twocity(x,params)
eq=fsolve(fun,[params.H/2,.6])


H1 = eq(1)
L1 = eq(2)
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2, WH2,CH1,CH2,CL1,CL2, UL1,UH1,UL2,UH2] = simple_model_twocity(H1,L1,params);

model2005.sk_premium = [WH1/WL1, WH2/WL2];
model2005.sizepremium = [WH1/WH2, WL1/WL2];
model2005.citysize = [L1+H1, L2+H2];
model2005.Hshare = [H1/(L1+H1), H2/(L2+H2)];
model2005.avgW = (H1*WH1 + H2*WH2 + L1*WL1 + L2*WL2)/(params.L+params.H) ;
model2005.WL2_WL2 = WL2/params.WL2;
model2005.WH2_WL2 = WH2/params.WL2;
model2005.WH1_WL2 = WH1/params.WL2;
model2005.WL1_WL2 = WL1/params.WL2;
model2005.H1share = H1/L1;
model2005.H2share = H2/L2;
model2005.avgWgr1980 = (model2005.avgW - model1980.avgW)/model1980.avgW;
params2005 = params;


%% amenities from 1980
% how important are the seemingly small changes in amenities? What does the
% model predict if you use the 1980 parameters but keep amenities at their
% 1980 level? 

params.AH1 = params1980.AH1;
params.AL1 = params1980.AL1;
fun = @(x) eq_simple_model_twocity(x,params)
eq=fsolve(fun,[params.H/2,.6])
H1 = eq(1)
L1 = eq(2)
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2, WH2,CH1,CH2,CL1,CL2, UL1,UH1,UL2,UH2] = simple_model_twocity(H1,L1,params);
model1980AM.sk_premium = [WH1/WL1, WH2/WL2];
model1980AM.sizepremium = [WH1/WH2, WL1/WL2];
model1980AM.citysize = [L1+H1, L2+H2];
model1980AM.Hshare = [H1/(L1+H1), H2/(L2+H2)];
model1980AM.avgW = (H1*WH1 + H2*WH2 + L1*WL1 + L2*WL2)/(params.L+params.H) ;
model1980AM.WL2_WL2 = WL2/params.WL2;
model1980AM.WH2_WL2 = WH2/params.WL2;
model1980AM.WH1_WL2 = WH1/params.WL2;
model1980AM.WL1_WL2 = WL1/params.WL2;
model1980AM.H1share = H1/L1;
model1980AM.H2share = H2/L2;
model1980AM.avgWgr1980 = (model1980AM.avgW - model1980.avgW)/model1980.avgW;
params1980AM = params;

%}
