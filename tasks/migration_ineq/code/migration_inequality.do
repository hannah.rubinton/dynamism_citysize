
cap log close
clear
set more off

global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/migration_ineq"
global seer "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_seer/output"


use "$ctask/input/acs_cbsa_migration.dta", clear

merge 1:1 cbsa year using "$seer/cbsa_pop.dta"
drop if _merge ~=3 //most are non-census years 

gen lpop = log(pop)

/*

local vars sh_mig5 sh_mig1 sd_lincwage sd_incwage_fc_resid sd_incwage_nc_resid sd_incwage_c_resid ///
	lincwage incwage_resid incwage_fc_resid incwage_c_resid incwage_nc_resid lrent lmortamt1 lpop
	
keep year cbsa `vars'
reshape wide `vars' ///
	, i(cbsa) j(year)
	
foreach v of local vars {
	gen dl9080_`v' = `v'1990 - `v'1980
	gen dl9000_`v' = `v'2000 - `v'1990
	gen dl1505_`v' = `v'2015 - `v'2005
}


*change in migration share vs change in lincome and change in sd lincome
reg dl9000_sh_mig5 dl9000_lincwage
reg dl9000_sh_mig5 dl9000_sd_lincwage 
reg dl9000_sh_mig5 dl9000_sd_lincwage dl9000_lincwage

reg dl9080_sh_mig5 dl9080_lincwage
reg dl9080_sh_mig5 dl9080_sd_lincwage 
reg dl9080_sh_mig5 dl9080_sd_lincwage dl9080_lincwage

reg dl1505_sh_mig1 dl1505_lincwage
reg dl1505_sh_mig1 dl1505_sd_lincwage 
reg dl1505_sh_mig1 dl1505_sd_lincwage dl1505_lincwage


