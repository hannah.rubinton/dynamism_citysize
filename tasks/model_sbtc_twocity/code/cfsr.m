function [sr,Pexit,zx] = cfsr(city,city_T,pm_T,j)


%% what would the counterfactual start up rate be if you could hold congestion (rent) constant 
fo = firm_decisions(city_T.mu,city_T.Y,city_T.wh,city_T.wl,city.r,city_T.Hd,city_T.Ld,pm_T,j);
a = fo.pi_no_adoption < fo.pi_adopt;
pi = fo.pi_no_adoption.*(1-a) + fo.pi_adopt.*(a);

%find value function and zx
v = vfunciteration(pi,pm_T);
assert(min(v')==0,'no exit');
zx_ind = find(v==0,1,'last');
zx = pm_T.zgrid(zx_ind);
%assert(pm.ze>=zx,'entry z is less than exit z')
x = v<=0;

%find firm distribution 
g = pm_T.G_zx{zx_ind}; %solve_firm_distribution(zx,pm);

%firm output decision 
fo = firm_decisions(city_T.mu,city_T.Y,city_T.wh,city_T.wl,city.r,city_T.Hd,city_T.Ld,pm_T,j,x,a);

%mass of firms from P=1
M = 1/(sum(fo.p(x==0).^(1-pm_T.sig).*g(x==0)));
%M = city_T.M
%mass of entrants = mass of firms who hit the exit threshold next period
%probability of successful entry
p_success = sum(pm_T.Pentry.*(1-x));
%the mass of entrants will be E=Mass_attempted*p_success
%we need E=pexit so scale pexit by 1/p_success
scale_for_success = 1/p_success;
E = M*sum(pm_T.P'*g .*x)*scale_for_success;

sr = E/M;
Pexit = sum(pm_T.P'*g .*x);

end
