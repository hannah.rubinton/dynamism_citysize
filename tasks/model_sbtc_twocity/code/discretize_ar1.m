%% THis program discretizes the ar1 process and produces a productivity transition matrix P
% %It also discretizes a entry productivity distribution and creates a
% vector of entry poductivities Pentry which gives the probability that a
% firm enters at each point of the z-grid

% %ben uses an AR1 process for productivity shocks 
% %log(s_t+1) = (1-rho9)*sbar + rhop*log(s_t) + sig_eps*epsilon

%% discretize ar1
midpoints(2:pm.N_z) = (pm.zgrid(2:end) + pm.zgrid(1:end-1))./2;
midpoints(1) = pm.zgrid(1);

P = zeros(pm.N_z,pm.N_z);


%normal distribution 
%want to calculate the probability that log(z') < Z
%and we know that log(z')~N((1-rho)*zbar + rho*log(zi) , sig_eps)
%so z' is distributed log normally 
lnormcdf = @(zj,zi) 1/2 * ...
    (1+erf((log(zj)-(1-pm.rhop)*pm.zbar - pm.rhop*log(zi))/sqrt(pm.sig_eps^2*2)));

for i = 1:pm.N_z 
    for j=2:pm.N_z-1
        P(i,j) = lnormcdf(midpoints(j+1),pm.zgrid(i)) - lnormcdf(midpoints(j),pm.zgrid(i));             
    end
    P(i,1) = lnormcdf(midpoints(2),pm.zgrid(i));
    P(i,pm.N_z) = 1 - lnormcdf(midpoints(pm.N_z),pm.zgrid(i));
end



%% discretize entry 
%pareto cdf
Ge = @(z) 1-(zmin_entry/z)^pm.nu;
Pentry = zeros(size(pm.zgrid));

%find index of firms zgrid point with non-zero entry prob
idx_entry = find(pm.zgrid>zmin_entry,1);

Pentry(idx_entry) = Ge(midpoints(idx_entry+1));
Pentry(pm.N_z) = 1 - Ge(midpoints(pm.N_z));

for i = idx_entry+1:pm.N_z-1
    Pentry(i) = (Ge(midpoints(i+1)) - Ge(midpoints(i)));
end
