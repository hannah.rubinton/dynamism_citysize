%solve for the equilibrium in each city given wages in the city
function [city] = solve_city(H,L,pm,j)



%inner loop, solve for wages

if j==1 
   % guess = [16;10;.1];
   guess = pm.initialguess_prices(1,1:end)';
else
    %guess = [8;5;.1];
    guess = pm.initialguess_prices(2,1:end)';
end
options = optimset('Display','notify','TolX',1e-10);
func = @(in) find_prices(in(1),in(2),in(3),H,L,pm,j);
out = fsolve(func,guess,options);
wh = out(1);
wl = out(2);
r=out(3);
%wh = whwl*wl;
[excess_labor_demand,Y,mu]=find_prices(wh,wl,r,H,L,pm,j)
%error_in_free_entry = find_r(r,wh,wl,Y,pm,j)


[fo] = firm_decisions(mu,Y,wh,wl,r,H,L,pm,j); 
if isnan(pm.fa(j)) 
    a = zeros(size(pm.zgrid));
    pi = fo.pi_no_adoption;
else 
    a = fo.pi_no_adoption < fo.pi_adopt;
    pi = fo.pi_no_adoption.*(1-a) + fo.pi_adopt.*(a);
end

%find value function and zx
v = vfunciteration(pi,pm);
assert(min(v')==0,'no exit');
zx_ind = find(v==0,1,'last');
zx = pm.zgrid(zx_ind);
%assert(pm.ze>=zx,'entry z is less than exit z')
x = v<=0;

%find firm distribution 
g = pm.G_zx{zx_ind}; %solve_firm_distribution(zx,pm);

%firm output decision 
[fo] = firm_decisions(mu,Y,wh,wl,r,H,L,pm,j,x,a);

%mass of firms from P=1
M = 1/(sum(fo.p(x==0).^(1-pm.sig).*g(x==0)));
%mass of entrants = mass of firms who hit the exit threshold next period
%probability of successful entry
p_success = sum(pm.Pentry.*(1-x));
%the mass of entrants will be E=Mass_attempted*p_success
%we need E=pexit so scale pexit by 1/p_success
scale_for_success = 1/p_success;
E = M*sum(pm.P'*g .*x)*scale_for_success; 
Pexit = sum(pm.P'*g .*x);

%use hicksian labor demand to solve for Hd and Ld 
if pm.fixed_and_entry_costs == "labor"
    Hd = M*sum(fo.hd.*fo.q.*g) + M*(1-mu)*pm.fc(j) + M*mu*pm.fa(j) + E*pm.fe(j);
    Ld = M*sum(fo.ld.*fo.q.*g) + M*(1-mu)*pm.fc(j) + M*mu*pm.fa(j) + E*pm.fe(j);
else 
    Hd = M*sum(fo.hd.*fo.q.*g); % + M*(1-mu)*pm.fc + M*mu*pm.fa + E*pm.fe;
    Ld = M*sum(fo.ld.*fo.q.*g); % + M*(1-mu)*pm.fc + M*mu*pm.fa + E*pm.fe;
end
Ytest = sum(M.*(fo.q).^((pm.sig-1)/pm.sig).*g)^(pm.sig/(pm.sig-1)); 
assert(abs(Y-Ytest)<1e-6,'test that implied Ys are equal')

%function output
city.Hd = Hd;
city.Ld = Ld;
city.x = x;
city.a = a;
city.g = g;
city.v = v; 
city.a = a;
city.mu = mu; 
city.x = x;
city.E = E;
city.M = M;
city.Pexit = Pexit;
city.Y = Y;
city.wh = wh;
city.wl = wl; 
city.r = r;

