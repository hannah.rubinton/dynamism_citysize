function v = vfunciteration(profits,pm)
v=profits;

%iterate
diff=1;
tol=.000001;
while diff>tol
    EV = pm.P*v; %expected value next period
    v_max = max(0,profits + pm.rho*EV); % v is max of 0 and profits + expected future value
    
    diff = max(abs(v_max-v)) ;
    v = v_max; %update guess

end