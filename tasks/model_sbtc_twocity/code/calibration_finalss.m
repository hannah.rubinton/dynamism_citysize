function [diff,city1_T,city2_T, pm] = calibration_finalss(gm,al,fa,pm,city1,city2)
%% Calibration of second steady state
pop_0 = pm.H + pm.L;
pm.H = .54;
pm.L = 1.19;
pop_T = pm.H + pm.L;
pm.popgr = (pop_T-pop_0)/pop_0
pm.b = pm.b.*(1+pm.popgr)
pm.sk_0 = [city1.Hd/city1.Ld, city2.Hd/city2.Ld];
%city agglomeration, amenities and congestion
%pm.psi = [1.08,1] %agglomeration (need to change the name)
pm.rho_mu = 0;
pm.psi_a = @(j) pm.psi(j).*al;
pm.gmHprime = gm.*[1,1];
pm.gmLprime = (1-gm).*[1,1];
pm.fa = [fa,fa]; 





%% solve model 
pm.initialguess_prices = al.*[city1.wh, city1.wl, city1.r; ...
                          city2.wh, city2.wl, city2.r];
optset('broyden','tol',1e-4);
func =@(in) equate_utility(in(1),in(2),pm)
out = broyden(func,[pm.H*2/3;pm.L/2])
H1_T = out(1);
L1_T = out(2);
[util_diff,city1_T,city2_T]=equate_utility(H1_T,L1_T,pm)



%% calibration 
m.Hgr = 23;
m.Lgr = -4;
m.estabspercap = .045;

wh_0 = (city1.wh*city1.Hd + city2.wh*city2.Hd)/(city1.Hd+city2.Hd);
wh_T = (city1_T.wh*city1_T.Hd + city2_T.wh*city2_T.Hd)/(city1_T.Hd+city2_T.Hd);
wl_0 = (city1.wl*city1.Ld + city2.wl*city2.Ld)/(city1.Ld+city2.Ld);
wl_T = (city1_T.wl*city1_T.Ld + city2_T.wl*city2_T.Ld)/(city1_T.Ld+city2_T.Ld);

Hgr = (wh_T - wh_0)/wh_0*100;
Lgr = (wl_T - wl_0)/wl_0*100;
MperCap = (city1_T.M+city2_T.M)/(pm.H+pm.L);

diff = sum([(Hgr - m.Hgr)^2; ...
        (Lgr - m.Lgr)^2; ...
        (MperCap - m.estabspercap)^2]);
    
end


