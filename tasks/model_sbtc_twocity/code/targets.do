
cap log close
clear
set more off

global mergeddata "/Users/hannah/Dropbox/dynamism_citysize/tasks/merge_cbsa_data/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/model_sbtc_twocity"

use "$mergeddata/sr_cbsa.dta", clear

*drop if sr<.04 | sr>.21
drop if sr==.
drop if citysizecat==1

gen srd = firms_ageg1 / denom_firms
bys cbsa: gen srd_3yma = (srd[_n] + srd[_n-1] + srd[_n-2])/3

sort cbsa year
bys cbsa: gen sr_3yma = (sr[_n] + sr[_n-1] + sr[_n-2])/3
bys cbsa: gen esr_3yma = (esr[_n] + esr[_n-1] + esr[_n-2])/3
bys cbsa: gen fer_3yma = (fer[_n] + fer[_n-1] + fer[_n-2])/3
bys cbsa: gen eer_3yma = (eer[_n] + eer[_n-1] + eer[_n-2])/3




drop lq*
drop job_*
drop sh_emp*
drop if year==.
keep if inlist(year,1980,1990,2000,2007,2013)


gen citysizecat_2cities = 1 if inrange(citysizecat1980,6,7)
replace citysizecat_2cities = 0 if inrange(citysizecat1980,1,5)

tempfile bycity 
save `bycity'

gen cities = 1
collapse  CtoH2 incwage incwage_college incwage_nc srd_3yma  sr_3yma esr_3yma fer_3yma eer_3yma /// 
	jcr jdr  HPI /// 
	(sum) cities clf firms estabs wapop  pop emp firms_ageg1 estabs_ageg1 emp_ageg1, by(year)
	

tempfile bycitysizecat
save `bycitysizecat' 


**aggregate changes 
use `bycity'
gen cities = 1
collapse  CtoH2 incwage incwage_college incwage_nc srd_3yma  sr_3yma esr_3yma fer_3yma eer_3yma /// 
	jcr jdr  HPI /// 
	(sum) cities clf firms estabs wapop  pop emp firms_ageg1 estabs_ageg1 emp_ageg1, by(year)

gen citysizecat_2cities = 100


append using `bycitysizecat'
order year citysizecat


gen sr1 = firms_ageg1 / firms
gen esr1 = estabs_ageg1 / estabs
*sz entrants
gen sz_ageg1 = emp_ageg1 / estabs_ageg1
gen sz = emp/estabs
gen relativeszentrants = sz_ageg1 / sz
sum relativeszentrants, d
gen relative_wages = incwage_college / incwage_nc

*firms per capita
gen estabsperpop = estabs / pop
gen estabsperwapop = estabs / wapop
gen estabsperclf = estabs / clf
gen estabsperemp = estabs / emp



sort citysizecat year
by citysizecat: gen Hwagegrowth = (incwage_college[_n] - incwage_college[_n-3])/incwage_college[_n-3]*100
by citysizecat: gen Lwagegrowth = (incwage_nc[_n] - incwage_nc[_n-3])/incwage_nc[_n-3]*100










