function [diff,mu] = find_Y(Y,wh,wl,r,pm,j)

if Y<=0 
    diff=[-Inf]
    return
end
%solve for mu
if isnan(pm.fa(j))
    mu=0;
elseif pm.rho_mu == 0
    mu=Inf;
else
    func = @(in) find_mu(in,Y,wh,wl,r,pm,j);
    if func(1)==1
        mu=1;
    elseif func(0)==0
        mu=0;
    else
        mu=bisect(func,0,1);
    end
end
%options = optimset('Display','off');


% find_mu_x = linspace(0,1,1000);
% find_mu_y = zeros(size(find_mu_x));
% for idx = 1:1000
%    find_mu_y(idx) = func(find_mu_x(idx));
% end
% plot(find_mu_x,find_mu_y)


%profits as a function of z 
[fo] = firm_decisions(mu,Y,wh,wl,r,pm,j); 
if mu>0 
    a = fo.pi_no_adoption < fo.pi_adopt;
    pi = fo.pi_no_adoption.*(1-a) + fo.pi_adopt.*(a);
else 
    a = zeros(size(pm.zgrid));
    pi = fo.pi_no_adoption;
end
    
%find value function and zx
v = vfunciteration(pi,pm);
%assert(min(v)==0,'no exit');
zx_ind = find(v==0,1,'last');
zx = pm.zgrid(zx_ind);
%assert(pm.ze>=zx,'entry z is less than exit z')
x = v<=0;


%exit for all states, Y needs to be higher. Free entry condition is not
%holding with equality we have Ve<w*fe 
if all(x==1)
    diff = -Inf;
    return
end

%value at entry
Ve = sum(v.*pm.Pentry);


%% equil condition
if pm.fixed_and_entry_costs == "labor"
    diff = [Ve-(wh+wl)*pm.fe(j)];
else
   diff = [Ve-pm.fe(j)]; 
end

