function [fo] = profits(mu,Y,wh,wl,r,pm,j,varargin)


%firm output decision 
ucf_no_adoption = (pm.A(j).*zgrid).^-1.*(pm.gmH(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmL(j)^pm.eps*wl^(1-pm.eps))^(1/(1-pm.eps));
ucf_adopt = (pm.Agg(mu).*pm.zgrid).^-1.*(pm.gmHprime(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmLprime(j)^pm.eps*wl^(1-pm.eps))^(1/(1-pm.eps));
p_no_adoption = ucf_no_adoption.*pm.sig./(pm.sig-1);
p_adopt = ucf_adopt.*pm.sig./(pm.sig-1); 
q_no_adoption = Y.*p.^(-pm.sig);
q_adopt = Y.*p_adopt.^(-pm.sig);

pi_no_adoption = p_no_adoption*q_no_adoption/pm.sigma - pm.fc - r*pm.fb;
pi_adopt = p_adopt*q_adopt/pm.sigma - pm.fa - r*pm.fb;


%firm level labor demand, depends on adoption
hd_no_adoption = wh^(-pm.eps)*pm.gmH(j)^pm.eps*(pm.A(j).*zgrid).^-1.*(pm.gmH(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmL(j)^pm.eps*wl^(1-pm.eps))^(pm.eps/(pm.eps-1));
hd_adopt =  wh^(-pm.eps)*pm.gmHprime(j)^pm.eps*(pm.Agg(mu).*zgrid).^-1.*(pm.gmHprime(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmLprime(j)^pm.eps*wl^(1-pm.eps))^(pm.eps/(pm.eps-1));

ld_no_adoption = wl^(-pm.eps)*pm.gmL(j)^pm.eps*(pm.A(j).*zgrid).^-1.*(pm.gmH(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmL(j)^pm.eps*wl^(1-pm.eps))^(pm.eps/(pm.eps-1));
ld_adopt =  wl^(-pm.eps)*pm.gmLprime(j)^pm.eps*(pm.Agg(mu).*zgrid).^-1.*(pm.gmHprime(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmLprime(j)^pm.eps*wl^(1-pm.eps))^(pm.eps/(pm.eps-1));

fo.ucf_no_adoption = ucf_no_adoption;
fo.ucf_adopt = ucf_adopt;
fo.p_no_adoption = p_no_adoption;
fo.p_adopt = p_adopt;
fo.q_no_adoption = q_no_adoption;
fo.q_adopt = q_adopt;
fo.pi_no_adoption = pi_no_adoption;
fo.pi_adopt = pi_adopt;


if nargin >7
    x = varargin{1};
    a = varargin{2};
    
    fo.q = q_no_adoption.*(1-a).*(1-x) + q_adopt.*a.*(1-x);
    fo.pi = pi_no_adoption.*(1-a).*(1-x) + pi_adopt.*a.*(1-x);
    fo.ld = ld_no_adoption.*(1-a).*(1-x) + ld_adopt.*a.*(1-x);
    fo.hd = hd_no_adoption.*(1-a).*(1-x) + hd_adopt.*a.*(1-x); 

end


end