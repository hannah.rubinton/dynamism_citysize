function [g] = solve_firm_distribution(zx,pm)
    
    if zx>0
        zx_ind = find(zx>=pm.zgrid,1,'last');
        x = zx>=pm.zgrid;
    else
        zx_ind = 0;
        x = zx>=pm.zgrid;
    end
    
   
    %{
    %This version works when entry happens at the point ze with prob = 1
    %now I'm changing the code so that there is a distribution of entry
    
    %P with entry and exit 
    Pa = pm.P;
    for i = 1:pm.N_z 
        Pa(i,:)=pm.P(i,:).*(1-x');
        Pa(i,pm.ze_ind) = Pa(i,pm.ze_ind) + sum(pm.P(i,:).*x');
    end

    assert(all(sum(Pa')-1<1^-15),'new probabilities should sum to 1')


    %steady state distribution 
    %Pa'*g_{t} = g_{t+1}
    %looking for steady state distribution where Pa'*g=g
    %so looking for the eigenvector associated with the eigenvalue of 1
    [V,D]=eig(Pa');
    [gind,~]=find(abs(D-1)<.00001);
    gind =min(gind);
    assert(all(V(:,gind)>=-eps),'eigenvector not right')
    g_test = V(:,gind).*(V(:,gind)>=0)/sum(V(:,gind).*(V(:,gind)>=0));
    %}
% test equil distribution 
%g = g_test;


%% solve by iterating till it converges
%probability of successful entry
p_success = sum(pm.Pentry.*(1-x));
%the mass of entrants will be E=Mass_attempted*p_success
%we need E=pexit so scale pexit by 1/p_success
scale_for_success = 1/p_success;

max_its=5000;
tol = 1e-6;
g=pm.Pentry; %zeros(size(pm.zgrid))+1/pm.N_z;
for its = 1:max_its
    its;
    gprime = pm.P'*g .*(1-x);
    pexit = sum(pm.P'*g .*x);
    if pexit>0
     gprime = gprime+pm.Pentry.*(1-x)*pexit*scale_for_success;
    end
        %dbstop if abs(sum(gprime)-1)>1e-4;
    
    assert(abs(sum(gprime)-1)<1e-4,'g does not sum to 1')
    %gprime = gprime ./ sum(gprime);
    err = sum(abs(g-gprime));
    
    if abs(err)<tol
        break
    else
        assert(its<max_its,'firm distribution does not converge')
        g=gprime;
    end
end %end loop over firm distribution

%assert(all(abs(g-g_test)<1e-5),'two methods for calculating firm distribution do not give same answer')
