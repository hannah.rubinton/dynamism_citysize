function [x0] = bisect_with_interpolation(x,y,a,b)

tol = 1e-3;
err=1;
max_its=1000;

for i = 1:max_its
    
    fa = interp1(x,y,a);
    fb = interp1(x,y,b);
    fapos = fa>0;
    
    mid = (a + b)/2;
    fmid = interp1(x,y,mid);
    
    if fmid>0 && fapos
        a = mid;
    elseif fmid<0 && fapos
        b = mid;
    elseif fmid>0 && ~fapos
        b = mid;
    else
        a = mid;
    end
        
        
    err = abs(a-b); 
    if err<tol
        break
    end
    
    
end

x0 = a;
y0 = interp1(x,y,x0);
assert(abs(y0)<1e-3,'not finding p=1')
if abs(y0)<1e-3
    sprintf('find y converged, y= %f',x0)
end
