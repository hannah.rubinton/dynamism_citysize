function [diff,r] = find_r(r,wh,wl,Y,pm,j)

if r<=0
    diff = Inf;
    return;
end

%profits as a function of z 
[fo] = firm_decisions(Y,wh,wl,r,pm,j); 
if isnan(pm.fa(j)) 
    a = zeros(size(pm.zgrid));
    pi = fo.pi_no_adoption;
else 
    a = fo.pi_no_adoption < fo.pi_adopt;
    pi = fo.pi_no_adoption.*(1-a) + fo.pi_adopt.*(a);
end

%find value function and zx
v = vfunciteration(pi,pm);
r;
assert(min(v)==0,'no exit');
if min(v)==0
    zx_ind = find(v==0,1,'last');
    zx = pm.zgrid(zx_ind);
    %assert(pm.ze>=zx,'entry z is less than exit z')
    x = v<=0;
else
    zx_ind = 0;
    zx = 0;
    x = v<=0;
end

[fo] = firm_decisions(Y,wh,wl,r,pm,j,x,a); 
g = pm.G_zx{zx_ind}; %solve_firm_distribution(zx,pm);

%mass of firms from P=1
M = 1/(sum(fo.p(x==0).^(1-pm.sig).*g(x==0)));


%value at entry
Ve = sum(v.*pm.Pentry);


Bs = pm.fb(j)*r^(1/(pm.eta(j)-1));
Bd = M*pm.fe(j);

%% equil condition
if pm.fixed_and_entry_costs == "labor"
    diff = [Ve-(wh+wl)*pm.fe(j)];
else
   diff = [Ve-pm.fe(j)]; 
end
%diff = [Bs-Bd];