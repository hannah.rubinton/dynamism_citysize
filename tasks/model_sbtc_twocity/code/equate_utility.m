%solve for excess labor demand in city 2
function [util_indif,city1,city2] = equate_utility(H1,L1,pm)

if H1>pm.H || L1>pm.L
    util_indif = [-Inf, -Inf];
    return
end
if H1<0 || L1<0
    util_indif = [Inf, Inf];
    return
end


%solve city 1
city1 = solve_city(H1,L1,pm,1); 

%use the labor market clearing condition to solve for labor in city 2
H2 = pm.H - H1;
L2 = pm.L - L1;


%solve city 2
city2 = solve_city(H2,L2,pm,2);


%use the indifference condition ot solve for wh2 and wl2 given the labor
%allocation and the wages in city 1 
Ch1 = pm.alpha(1)*log(H1 + L1);
Ch2 = pm.alpha(1)*log(H2 + L2); 
Cl1 = pm.alpha(2)*log(H1 + L1);
Cl2 = pm.alpha(2)*log(H2 + L2);  
% Uh1 = log(pm.Ah1) + log(city1.wh) - Ch1;
% Uh2 = log(city2.wh) - Ch2;
% Ul1 = log(pm.Al1) + log(city1.wl) - Cl1;
% Ul2 = log(city2.wl) - Cl2;
% 
% city1.Uh = Uh1;
% city2.Uh = Uh2;
% city1.Ul = Ul1;
% city2.Ul = Ul2;

func_logistic = @(x,mu,beta) 1 - 1/(1+exp(-(x-mu)/beta));
Hshare1 = func_logistic(log(city2.wh)-log(city1.wh)-Ch2+Ch1-log(pm.Ah1),...
    0,pm.beta);

Lshare1 = func_logistic(log(city2.wl)-log(city1.wl)-Cl2+Cl1-log(pm.Al1),...
    0,pm.beta);



%util_indif = [Uh1 - Uh2; Ul1 - Ul2]

util_indif = [H1 - pm.H*Hshare1; L1 - pm.L*Lshare1]

end