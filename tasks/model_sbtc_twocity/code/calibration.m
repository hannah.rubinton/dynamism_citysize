usecluster = 1;
if usecluster ==1
    addpath('/home/hherman/matlab/compecon/CEtools')
    addpath('/scratch/network/hherman/lightspeed')
end

%% Calibration 
%other parameters 
pm.L = 1;
pm.H = .24;
%parameters of the production function
pm.sig = 4; %eos btw varieties 
pm.rho = .95; %discount rate
pm.eps = 1.62; %eos btw h and l
pm.zeta = [1,1]; %high and low sk
pm.alpha = [.9,.9]; %high and low sk 
pm.eta = [11,11]; %1/(eta-1)=elasticity of housing supply 
pm.fixed_and_entry_costs = "final good";
pm.beta =1; %scale of gumbel distribution

%city agglomeration, amenities and congestion
pm.psi = [1,1] %agglomeration (need to change the name)
pm.psi_a = @(j) pm.psi(j);
pm.gmHprime = [1.1,1.1];
pm.gmLprime = [1,1];
pm.fa = [NaN,NaN]; 

% zgrid 
%zgrid 
pm.N_z = 550; zmin=.001; zmax=550;
%zgrid = (exp(exp(linspace(zmin,log(log(zmax)),N_z))-1)-1)';
pm.zgrid = (exp(linspace(zmin,log(zmax),pm.N_z))-1)';

pm.sig_eps = .262;
pm.rhop=.98;
pm.zbar =0;
zmin_entry = .5;

%% moments
moments.wh1wl2 = 1.86;
moments.wl1wl2 = 1.18;
moments.wh2wl2 = 1.54;
moments.H = .24;
moments.L = 1;
moments.H1L1 = .26;
moments.H2L2 = .21;
moments.r1wl2 = 3.16;
moments.r2wl2 = 1.92
moments.firmspercapita1 = .041;
moments.firmspercapita2 = .042;
moments.relativeszentrants1 = .31;
moments.relativeszentrants2 = .35;
moments.sr1 = 14.5;
moments.sr2 = 14.2; 

%% guess nu
pm.nu = 1.45; %tail index for pareto distn for entry
%transition matrix
discretize_ar1; 
pm.P=P;
pm.Pentry=Pentry;

%solve for the firm distribution for each possible zx
G_zx{pm.N_z} = zeros(size(pm.zgrid));
for idx = 1:(pm.N_z -20)
   G_zx{idx} = solve_firm_distribution(pm.zgrid(idx),pm); 
end


pm.G_zx = G_zx;


%% use start up rate moment to solve for zx and g()
%guess zx 

sr_cal = zeros(size(pm.zgrid));
for idx = 1:pm.N_z-1
g = solve_firm_distribution(pm.zgrid(idx),pm);
x = pm.zgrid<=pm.zgrid(idx);
p_success = sum(pm.Pentry.*(1-x));
%the mass of entrants will be E=Mass_attempted*p_success
%we need E=pexit so scale pexit by 1/p_success
scale_for_success = 1/p_success;
sr_cal(idx) = sum(pm.P'*g .*x)*scale_for_success;
end 

%city1
    zx_ind1 = find(moments.sr1<sr_cal.*100,1)-1;
    zx1 = pm.zgrid(zx_ind1);
    g1 = pm.G_zx{zx_ind1}; % solve_firm_distribution(zx1,pm);
    x1 = pm.zgrid<=zx1;
    p_success1 = sum(pm.Pentry.*(1-x1));
    %the mass of entrants will be E=Mass_attempted*p_success
    %we need E=pexit so scale pexit by 1/p_success
    scale_for_success1 = 1/p_success1;
    sr1 = sum(pm.P'*g1 .*x1)*scale_for_success1;

%city2 
    zx_ind2 = find(moments.sr2<sr_cal.*100,1)-1;
    zx2 = pm.zgrid(zx_ind2);
    g2 = pm.G_zx{zx_ind2}; % solve_firm_distribution(zx2,pm);
    x2 = pm.zgrid<=zx2;
    p_success2 = sum(pm.Pentry.*(1-x2));
    %the mass of entrants will be E=Mass_attempted*p_success
    %we need E=pexit so scale pexit by 1/p_success
    scale_for_success2 = 1/p_success2;
    sr2 = sum(pm.P'*g2 .*x2)*scale_for_success2;
    
%solve for H2, L2, H1, L1, M1, M2 using the moments
L2 = (moments.H1L1 - moments.H)/(moments.H1L1-moments.H2L2);
L1 = moments.L - L2;
H2 = moments.H2L2*L2;
H1 = moments.H - H2;
M1 = moments.firmspercapita1*(H1+L1);
M2 = moments.firmspercapita2*(H2+L2);

%% solve for gammaH2 
%use H/L=(wh/wl)^-eps*(gmH/gmL)^eps
pm.gmL(2) = 1/(moments.H2L2^(1/pm.eps)*moments.wh2wl2+1);
pm.gmH(2) = 1-pm.gmL(2);

%solve for wl2 using P=1
wl2 = (M2*((pm.gmH(2)^pm.eps*(moments.wh2wl2)^(1-pm.eps)+pm.gmL(2)^pm.eps)^(1/(1-pm.eps))*(pm.sig/(pm.sig-1)))^(1-pm.sig)...
    *sum(1./pm.zgrid.^(1-pm.sig).*g2))^(-1/(1-pm.sig));
wh2 = moments.wh2wl2*wl2;
wh1 = moments.wh1wl2*wl2;
wl1 = moments.wl1wl2*wl2;
r1 = moments.r1wl2*wl2;
r2 = moments.r2wl2*wl2;


%this version normalizes gmL=1 in both cities and psi=1 in city 2
%need to solve for gmH in both cities and psi in city 1
pm.gmL(1) = 1/(moments.H1L1^(1/pm.eps)*wh1/wl1+1);
pm.gmH(1) = 1-pm.gmL(1);
pm.psi(2) = 1;

%use P=1 to solve for psi in city 1
pm.psi(1) = (M1*((pm.gmH(1)^pm.eps*(wh1)^(1-pm.eps)+pm.gmL(1)^pm.eps*wl1^(1-pm.eps))^(1/(1-pm.eps))*(pm.sig/(pm.sig-1)))^(1-pm.sig)...
     *sum(1./pm.zgrid.^(1-pm.sig).*g1))^(1/(1-pm.sig));



%prices
p1 = (pm.psi(1).*pm.zgrid).^-1.*(pm.gmH(1)^pm.eps*wh1^(1-pm.eps)+pm.gmL(1)^pm.eps*wl1^(1-pm.eps))^(1/(1-pm.eps)).*pm.sig./(pm.sig-1);
p2 = (pm.psi(2).*pm.zgrid).^-1.*(pm.gmH(2)^pm.eps*wh2^(1-pm.eps)+pm.gmL(2)^pm.eps*wl2^(1-pm.eps))^(1/(1-pm.eps)).*pm.sig./(pm.sig-1);


%labor demand 
h1 = wh1^-pm.eps*pm.gmH(1)^pm.eps/(pm.gmH(1)^pm.eps*wh1^(1-pm.eps)+pm.gmL(1)^pm.eps*wl1^(1-pm.eps))^(pm.eps/(pm.eps-1))./pm.zgrid./pm.psi(1);
h2 = wh2^-pm.eps*pm.gmH(2)^pm.eps/(pm.gmH(2)^pm.eps*wh2^(1-pm.eps)+pm.gmL(2)^pm.eps*wl2^(1-pm.eps))^(pm.eps/(pm.eps-1))./pm.zgrid./pm.psi(2);
l1 = wl1^-pm.eps*pm.gmL(1)^pm.eps/(pm.gmH(1)^pm.eps*wh1^(1-pm.eps)+pm.gmL(1)^pm.eps*wl1^(1-pm.eps))^(pm.eps/(pm.eps-1))./pm.zgrid./pm.psi(1);
l2 = wl2^-pm.eps*pm.gmL(2)^pm.eps/(pm.gmH(2)^pm.eps*wh2^(1-pm.eps)+pm.gmL(2)^pm.eps*wl2^(1-pm.eps))^(pm.eps/(pm.eps-1))./pm.zgrid./pm.psi(2);


%use labor market clearing to solve for Y 
Y1 = H1/(sum(h1.*p1.^-pm.sig.*g1)*M1);
Y2 = H2/(sum(h2.*p2.^-pm.sig.*g2)*M2);


%test this using L
Y1test = L1/(sum(l1.*p1.^-pm.sig.*g1)*M1);
Y2test = L2/(sum(l2.*p2.^-pm.sig.*g2)*M2);


%use the building market clearing condition to solve for fb
pm.b(1)=.001;
pm.fb(1) = pm.b(1)*r1^(1/(pm.eta(1)-1))/M1;
pm.fb(2) = pm.fb(1); 
%pm.fb(2) = r2^(1/(pm.eta(2)-1))/M2;
pm.b(2) = M2*pm.fb(2)/r2^(1/(pm.eta(2)-1));
%pm.b(1)=1;

%solve for fc using profit maximization 
%firm output decision 
q1 = Y1.*p1.^(-pm.sig);
q2 = Y2.*p2.^(-pm.sig);
varpi1 = p1.*q1/pm.sig;
varpi2 = p2.*q2/pm.sig;

cpm1 = @(fc) calibration_profit_max(fc,zx1,r1,pm.fb(1),varpi1,pm)
cpm2 = @(fc) calibration_profit_max(fc,zx2,r2,pm.fb(2),varpi2,pm)

fc_x = linspace(0,3,100);
for idx = 1:100
   fc_y1(idx) = cpm1(fc_x(idx)); 
   fc_y2(idx) = cpm2(fc_x(idx)); 

end
pm.fc(1) = fc_x(find(fc_y1>=0,1,'last'))
pm.fc(2) = fc_x(find(fc_y2>=0,1,'last'))
%pm.fc(1) = bisect(cpm1,eps,.1)
%pm.fc(2) = bisect(cpm2,eps,.1)
pi1 = varpi1 - r1*pm.fb(1) - pm.fc(1);
pi2 = varpi2 - r2*pm.fb(2) - pm.fc(2);
v1 = vfunciteration(pi1,pm);
v2 = vfunciteration(pi2,pm);


%solve for fe using free entry 
pm.fe(1) = sum(v1.*pm.Pentry);
pm.fe(2) = sum(v2.*pm.Pentry);

%% solve for amenitites using utility equalization
Ch1 = pm.alpha(1)*log(H1 + L1);
Ch2 = pm.alpha(1)*log(H2 + L2); 
Cl1 = pm.alpha(2)*log(H1 + L1);
Cl2 = pm.alpha(2)*log(H2 + L2); 
% pm.Ah1 = exp(log(wh2) - log(wh1) + Ch1 - Ch2);
% pm.Al1 = exp(log(wl2) - log(wl1) + Cl1 - Cl2);

%instead solve for A1 using the A1 that gives 
%H1 = P(zeta_i1 - zeta_i2>Cbar)H where the distribution is logistic
func_logistic = @(x,mu,beta) 1 - 1/(1+exp(-(x-mu)/beta));
func_Ah1 = @(Ah1) pm.H*func_logistic(log(wh2)-log(wh1)-pm.alpha(1)*log(H2+L2)+pm.alpha(1)*log(H1+L1)-log(Ah1),...
    0,pm.beta) - H1
pm.Ah1 = fsolve(func_Ah1,2)

func_Al1 = @(Al1) pm.L*func_logistic(log(wl2)-log(wl1)-pm.alpha(2)*log(H2+L2)+pm.alpha(2)*log(H1+L1)-log(Al1),...
    0,pm.beta) - L1
pm.Al1 = fsolve(func_Al1,2)

% %% instead of matching the moments perfectly set fixed costs equal in both cities
%  pm.fc = [1,1].*mean(pm.fc);
%  pm.fe = [1,1].* mean(pm.fe);
%  pm.b = [1,1].*mean(pm.b);
 

%% size of entrants vs. size of all firms 
sz1 = (h1+l1).*q1;
sz2 = (h2+l2).*q2;
avg_sz1 = sum(sz1.*g1);
avg_sz2 = sum(sz2.*g2);
avg_sz_entrants1 = sum(sz1.*pm.Pentry.*(pm.zgrid>zx1))
avg_sz_entrants2 = sum(sz2.*pm.Pentry.*(pm.zgrid>zx2))
relative_sz_entrants1 = avg_sz_entrants1/avg_sz1
relative_sz_entrants2 = avg_sz_entrants2/avg_sz2


%% solve model 
pm.initialguess_prices = [wh1, wl1,r1; ...
                          wh2, wl2,r2];
pm.initialguess_Y = [Y1; Y2];
optset('broyden','tol',1e-4);
func =@(in) equate_utility(in(1),in(2),pm)
out = broyden(func,[.18;.72])
H1 = out(1);
L1 = out(2);
[util_diff,city1,city2]=equate_utility(H1,L1,pm)



excess_labor_demand = [city1.Hd + city2.Hd - pm.H, city1.Ld + city2.Ld - pm.L]
excess_supply_buildings = [pm.b(1)*city1.r^(1/(pm.eta(1)-1)) - pm.fb(1)*city1.M, ...
        pm.b(2)*city2.r^(1/(pm.eta(2)-1)) - pm.fb(2)*city2.M]
    

%% calibrate final steady state 
%calibration_finalss(gm,al,fa,pm,city1,city2)
[diff,city1_T,city2_T, pm_T] = calibration_finalss(.55,1.2,1.05*max(pm.fc),pm,city1,city2)

%func_calibrate = @(in) calibration_finalss(in(1),in(2),in(3),pm,city1,city2)
%out_pm = fsolve(func_calibrate,[.55;1.2;1.1*pm.fc(1)])
 
%[diff,city1_T,city2_T, pm_T] = calibration_finalss(out_pm(1),out_pm(2),out_pm(3),pm,city1,city2)
excess_supply_buildings_T = [(pm_T.b(1)*city1_T.r^(1/(pm_T.eta(1)-1)) - pm_T.fb(1)*city1_T.M)/pm_T.fb(1)*city1_T.M, ...
        (pm_T.b(2)*city2_T.r^(1/(pm_T.eta(2)-1)) - pm_T.fb(2)*city2_T.M)/pm_T.fb(2)*city2_T.M]
excess_labor_demand_T = [city1_T.Hd + city2_T.Hd - pm_T.H, city1_T.Ld + city2_T.Ld - pm_T.L]

% results
sk_premium_0 = [city1.wh/city1.wl, city2.wh/city2.wl]
sk_premium_T = [city1_T.wh/city1_T.wl, city2_T.wh/city2_T.wl]
city_size_premium_0 = [city1.wh/city2.wh, city1.wl/city2.wl]
city_size_premium_T = [city1_T.wh/city2_T.wh, city1_T.wl/city2_T.wl]
sk_intensity_0 = [city1.Hd/city1.Ld, city2.Hd/city2.Ld]
sk_intensity_T = [city1_T.Hd/city1_T.Ld, city2_T.Hd/city2_T.Ld]
city1_T.mu
city2_T.mu
pop1_0 = city1.Hd + city1.Ld;
pop1_T = city1_T.Hd + city1_T.Ld;
pop2_0 = city2.Hd + city2.Ld;
pop2_T = city2_T.Hd + city2_T.Ld;
pop_gr = [(pop1_T - pop1_0)/pop1_0, (pop2_T-pop2_0)/pop2_0]
rents_bywage_0 = [city1.r/city2.wl, city2.r/city2.wl]
rents_bywage_T = [city1_T.r/city2.wl, city2_T.r/city2.wl]
estabs_per_cap_0 =[city1.M/(city1.Hd+city1.Ld), city2.M/(city2.Hd+city2.Ld)]
estabs_per_cap_T =[city1_T.M/(city1_T.Hd+city1_T.Ld), city2_T.M/(city2_T.Hd+city2_T.Ld)]

wh_normalized_0 = [city1.wh/city2.wl, city2.wh/city2.wl]
wl_normalized_0 = [city1.wl/city2.wl, city2.wl/city2.wl]
wh_normalized_T = [city1_T.wh/city2.wl, city2_T.wh/city2.wl]
wl_normalized_T = [city1_T.wl/city2.wl, city2_T.wl/city2.wl]


sr_0 = [city1.E/city1.M, city2.E/city2.M]
sr_T = [city1_T.E/city1_T.M, city2_T.E/city2_T.M]



(city_size_premium_T - city_size_premium_0)./city_size_premium_0
(sk_premium_T - sk_premium_0)./sk_premium_0
(sk_intensity_T - sk_intensity_0)./sk_intensity_0


fo1 = firm_decisions(city1.mu,city1.Y,city1.wh,city1.wl,city1.r,city1.Hd,city1.Ld,pm,1,city1.x,city1.a);
fo2 = firm_decisions(city2.mu,city2.Y,city2.wh,city2.wl,city2.r,city2.Hd,city2.Ld,pm,2,city2.x,city2.a);
fo1_T = firm_decisions(city1_T.mu,city1_T.Y,city1_T.wh,city1_T.wl,city1_T.r,city1_T.Hd,city1_T.Ld,pm_T,1,city1_T.x,city1_T.a);
fo2_T = firm_decisions(city2_T.mu,city2_T.Y,city2_T.wh,city2_T.wl,city2_T.r,city2_T.Hd,city2_T.Ld,pm_T,2,city2_T.x,city2_T.a);

figure(1)
plot(pm.zgrid,city1.v,pm.zgrid,city2.v,pm.zgrid,city1_T.v,pm.zgrid,city2_T.v)
figure(2)
plot(pm.zgrid,fo1.pi,pm.zgrid,fo2.pi,pm.zgrid,fo1_T.pi,pm.zgrid,fo2_T.pi)

 
% what would the counterfactual start up rate be if you could hold congestion (rent) constant 
[cfsr1,cfPexit1,cfzx1] = cfsr(city1,city1_T,pm_T,1)
[cfsr2,cfPexit2,cfzx2] = cfsr(city2,city2_T,pm_T,2)


%% helper function 
function [diff] = calibration_profit_max(fc,zx,r,fb,varpi,pm)
    profits = varpi - fc - r*fb;
    v=vfunciteration(profits,pm);
    zx_vind = find(v==0,1,'last');
    zx_v = pm.zgrid(zx_vind);
    diff = zx-zx_v;
end






