function [fo] = firm_decisions(mu,Y,wh,wl,r,H,L,pm,j,varargin)

sk = H/L;

%firm output decision 
ucf_no_adoption = (pm.psi(j).*pm.zgrid).^-1.*(pm.gmH(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmL(j)^pm.eps*wl^(1-pm.eps))^(1/(1-pm.eps));
ucf_adopt = (pm.psi_a(j).*pm.zgrid).^-1.*(pm.gmHprime(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmLprime(j)^pm.eps*wl^(1-pm.eps))^(1/(1-pm.eps));
p_no_adoption = ucf_no_adoption.*pm.sig./(pm.sig-1);
p_adopt = ucf_adopt.*pm.sig./(pm.sig-1); 
q_no_adoption = Y.*p_no_adoption.^(-pm.sig);
q_adopt = Y.*p_adopt.^(-pm.sig);


%fc in final good or labor
if pm.fixed_and_entry_costs == "labor"
    FC = (wh+wl)*pm.fc(j);
    FA = (wh+wl)*pm.fa(j);
else 
    FC = pm.fc(j);
    FA = pm.fa(j);
end

%with and without rent
if isnan(r)
    pi_no_adoption = p_no_adoption.*q_no_adoption/pm.sig - FC;
    pi_adopt = p_adopt.*q_adopt/pm.sig - FA;
else
    pi_no_adoption = p_no_adoption.*q_no_adoption/pm.sig - FC - r*pm.fb(j);
    pi_adopt = p_adopt.*q_adopt/pm.sig - FA - r*pm.fb(j);
end

 
fo.varpi_no_adoption = p_no_adoption.*q_no_adoption/pm.sig;
fo.varpi_adopt = p_adopt.*q_adopt/pm.sig;


%firm level labor demand, depends on adoption
hd_no_adoption = wh^(-pm.eps)*pm.gmH(j)^pm.eps*(pm.psi(j).*pm.zgrid).^-1.*(pm.gmH(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmL(j)^pm.eps*wl^(1-pm.eps))^(-pm.eps/(pm.eps-1));
hd_adopt =  wh^(-pm.eps)*pm.gmHprime(j)^pm.eps*(pm.psi_a(j).*pm.zgrid).^-1.*(pm.gmHprime(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmLprime(j)^pm.eps*wl^(1-pm.eps))^(-pm.eps/(pm.eps-1));

ld_no_adoption = wl^(-pm.eps)*pm.gmL(j)^pm.eps*(pm.psi(j).*pm.zgrid).^-1.*(pm.gmH(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmL(j)^pm.eps*wl^(1-pm.eps))^(-pm.eps/(pm.eps-1));
ld_adopt =  wl^(-pm.eps)*pm.gmLprime(j)^pm.eps*(pm.psi_a(j).*pm.zgrid).^-1.*(pm.gmHprime(j)^pm.eps*wh^(1-pm.eps) ... 
    +pm.gmLprime(j)^pm.eps*wl^(1-pm.eps))^(-pm.eps/(pm.eps-1));

fo.ucf_no_adoption = ucf_no_adoption;
fo.ucf_adopt = ucf_adopt;
fo.p_no_adoption = p_no_adoption;
fo.p_adopt = p_adopt;
fo.q_no_adoption = q_no_adoption;
fo.q_adopt = q_adopt;
fo.pi_no_adoption = pi_no_adoption;
fo.pi_adopt = pi_adopt;


if nargin >9
    x = varargin{1};
    a = varargin{2};
    fo.p = p_no_adoption.*(1-a).*(1-x) + p_adopt.*a.*(1-x);
    fo.q = q_no_adoption.*(1-a).*(1-x) + q_adopt.*a.*(1-x);
    fo.pi = pi_no_adoption.*(1-a).*(1-x) + pi_adopt.*a.*(1-x);
    fo.ld = ld_no_adoption.*(1-a).*(1-x) + ld_adopt.*a.*(1-x);
    fo.hd = hd_no_adoption.*(1-a).*(1-x) + hd_adopt.*a.*(1-x); 

end


end