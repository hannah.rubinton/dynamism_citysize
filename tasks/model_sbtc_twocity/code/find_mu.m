function [diff] = find_mu(mu,Y,wh,wl,r,H,L,pm,j)

%profits as a function of z 
[fo] = firm_decisions(mu,Y,wh,wl,r,H,L,pm,j);
a = fo.pi_no_adoption < fo.pi_adopt;
za_ind = find(a==1,1,'first');
pi = fo.pi_no_adoption.*(1-a) + fo.pi_adopt.*(a);

%find value function and zx
v = vfunciteration(pi,pm);
%assert(min(v)==0,'no exit');
zx_ind = find(v==0,1,'last');
zx = pm.zgrid(zx_ind);
%assert(pm.ze>=zx,'entry z is less than exit z')

%find firm distribution 
g = pm.G_zx{zx_ind}; %solve_firm_distribution(zx,pm);

%use firm distribution and za to solve for mu
mu_prime = sum(g(za_ind:end));

%% equil condition is: 
diff = (mu - mu_prime);