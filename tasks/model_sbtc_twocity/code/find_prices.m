function [diff,Y,mu] = find_prices(wh,wl,r,H,L,pm,j)
%wh = whwl*wl;

if wh<=0 || wl<=0 || r<=0
    diff = [-10000;-10000;-10000];
    return
end


Y = (wh*H + wl*L)*(pm.sig/(pm.sig-1));

% %solve for r
% options = optimset('Display','off');
% 
% %if r=0 and the diff is still negative, then the wages are too high
% 
% if find_r(0,wh,wl,Y,pm,j)<0;
%     diff = [10000;10000];
%     r=0; mu=0;
%     return;
% end
%     
% func = @(in) find_r(in(1),wh,wl,Y,pm,j);
% out = fsolve(func,pm.initialguess_Y(j),options);
% r=out(1);
  

%find mu
%solve for mu
if isnan(pm.fa(j))
    mu=0;
elseif pm.rho_mu == 0
    mu=0;
else
    func = @(in) find_mu(in,Y,wh,wl,r,H,L,pm,j);
    if func(1)==1
        mu=1;
     elseif func(0)==0
         mu=0;
    else
        mu=bisect(func,0,1);
        %func(mu);
    end
    if mu<0 || func(mu)>.001
        mu = 0;
    end
    if mu==0
        if func(.1)<0
            mu=bisect(func,.1,1);
        elseif func(.2)<0
            mu=bisect(func,.2,1);
        elseif func(.05)<0
            mu=bisect(func,.05,1);
        elseif func(.01)<0
            mu=bisect(func,.01,1);
        end
    end
end

%profits as a function of z 
if isnan(pm.fa(j)) 
    [fo] = firm_decisions(mu,Y,wh,wl,r,H,L,pm,j);
    a = zeros(size(pm.zgrid));
    pi = fo.pi_no_adoption;
else 
    [fo] = firm_decisions(mu,Y,wh,wl,r,H,L,pm,j);
    a = fo.pi_no_adoption < fo.pi_adopt;
    pi = fo.pi_no_adoption.*(1-a) + fo.pi_adopt.*(a);
end

%find value function and zx
v = vfunciteration(pi,pm);
assert(min(v)==0,'no exit');
zx_ind = find(v==0,1,'last');
zx = pm.zgrid(zx_ind);
%assert(pm.ze>=zx,'entry z is less than exit z')
x = v<=0;

%find firm distribution 
g = pm.G_zx{zx_ind}; %solve_firm_distribution(zx,pm);



if isnan(pm.fa(j)) 
    mu=0;
else 
    %za_ind = find(a==1,1,'first');
    mu = sum(g.*a);
end


[fo] = firm_decisions(mu,Y,wh,wl,r,H,L,pm,j,x,a); 

%mass of firms from P=1
M = 1/(sum(fo.p(x==0).^(1-pm.sig).*g(x==0)));
%mass of entrants = mass of firms who hit the exit threshold next period
%probability of successful entry
p_success = sum(pm.Pentry.*(1-x));
%the mass of entrants will be E=Mass_attempted*p_success
%we need E=pexit so scale pexit by 1/p_success
scale_for_success = 1/p_success;
E = M*sum(pm.P'*g .*x)*scale_for_success;


%use hicksian labor demand to solve for Hd and Ld 
if pm.fixed_and_entry_costs == "labor"
    Hd = M*sum(fo.hd.*fo.q.*g) + M*(1-mu)*pm.fc(j) + M*mu*pm.fa(j) + E*pm.fe(j);
    Ld = M*sum(fo.ld.*fo.q.*g) + M*(1-mu)*pm.fc(j) + M*mu*pm.fa(j) + E*pm.fe(j);
else 
    Hd = M*sum(fo.hd.*fo.q.*g); % + M*(1-mu)*pm.fc + M*mu*pm.fa + E*pm.fe;
    Ld = M*sum(fo.ld.*fo.q.*g); % + M*(1-mu)*pm.fc + M*mu*pm.fa + E*pm.fe;
end


%value at entry
Ve = sum(v.*pm.Pentry);
%Ve - pm.fe(j)
%zx_ind
%M

Bs = pm.b(j)*r^(1/(pm.eta(j)-1));
Bd = M*pm.fb(j);

%% three equil conditions 
%labor market clearing
%free entry
if isnan(r)
    diff = [H-Hd;L-Ld];
else
    diff = [(Ve-pm.fe(j));(L-Ld);(Bs-Bd)];
end

