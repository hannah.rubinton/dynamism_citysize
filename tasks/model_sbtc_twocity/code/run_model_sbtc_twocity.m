%main code where I will run the equilibrium solver and find steady state


%% parameters
pm.L = 1;
pm.H = .4;
%parameters of the production function
pm.gmH = [1,1];
pm.gmL = [1,1];
pm.gmHprime = [1.1,1.1];
pm.gmLprime = [1,1];
pm.sig = 4; %eos btw varieties 
pm.rho = .95; %discount rate
pm.eps = 1.62; %eos btw h and l
%city agglomeration, amenities and congestion
pm.psi = [1.2,1] %agglomeration (need to change the name)
pm.psi_a = @(mu,j) pm.psi(j)*(1+mu);
pm.Ah1 = 1; %ammenities 
pm.Al1 = 1;
pm.zeta = [1,1];
pm.alpha = [2,2];
pm.eta = [2,2]; %elasticity of housing supply 
%fixed costs
pm.fb = 4;
pm.fc = 2;
pm.fa = NaN;
pm.fe = 2; 
pm.b(1) = 1; pm.b(2)=.25;
pm.fixed_and_entry_costs = "final good";

% zgrid 
%zgrid 
pm.N_z = 25; zmin=.001; zmax=150;
%zgrid = (exp(exp(linspace(zmin,log(log(zmax)),N_z))-1)-1)';
pm.zgrid = (exp(linspace(zmin,log(zmax),pm.N_z))-1)';

pm.sig_eps = .262;
pm.rhop=.98;
pm.zbar =0;
%pm.ze_param = 1;
%pm.ze_ind = min(find(pm.zgrid>pm.ze_param)); %entry grid point 
%pm.ze =  pm.zgrid(pm.ze_ind); 
zmin_entry = .5;
pm.nu = 3; %tail index for pareto distn for entry
%transition matrix
discretize_ar1; 

pm.P=P;
pm.Pentry=Pentry;


%% solve model 
optset('broyden','tol',1e-4);
func =@(in) equate_utility(in(1),in(2),pm)
out = broyden(func,[.245;.6125])
H1 = out(1);
L1 = out(2);
[~,city1,city2]=equate_utility(H1,L1,pm)

%% get the profit functions 
fo1 = firm_decisions(city1.mu,city1.Y,city1.wh,city1.wl,city1.r,pm,1,city1.x,city1.a);
fo2 = firm_decisions(city2.mu,city2.Y,city2.wh,city2.wl,city2.r,pm,2,city2.x,city2.a);

figure(1)
subplot(1,2,1)
plot(pm.zgrid,fo1.pi_no_adoption,pm.zgrid,fo1.pi_adopt)
subplot(1,2,2)
plot(pm.zgrid,fo2.pi_no_adoption,pm.zgrid,fo2.pi_adopt)

figure(2)
subplot(1,2,1)
plot(pm.zgrid,city1.x,pm.zgrid,city2.x)
subplot(1,2,2)
plot(pm.zgrid,city1.a,pm.zgrid,city2.a)

%profits
figure(3)
plot(pm.zgrid,fo1.pi_no_adoption,pm.zgrid,fo1.pi_adopt,pm.zgrid,fo2.pi_no_adoption,pm.zgrid,fo2.pi_adopt)

%varpi
figure(4)
plot(pm.zgrid,fo1.varpi_no_adoption,pm.zgrid,fo1.varpi_adopt,pm.zgrid,fo2.varpi_no_adoption,pm.zgrid,fo2.varpi_adopt)

figure(5)
plot(pm.zgrid,city1.v,pm.zgrid,city2.v)


diff1 = (fo1.varpi_adopt - fo1.varpi_no_adoption);
diff2 = (fo2.varpi_adopt - fo2.varpi_no_adoption);

%diff between var profits for adoption vs no adoption, this diff should
%determine whether or not they will pay the fixed cost
figure(6)
plot(pm.zgrid,diff1,pm.zgrid,diff2)


%adoption_cost1 = (city1.wh+city1.wl)*pm.fa
%adoption_cost2 = (city2.wh+city2.wl)*pm.fa

%looking at figure 5, what I want is to compare the horizontal difference
%between the two curves. Use interpolation 
%i.e. we are actually trying to invert figure 5.
figure(7)
plot(diff1,pm.zgrid,diff2,pm.zgrid)
%putting them on the same x axis points
zint2 = interp1(diff1,pm.zgrid,diff2);
plot(diff1,pm.zgrid,diff1,zint2)
diff_of_diff = pm.zgrid - zint2;
figure(8)
plot(pm.zgrid,diff_of_diff)