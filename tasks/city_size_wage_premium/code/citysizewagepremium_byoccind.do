

set more off
cap log close
*ssc install estout, replace
net from "/home/hherman/estout/"
clear
set matsize 10000
set maxvar 10000


global rawdata "/scratch/network/hherman/rawdata"
global xwalks "/home/hherman/dynamism_citysize/tasks/clean_crosswalks/output"
global data "/scratch/network/hherman/dynamism_citysize/data"
global output "/home/hherman/dynamism_citysize/tasks/city_size_wage_premium/output"
global citysizecat "/home/hherman/dynamism_citysize/tasks/citysizecat/output"
global clean_acs "../../clean_acs/output"
global seer "../../clean_seer/output"

program define runregs_indoccFEs 
	args citysizecat indocc
	use "$clean_acs/acs_`citysizecat'_`indocc'_skillgroup.dta", clear

	if "`citysizecat'" == "cbsa" {
		merge m:1 cbsa year  using "$seer/cbsa_pop.dta", nogen keep(master match) 
		gen lwapop = log(wapop)
	} 
	local regvars lincwage lhwage incwage_res hwage_res incwage_fs_res hwage_fs_res incwage_fs_noeduc_res hwage_fs_noeduc_res
	local years 1980 1990 2000 2007 2010 2013
	foreach var of local regvars { 
		eststo clear 
		foreach yr of local years {
			if "`citysizecat'" == "cbsa" {
				eststo, title("`var' `yr'"): reg `var' i.skillgroup i.skillgroup#c.lwapop i.`indocc' if year==`yr'
			} 
			else {	

				eststo, title("`var' `yr'"): reg `var' i.skillgroup i.skillgroup#i.citysizecat i.`indocc'  if year==`yr'
			}
		}

		esttab using "$output/`var'`citysizecat'`indocc'.csv", replace drop(*`indocc'*) mtitle
		}		
end


**Run regressions `
runregs_indoccFEs citysizecat1980 ind1990
runregs_indoccFEs citysizecat ind1990
runregs_indoccFEs cbsa ind1990
runregs_indoccFEs citysizecat1980 occ1990dd
runregs_indoccFEs citysizecat occ1990dd
runregs_indoccFEs cbsa occ1990dd




cap rm "$output/regressions_byindocc.xlsx"
local i =1 
local regvars lincwage lhwage incwage_res hwage_res incwage_fs_res hwage_fs_res incwage_fs_noeduc_res hwage_fs_noeduc_res
foreach var of local regvars {
foreach indocc in ind1990 occ1990dd {
foreach cs in citysizecat citysizecat1980 cbsa {
	import delimited using "$output/`var'`cs'`indocc'.csv", clear
	export excel using "$output/regressions_byindocc.xlsx", sheet("`cs'`indocc'`i'") 
	rm "$output/`var'`cs'`indocc'.csv"
	local i = `i'+1
}
}
}


















 
