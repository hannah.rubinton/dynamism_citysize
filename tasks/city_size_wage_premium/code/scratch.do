
cap log close
clear
set more off

global acs "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_acs/output"
global pop "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_seer/output"
 
/*
use "$acs/acs_cbsa_ind.dta", clear

merge m:1 cbsa year using "$pop/cbsa_pop.dta"

gen lwapop = log(wapop)
keep if inlist(year,1980,1990,2000,2005,2007,2010,2013)
bys year: reg lincwage lwapop i.ind1990


use "$acs/acs_cbsa_occ.dta", clear

merge m:1 cbsa year using "$pop/cbsa_pop.dta"

gen lwapop = log(wapop)
keep if inlist(year,1980,1990,2000,2005,2007,2010,2013)
bys year: reg lincwage lwapop i.occ1990



use "$acs/acs_citysize1980_ind.dta", clear

drop if citysizecat == . | citysizecat==0
gen lincwage_college = log(incwage_college)
gen lincwage_nc = log(incwage_nc)

sort year ind citysizecat 
by year ind: gen szpremium = lincwage[_n] - lincwage[_n-6] if _n==7
by year ind: gen szpremium_nc = lincwage_nc[_n] - lincwage_nc[_n-6] if _n==7
by year ind: gen szpremium_college = lincwage_college[_n] - lincwage_college[_n-6] if _n==7
bys year: sum szpremium*


eststo clear
foreach yr in 1980 1990 2000 2007 2013 {
	eststo: reg lincwage_college i.citysizecat i.ind if year==`yr'
}
estout using ../output/year.csv




/*
gen beta_cs = .
foreach o of local occ { 
	foreach y of local years {
		display `o'
		display `y'
		reg lincwage i.skillgroup i.skillgroup#i.citysizecat1980 if year==`y' & occ1990dd==`o'
			forvalue j=2/7 {
				replace beta_cs = _b[1.skillgroup#`j'.citysizecat1980] if year==`y' & occ1990dd==`o' & citysizecat==`j'
			}
		}
}



sort year occ skillgroup citysizecat 
by year occ skillgroup: gen szpremium = lincwage[_n] - lincwage[_n-6] if _n==7
bys year: sum szpremium*


*/
