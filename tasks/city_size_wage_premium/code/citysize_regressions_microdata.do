
set more off
cap log close
*ssc install estout, replace
net from "/home/hherman/estout/"
clear
set matsize 10000
set maxvar 10000


global rawdata "/scratch/network/hherman/rawdata"
global xwalks "/home/hherman/dynamism_citysize/tasks/clean_crosswalks/output"
global data "/scratch/network/hherman/dynamism_citysize/data"
global output "/home/hherman/dynamism_citysize/tasks/city_size_wage_premium/output"
global citysizecat "/home/hherman/dynamism_citysize/tasks/citysizecat/output"
global seer "../../clean_seer/output"

use  "$data/acs_3perc_cleaned.dta", clear
*use  "$data/acs_3perc_cleaned.dta" if runiform()<.01, clear

merge m:1 cbsa year using "$citysizecat/citysizecat.dta", nogen keep(master match)
merge m:1 cbsa year using "$seer/cbsa_pop.dta", nogen keep(master match)


foreach v in wapop incwage_college incwage_nc hwage_college hwage_nc {
	gen l`v' = log(`v')
}


local vars hwage incwage incwage_college incwage_nc
local years 1980 1990 2000 2007 2013
*levelsof year, local(years)


foreach var of local vars {
	eststo clear
	foreach yr of local years {
		** calcualte cbsa fixed effects 
		eststo, title("`var' `yr'"): reg l`var' lwapop  i.educcat age age2 i.sex i.race_cat  [aw=wgt] if year ==`yr'

		*ind
		eststo, title("`var' `yr', ind"): reg l`var' lwapop  i.educcat age age2  i.ind1990 i.sex i.race_cat  [aw=wgt] if year==`yr'
		
		*occ
		eststo, title("`var' `yr', occ"): reg l`var' lwapop  i.educcat age age2 i.occ1990dd  i.sex i.race_cat  [aw=wgt] if year==`yr'

		*full set of controls
		eststo, title("`var' `yr', ind occ"): reg l`var' lwapop  i.educcat age age2 i.occ1990dd i.ind1990 i.sex i.race_cat  [aw=wgt] if year==`yr'
	}
	esttab using "$output/`var'_lwapop.csv", replace keep(*wapop*) mtitle
}


foreach var of local vars {
	eststo clear
	foreach yr of local years {
		** calcualte cbsa fixed effects 
		eststo, title("`var' `yr'"): reg l`var' i.citysizecat1980  i.educcat age age2 i.sex i.race_cat  [aw=wgt] if year ==`yr'

		*ind
		eststo, title("`var' `yr', ind"): reg l`var' i.citysizecat1980  i.educcat age age2  i.ind1990 i.sex i.race_cat  [aw=wgt] if year==`yr'
		
		*occ
		eststo, title("`var' `yr', occ"): reg l`var' i.citysizecat1980  i.educcat age age2 i.occ1990dd  i.sex i.race_cat  [aw=wgt] if year==`yr'

		*full set of controls
		eststo, title("`var' `yr', ind occ"): reg l`var' i.citysizecat1980  i.educcat age age2 i.occ1990dd i.ind1990 i.sex i.race_cat  [aw=wgt] if year==`yr'
	}
	esttab using "$output/`var'_citysizecat1980.csv", replace keep(*citysizecat*) mtitle
}

	
foreach var of local vars {
	eststo clear
	foreach yr of local years {
		** calcualte cbsa fixed effects 
		eststo, title("`var' `yr'"): reg l`var' i.citysizecat  i.educcat age age2 i.sex i.race_cat  [aw=wgt] if year ==`yr'

		*ind
		eststo, title("`var' `yr', ind"): reg l`var' i.citysizecat  i.educcat age age2  i.ind1990 i.sex i.race_cat  [aw=wgt] if year==`yr'
		
		*occ
		eststo, title("`var' `yr', occ"): reg l`var' i.citysizecat  i.educcat age age2 i.occ1990dd  i.sex i.race_cat  [aw=wgt] if year==`yr'

		*full set of controls
		eststo, title("`var' `yr', ind occ"): reg l`var' i.citysizecat  i.educcat age age2 i.occ1990dd i.ind1990 i.sex i.race_cat  [aw=wgt] if year==`yr'
	}
	esttab using "$output/`var'_citysizecat.csv", replace keep(*citysizecat*) mtitle 

}





local vars hwage incwage incwage_college incwage_nc
local cat citysizecat citysizecat1980 lwapop
cap rm "$output/regressions_microdata.xlsx"
foreach v of local vars {
	local i = 1
	foreach c of local cat {
		import delimited using "$output/`v'_`c'.csv", clear
		export excel using "$output/regressions_microdata.xlsx", sheet("`v'`i'") 
		rm "$output/`v'_`c'.csv"
		local i = `i'+1
	}
}









//

