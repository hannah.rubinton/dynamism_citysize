cap log close
clear
set more off

global merge "/Users/hannah/Dropbox/dynamism_citysize/tasks/merge_cbsa_data/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/city_size_wage_premium"

 
 
use "$merge/sr_cbsa.dta", clear
drop lincwage lincwage1-lincwage5
gen lwapop = log(wapop)
local varlist incwage incwage_college incwage_nc incwage1 incwage2 incwage3 ///
	incwage4 incwage5 incwage_res incwage_fs_res incwage_byskc_res incwage_bysknc_res ///
	incwage_byeduccat1_res incwage_byeduccat2_res incwage_byeduccat3_res ///
	incwage_byeduccat4_res incwage_byeduccat5_res incwage_c_res incwage_nc_res ///
	incwage_c_fs_res incwage_nc_fs_res
eststo clear
foreach v of local varlist {
	gen l`v' = log(`v')
	eststo: reg l`v' i.year i.year#c.lwapop
	matrix `v'pop = e(b) 
	matrix `v'popV = e(V)
}

esttab using "$ctask/output/citysizewagepremium_residuals.csv", se r2 ///
	 star(* .1 ** .05 *** .01) /// 
	 keep(*lwapop*) label replace 
	 
	 

	 
**make a dataset of betas and confidence interavals by year so we can plot citysize elasticities 
keep year
bys year: keep if _n==1
levelsof year, local(years)
foreach v of local varlist {
	gen l`v'b = .
	gen l`v'cilb = .
	gen l`v'ciub = .
	
foreach yr of local years {
	replace l`v'b = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lwapop")] if year==`yr'
	replace l`v'cilb = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lwapop")] - 1.96*sqrt(`v'popV[rownumb(`v'popV,"`yr'.year#c.lwapop"),colnumb(`v'popV,"`yr'.year#c.lwapop")]) if year==`yr'
	replace l`v'ciub = `v'pop[1,colnumb(`v'pop,"`yr'.year#c.lwapop")] + 1.96*sqrt(`v'popV[rownumb(`v'popV,"`yr'.year#c.lwapop"),colnumb(`v'popV,"`yr'.year#c.lwapop")]) if year==`yr'

}

}


**plot city size elasticities 
twoway line lincwageb lincwagecilb lincwageciub year, pstyle(p1 p1 p1) legend(off) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") lpattern("1" "-" "-")
graph export "$ctask/plots/e_lincwage.eps", replace 	


twoway line lincwage_collegeb lincwage_collegeci* ///
	lincwage_ncb lincwage_ncci* year, pstyle(p1 p1 p1 p2 p2 p2) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4) label(1 "college") label(4 "no college")) lpattern("1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lincwage_college_vs_nc.eps", replace

*controls
twoway line lincwage_c_resb lincwage_c_resci* ///
	lincwage_nc_resb lincwage_nc_resci* year, pstyle(p1 p1 p1 p2 p2 p2) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4) label(1 "college") label(4 "no college")) lpattern("1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lresincwage_college_vs_nc.eps", replace

*more controls
twoway line lincwage_c_fs_resb lincwage_c_fs_resci* ///
	lincwage_nc_fs_resb lincwage_nc_fs_resci* year, pstyle(p1 p1 p1 p2 p2 p2) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4) label(1 "college") label(4 "no college")) lpattern("1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lresincwage_fs_college_vs_nc.eps", replace



* by skill group controls
twoway line /// 
	lincwage_byskc_resb lincwage_byskc_resci* ///
	lincwage_bysknc_resb lincwage_bysknc_resci* year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3) graphregion(color(white)) /// 
	title("Elasticity of CBSA fes to population") /// 
	legend(order(1 4 7) label(1 "everyone") label(4 "college") label(7 "no college")) /// 
	lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-") ///
	note("full set of controls, runnings regressions separately for each skill group")
graph export "$ctask/plots/e_lresincwage_bysk.eps", replace

*more skill groups 
twoway line lincwage1b lincwage1ci* /// 
	lincwage2b lincwage2ci* ///
	lincwage3b lincwage3ci* ///
	lincwage4b lincwage4ci* ///
	lincwage5b lincwage5ci* ///
	year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3 p4 p4 p4 p5 p5 p5 ) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4 7 10 13) label(1 "< HS") label(4 "HS") label(7 "some college") /// 
		label(10 "college") label(13 "graduate degree")) ///
		lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-" "1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lincwage_edcuccat.eps", replace


*more skill groups, residual wages
twoway line lincwage_byeduccat1_resb lincwage_byeduccat1_resci* /// 
	lincwage_byeduccat2_resb lincwage_byeduccat2_resci* ///
	lincwage_byeduccat3_resb lincwage_byeduccat3_resci* ///
	lincwage_byeduccat4_resb lincwage_byeduccat4_resci* ///
	lincwage_byeduccat5_resb lincwage_byeduccat5_resci* ///
	year, /// 
	pstyle(p1 p1 p1 p2 p2 p2 p3 p3 p3 p4 p4 p4 p5 p5 p5 ) graphregion(color(white)) /// 
	title("Elasticity of average log income to population") /// 
	legend(order(1 4 7 10 13) label(1 "< HS") label(4 "HS") label(7 "some college") /// 
		label(10 "college") label(13 "graduate degree")) ///
		lpattern("1" "-" "-" "1" "-" "-" "1" "-" "-" "1" "-" "-" "1" "-" "-")
graph export "$ctask/plots/e_lresincwage_edcuccat.eps", replace




