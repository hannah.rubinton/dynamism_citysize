function [H2,L2,WH2,WL2,MH1,MH2,ML1,ML2, PH1,PH2,PL1,PL2,Y1,Y2,YH1_D,YH2_D,YL1_D,YL2_D,...
    YH1_S,YH2_S,YL1_S,YL2_S, ...
    UH1,UL1,results] = melitz_model_twocity(H1,L1,WH1,WL1,params)
    
    %parameters 
    ep=params.ep; %eos
    sig=params.sig;
    gammaL=params.gammaL; %weight on L in final good 
    gammaH=params.gammaH;
    fcH=params.fcH;
    fcL=params.fcL;
    feH=params.feH ;
    feL=params.feL;
    dlH=params.dlH;
    dlL=params.dlL;
    lambdaH=params.lambdaH ; %pareto shape parameter 
    lambdaL=params.lambdaL ; %pareto shape parameter 
    L=params.L;
    H=params.H;
    th=params.theta;
    psiH=params.psiH;
    psiL=params.psiL; %if negative then they like big cities 
    alH=params.alH ;
    alL=params.alL ;
    AH1=params.AH1;
    AL1=params.AL1;

    H2 = H-H1;
    L2 = L-L1;
    
    
    %%worker utility 
    %congestion 
    CH1 = params.psiH*(H1+L1)^params.alH;
    CH2 = params.psiH*(H2+L2)^params.alH;
    CL1 = params.psiL*(H1+L1)^params.alL;
    CL2 = params.psiL*(H2+L2)^params.alL;
    %u = @(w) (w^(1-th)-1)/(1-th);
    u = @(c) (c^(1-th))/(1-th);
    %worker utility 
    UH1 = AH1*u(WH1) - CH1;
    UL1 = AL1*u(WL1) - CL1;

    WH2 = ((UH1 + CH2)*(1-th))^(1/(1-th)); 
    WL2 = ((UL1 + CL2)*(1-th))^(1/(1-th)); 
    
    %intermediates
    phistarH1 = (-feH(1)/fcH(1)*(sig-lambdaH-1)/(sig-1)*dlH(1))^(-1/lambdaH);
    phistarH2 = (-feH(2)/fcH(2)*(sig-lambdaH-1)/(sig-1)*dlH(2))^(-1/lambdaH);
    phistarL1 = (-feL(1)/fcL(1)*(sig-lambdaL-1)/(sig-1)*dlL(1))^(-1/lambdaL);
    phistarL2 = (-feL(2)/fcL(2)*(sig-lambdaL-1)/(sig-1)*dlL(2))^(-1/lambdaL);
    assert(phistarH1>1 && phistarH2>1 && phistarL1>1 && phistarL2>1)
    phitildeH1 = (-lambdaH*phistarH1^(sig-1)/(sig-lambdaH-1))^(1/(sig-1));
    phitildeH2 = (-lambdaH*phistarH2^(sig-1)/(sig-lambdaH-1))^(1/(sig-1));
    phitildeL1 = (-lambdaL*phistarL1^(sig-1)/(sig-lambdaL-1))^(1/(sig-1));
    phitildeL2 = (-lambdaL*phistarL2^(sig-1)/(sig-lambdaL-1))^(1/(sig-1));
    
    %solve for R/M and PI/M to so that you can solve for M using labor
    %market clearing 
%     RdMH1 = (phitildeH1/phistarH1)^(sig-1)*sig*WH1*fcH(1);
%     RdMH2 = (phitildeH2/phistarH2)^(sig-1)*sig*WH2*fcH(2);
%     RdML1 = (phitildeL1/phistarL1)^(sig-1)*sig*WL1*fcL(1);
%     RdML2 = (phitildeL2/phistarL2)^(sig-1)*sig*WL2*fcL(2);
%     PidMH1 = WH1*fcH(1)*((phitildeH1/phistarH1)^(sig-1)-1);
%     PidMH2 = WH2*fcH(2)*((phitildeH2/phistarH2)^(sig-1)-1);
%     PidML1 = WL1*fcL(1)*((phitildeL1/phistarL1)^(sig-1)-1);
%     PidML2 = WL2*fcL(2)*((phitildeL2/phistarL2)^(sig-1)-1);
    GphistarH1 = (phistarH1^(-lambdaH));
    GphistarH2 = (phistarH2^(-lambdaH));
    GphistarL1 = (phistarL1^(-lambdaL));
    GphistarL2 = (phistarL2^(-lambdaL));
    denomH1 = fcH(1)*((phitildeH1/phistarH1)^(sig-1)*(sig-1)+1) + feH(1)*dlH(1)/GphistarH1;
    denomH2 = fcH(2)*((phitildeH2/phistarH2)^(sig-1)*(sig-1)+1) + feH(2)*dlH(2)/GphistarH2;
    denomL1 = fcL(1)*((phitildeL1/phistarL1)^(sig-1)*(sig-1)+1) + feL(1)*dlL(1)/GphistarL1;
    denomL2 = fcL(1)*((phitildeL2/phistarL2)^(sig-1)*(sig-1)+1)+ feL(2)*dlL(2)/GphistarL2;
    
    MH1 = H1 / denomH1;
    MH2 = H2 / denomH2;
    ML1 = L1 / denomL1;
    ML2 = L2 / denomL2; 
    
    %Profits and entry costs paid 
    PIH1 = MH1*WH1*fcH(1)*((phitildeH1/phistarH1)^(sig-1)-1);
    PIH2 = MH2*WH2*fcH(2)*((phitildeH2/phistarH2)^(sig-1)-1);
    PIL1 = ML1*WL1*fcL(1)*((phitildeL1/phistarL1)^(sig-1)-1);
    PIL2 = ML2*WL2*fcL(2)*((phitildeL2/phistarL2)^(sig-1)-1);
 
    ECH1 = MH1*dlH(1)/phistarH1^(-lambdaH)*WH1*feH(1);
    ECH2 = MH2*dlH(2)/phistarH2^(-lambdaH)*WH2*feH(2);
    ECL1 = ML1*dlL(1)/phistarL1^(-lambdaL)*WL1*feL(1);
    ECL2 = ML2*dlL(2)/phistarL2^(-lambdaL)*WL2*feL(2);
    
    %intermediate prices
    PH1 = MH1^(1/(1-sig))*WH1/phitildeH1*sig/(sig-1);
    PH2 = MH2^(1/(1-sig))*WH2/phitildeH2*sig/(sig-1);
    PL1 = ML1^(1/(1-sig))*WL1/phitildeL1*sig/(sig-1);
    PL2 = ML2^(1/(1-sig))*WL2/phitildeL2*sig/(sig-1); 
    
    
    %final good output 
    Y1 = WH1*H1 + WL1*L1 + PIH1 + PIL1 - ECH1 - ECL1;
    Y2 = WH2*H2 + WL2*L2 + PIH2 + PIL2 - ECH2 - ECL2;
    YH1_D = PH1^(-ep)*gammaH(1)^ep*Y1;
    YH2_D = PH2^(-ep)*gammaH(2)^ep*Y2;
    YL1_D = PL1^(-ep)*gammaL(1)^ep*Y1;
    YL2_D = PL2^(-ep)*gammaL(2)^ep*Y2;
    
    
    %sector revenue = total labor wages 
    RH1 = WH1*H1;
    RH2 = WH2*H2;
    RL1 = WL1*L1;
    RL2 = WL2*L2;  
     
    YH1_S = MH1^(sig/(sig-1))*RH1*PH1^sig*(WH1/phistarH1*sig/(sig-1))^(-sig); 
    YH2_S = MH1^(sig/(sig-1))*RH2*PH2^sig*(WH2/phistarH2*sig/(sig-1))^(-sig);     
    YL1_S = ML1^(sig/(sig-1))*RL1*PL1^sig*(WL1/phistarL1*sig/(sig-1))^(-sig); 
    YL2_S = ML1^(sig/(sig-1))*RL2*PL2^sig*(WL2/phistarL2*sig/(sig-1))^(-sig);     

    
    results.sk_premium = [WH1/WL1, WH2/WL2];
    results.sizepremium = [WH1/WH2, WL1/WL2];
    results.citysize = [L1+H1, L2+H2];
    results.Hshare = [H1/(L1+H1), H2/(L2+H2)];
    results.avgW = (H1*WH1 + H2*WH2 + L1*WL1 + L2*WL2)/(params.L+params.H) ;
    if params.year==1980
        results.WL2_WL2 = WL2/WL2;
        results.WH2_WL2 = WH2/WL2;
        results.WH1_WL2 = WH1/WL2;
        results.WL1_WL2 = WL1/WL2;
    else 
      results.WL2_WL2 = WL2/params.WL2;
        results.WH2_WL2 = WH2/params.WL2;
        results.WH1_WL2 = WH1/params.WL2;
        results.WL1_WL2 = WL1/params.WL2;  
    end
    results.H1share = H1/L1;
    results.H2share = H2/L2;
    results.relative_M = [MH1/ML1, MH2/ML2]; 
    results.HperM = [H1/MH1, H2/MH2];
    results.LperM = [L1/ML1, L2/ML2];
    results.phitildeH1 = phitildeH1;
    results.phitildeH2 = phitildeH2;
    results.phitildeL1 = phitildeL1;
    results.phitildeL2 = phitildeL2;
    results.phistarH1 = phistarH1;
    results.phistarH2 = phistarH2;
    results.phistarL1 = phistarL1;
    results.phistarL2 = phistarL2;
end
    
    
    
    
    
    