function [ diff] = eq_melitz_model_twocity(x,params)
   
    H1 = x(1);
    L1 = x(2);
    WH1 = x(3);
    WL1 = x(4);

    [H2,L2,WH2,WL2,MH1,MH2,ML1,ML2, PH1,PH2,PL1,PL2,Y1,Y2,YH1_D,YH2_D,YL1_D,YL2_D,...
    YH1_S,YH2_S,YL1_S,YL2_S, ...
    UH1,UL1,results] = melitz_model_twocity(H1,L1,WH1,WL1,params);
    
    diff = [(YH1_S - YH1_D)/YH1_D, ...
            (YH2_S - YH2_D)/YH2_D, ...
            (YL1_S - YL1_D)/YL1_D, ...
            (YL2_S - YL2_D)/YL2_D];

end