
clear 


%solving two city version of simple model
%parameters 

params.ep=2; %eos
params.sig=3;
params.gammaL=[1,1]; %weight on L in final good
params.gammaH=[1,1];
params.fcH = [.2,.1];
params.fcL=[.1,.1];
params.feH = [.5,.5];
params.feL=[.5,.5];
params.dlH = [.08,.08];
params.dlL = [.08,.08];
params.lambdaH = 5; %pareto shape parameter 
params.lambdaL = 5; %pareto shape parameter 
params.L=1;
params.H=1;
params.theta=.5;
params.psiH= .5;
params.psiL= .5; %if negative then they like big cities 
params.alH = 1.2;
params.alL = 1.2;
params.AH1=1;
params.AL1=1;
params.year=1980;

fun = @(x) eq_melitz_model_twocity(x,params)
eq=fsolve(fun,[.5,.5,.5,.5])
H1=eq(1)
L1=eq(2)
WH1=eq(3)
WL1=eq(4)

% H1=.5
% L1=.5
% WH1=.7
% WL1=.7
[H2,L2,WH2,WL2,MH1,MH2,ML1,ML2, PH1,PH2,PL1,PL2,Y1,Y2,YH1_D,YH2_D,YL1_D,YL2_D,...
    YH1_S,YH2_S,YL1_S,YL2_S, ...
    UH1,UL1,results] = melitz_model_twocity(H1,L1,WH1,WL1,params)

