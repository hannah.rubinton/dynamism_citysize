
clear 
clc

%solving two city version of simple model
%parameters 

params.zH=[1,1];
params.zL=[1,1];
params.btaH=.5;
params.btaL=.8; 
params.al = [.8,.5]; %decreasing returns to scale
params.ep=3; %eos
params.gammaL=[1,1.2]; %weight on L in final good 
params.gammaH=[1.06,.84];
params.fc =.5;
params.bc = 1;
params.feH = [.5,.5];
params.feL=[.5,.5];
params.B=1
params.L=1.19;
params.H=.54;
params.theta=.5;
params.psiH= .5;
params.psiL= .5; %if negative then they like big cities 
params.alH = 1.2;
params.alL = 1.2;
params.lambda = 3;
params.AH1=1.2;
params.AL1=1.2;
params.year=2005; 
params.WL2=.4

%% calibrate 1980 
pout = calibrate_med_twocity([1,1,1,1,1,1],params)
lb = [eps,eps,eps,eps,eps,eps];
ub = [2,2,2,2,10,10];
funcal = @(p) calibrate_med_twocity(p,params)
pout = fmincon(funcal,[1,1,1,1,1,1],[],[],[],[],lb,ub)
params.gammaL=[pout(1),pout(2)];
params.gammaH=[pout(3),pout(4)];
params.AH1 = pout(5);
params.AL1 = pout(6);

%solve model with calibrated parameters 
fun = @(x) eq_med_model_twocity(x,params)
eq_1980=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
H1 = eq_1980(1);
L1 = eq_1980(2);
NH1 = eq_1980(3);
NH2 = eq_1980(4);
NL1 = eq_1980(5);
NL2 = eq_1980(6);
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,modeltest] ...
    = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);
paramstest = params;
paramstest.WL2 = WL2; 



%% lower entry costs
%params.AH1 = 1.05;
%params.AL1 = 1.25;
paramstest.feH = [.01,.01];
%solve model with calibrated parameters 
fun = @(x) eq_med_model_twocity(x,paramstest)
eq=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
H1 = eq(1);
L1 = eq(2);
NH1 = eq(3);
NH2 = eq(4);
NL1 = eq(5);
NL2 = eq(6);
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,modeltest2] ...
    = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);

