function [loss] = calibrate_simple_twocity(pin)
%solving two city version of simple model
%parameters 

%period 1: 1980
moments.WL1_WL1 = 1;
moments.WH2_WL1 = 1.52;
moments.WH1_WL1 = 1.86;
moments.WL2_WL1 = 1.18;
moments.H1share = .3;
moments.H2share = .2;


% %period 2: 2005-2007
% moments.WL1_WL1 = 1;
% moments.WH2_WL1 = 1.72;
% moments.WH1_WL1 = 2.52;
% moments.WL2_WL1 = 1.17;
% moments.H1share = .65;
% moments.H2share = .3;




params.zH=[1,1];
params.zL=[1,1];
params.psiH=.5;
params.psiL=.5; %if negative then htely like big cities 
params.alH = 1.2;
params.alL = 1.2;
params.ep=3;
params.gammaL=[pin(3),pin(4)];
params.gammaH=[pin(1),pin(2)];
params.theta=.2;
params.L=1;
params.H=.34;
params.AH1 = pin(5);
params.AL1 = pin(6);

fun = @(x) eq_simple_model_twocity(x,params)
eq=fsolve(fun,[.7,.4])


H1 = eq(1)
L1 = eq(2)
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2, WH2,CH1,CH2,CL1,CL2, UL1,UH1,UL2,UH2] = simple_model_twocity(H1,L1,params);

model.WL1_WL1 = 1;
model.WH2_WL1 = WH2/WL1;
model.WH1_WL1 = WH1/WL1;
model.WL2_WL1 = WL2/WL1;
model.H1share = H1/L1;
model.H2share = H2/L2;

loss = (



