#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Simple two city model: no intermediate producers 
\end_layout

\begin_layout Standard
Two cities 
\begin_inset Formula $j\in\{1,2\}$
\end_inset

.
 Two types of labor unskilled 
\begin_inset Formula $L$
\end_inset

 and high skilled 
\begin_inset Formula $H$
\end_inset

.
 
\end_layout

\begin_layout Standard
Final goods aggregator in each city produces according to 
\begin_inset Formula 
\[
Y_{j}=(\gamma_{Lj}(z_{Lj}L_{j})^{\frac{\epsilon-1}{\epsilon}}+\gamma_{Hj}(z_{Hj}H_{j})^{\frac{\epsilon-1}{\epsilon}})^{\frac{\epsilon}{\epsilon-1}}
\]

\end_inset

Workers provide one unit of labor and consumer their wages.
 They choose their city 
\begin_inset Formula $j$
\end_inset

 to maximize utility givey by 
\begin_inset Formula 
\[
U_{Hj}=u(w_{Hj})-c(H_{j}+L_{j})
\]

\end_inset

where 
\begin_inset Formula $c(H_{j}+L_{j})$
\end_inset

 is the congestion cost or amenity value of living in a city with population
 
\begin_inset Formula $H_{j}+L_{j}$
\end_inset

.
 In equilibrium workers have to be indifferent between living in each city.
 
\begin_inset Formula 
\[
U_{H1}=U_{H2};\,\,\,\,U_{L1}=U_{L2}
\]

\end_inset

Labor markets have to clear
\begin_inset Formula 
\[
L=L_{1}+L_{2}
\]

\end_inset


\begin_inset Formula 
\[
H=H_{1}+H_{2}
\]

\end_inset

The final goods producer sovle 
\begin_inset Formula 
\[
maxY_{j}-w_{Lj}L_{j}-w_{Hj}H_{j}
\]

\end_inset

focs: 
\begin_inset Formula 
\[
Y_{j}^{\frac{1}{\epsilon}}\gamma_{Lj}z_{Lj}^{\frac{\epsilon-1}{\epsilon}}L_{j}^{\frac{-1}{\epsilon}}=w_{Lj}
\]

\end_inset


\begin_inset Formula 
\[
Y_{j}^{\frac{1}{\epsilon}}\gamma_{Hj}z_{Hj}^{\frac{\epsilon-1}{\epsilon}}H_{j}^{\frac{-1}{\epsilon}}=w_{Hj}
\]

\end_inset

So the skill premium in city 
\begin_inset Formula $j$
\end_inset

 will be 
\begin_inset Formula 
\[
\hat{w}_{j}=\frac{w_{H_{j}}}{w_{Lj}}=\frac{\gamma_{Hj}}{\gamma_{Lj}}\left(\frac{z_{Hj}}{z_{Lj}}\right)^{\frac{\epsilon-1}{\epsilon}}\left(\frac{H_{j}}{L_{j}}\right)^{\frac{-1}{\epsilon}}
\]

\end_inset

So the skill premium is decreasing in ratio of skilled to unskilled worker,
 but increasing in the ratio of their productivities.
 Assuming that 
\begin_inset Formula $H_{j}$
\end_inset

 increases with 
\begin_inset Formula $z_{Hj}$
\end_inset

, the skill premium won't increase as much as the productivity premium.
 
\end_layout

\begin_layout Standard
Looking at labor supply, 
\begin_inset Formula 
\[
U_{H1}=U_{H2}
\]

\end_inset


\begin_inset Formula 
\[
u(w_{H1})-c_{H}(H_{1}+L_{1})=u(w_{H2})-c_{H}(H_{2}+L_{2})
\]

\end_inset


\end_layout

\begin_layout Subsection*
Solving 
\end_layout

\begin_layout Standard
guessing 
\begin_inset Formula $H_{1}$
\end_inset

 and 
\begin_inset Formula $L_{1}$
\end_inset

, use labor market clearing to solve for 
\begin_inset Formula $H_{2}$
\end_inset

 and 
\begin_inset Formula $L_{2}$
\end_inset

.
 Since we know factor allocations, solve for 
\begin_inset Formula $Y_{1}$
\end_inset

 and 
\begin_inset Formula $Y_{2}$
\end_inset

.
 Then use the first order conditions to solve for wages.
 Plug in wage and factor allocations into the utility functions.
 Iterate until you find an 
\begin_inset Formula $H_{1}$
\end_inset

 and 
\begin_inset Formula $L_{1}$
\end_inset

 that solve 
\begin_inset Formula $U_{H1}=U_{H2}$
\end_inset

 and 
\begin_inset Formula $U_{L1}=U_{L2}$
\end_inset

.
 
\end_layout

\begin_layout Subsection*
City size premium: 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
u(w_{H1})-c_{H}(H_{1}+L_{1})=u(w_{H2})-c_{H}(H_{2}+L_{2})
\]

\end_inset

suppose I have log utility, then I can rewrite this condition as 
\begin_inset Formula 
\[
log(\frac{w_{H1}}{w_{H2}})=c_{H}(pop_{1})-c_{H}(1-pop_{1})
\]

\end_inset

Suppose 
\begin_inset Formula $C_{H}(.)=\psi()^{\alpha}$
\end_inset

 then 
\begin_inset Formula 
\[
\frac{dlog\left(w_{H1}/w_{H2}\right)}{dpop_{1}}=\psi\alpha pop_{1}^{\alpha-1}+\psi\alpha(1-pop_{1})^{\alpha-1}
\]

\end_inset

This gives the 
\begin_inset Formula $\%\Delta$
\end_inset

 in the city size wage premium that is necessary to compensate the person
 for a 1 person increase in the population of city 1.
 The higher 
\begin_inset Formula $\alpha$
\end_inset

, the more the city size wage premium responds to changes in population.
 If 
\begin_inset Formula $\alpha=0$
\end_inset

 then they are unaffected by population.
 If 
\begin_inset Formula $\psi$
\end_inset

 is negative then they like big cities.
\end_layout

\begin_layout Section
Two city model: homogeneous intermediate producers 
\end_layout

\begin_layout Standard
Two cities 
\begin_inset Formula $j\in\{1,2\}$
\end_inset

.
 Two types of labor unskilled 
\begin_inset Formula $L$
\end_inset

 and high skilled 
\begin_inset Formula $H$
\end_inset

 and two types of intermediate producers who use unskilled and skilled labor
 respectively, 
\begin_inset Formula $i\in\{L,H\}$
\end_inset

.
 
\end_layout

\begin_layout Standard
Final goods aggregator in each city produces according to 
\begin_inset Formula 
\[
Y_{j}=(\gamma_{Lj}Y_{Lj}^{\frac{\epsilon-1}{\epsilon}}+\gamma_{Hj}Y_{Hj}^{\frac{\epsilon-1}{\epsilon}})^{\frac{\epsilon}{\epsilon-1}}
\]

\end_inset

They solve
\begin_inset Formula 
\[
max_{Y_{Lj},Y_{Hj}}Y_{j}-P_{Lj}Y_{Lj}-P_{Hj}Y_{Hj}
\]

\end_inset

focs: 
\begin_inset Formula 
\[
Y_{j}^{\frac{1}{\epsilon}}\gamma_{Lj}Y_{Lj}^{-\frac{1}{\epsilon}}=P_{Lj}
\]

\end_inset


\begin_inset Formula 
\[
Y_{j}^{\frac{1}{\epsilon}}\gamma_{Hj}Y_{Hj}^{-\frac{1}{\epsilon}}=P_{Hj}
\]

\end_inset

divide to get relative prices 
\begin_inset Formula 
\[
p_{j}=\frac{P_{Hj}}{P_{Lj}}=\frac{\gamma_{Hj}}{\gamma_{Lj}}\left(\frac{Y_{Hj}}{Y_{Lj}}\right)^{-\frac{1}{\epsilon}}
\]

\end_inset

Dual price index: 
\begin_inset Formula 
\[
PY=P_{L}Y_{L}+P_{H}Y_{H}
\]

\end_inset


\begin_inset Formula 
\[
=P_{L}\left(\frac{P_{L}}{\gamma_{L}}\right)^{-\epsilon}Y+P_{H}\left(\frac{P_{H}}{\gamma_{H}}\right)^{-\epsilon}Y
\]

\end_inset

If 
\begin_inset Formula $Y=1,P=1$
\end_inset

 then 
\begin_inset Formula 
\[
1=\left(\gamma_{L}^{\epsilon}P_{L}^{1-\epsilon}+\gamma_{H}{}^{\epsilon}P_{H}^{1-\epsilon}\right)^{\frac{1}{1-\epsilon}}
\]

\end_inset


\begin_inset Formula 
\[
1=\left(\gamma^{\epsilon}P_{L}^{1-\epsilon}+\gamma{}_{H}^{\epsilon}P_{H}^{1-\epsilon}\right)^{\frac{1}{1-\epsilon}}
\]

\end_inset


\begin_inset Formula 
\[
=P_{L}\left(\gamma^{\epsilon}+\gamma_{H}{}^{\epsilon}\left(\frac{P_{H}}{P_{L}}\right)^{1-\epsilon}\right)^{\frac{1}{1-\epsilon}}
\]

\end_inset


\begin_inset Formula 
\[
\implies P_{L}=\left(\gamma_{L}^{\epsilon}+\gamma_{H}{}^{\epsilon}\left(p\right)^{1-\epsilon}\right)^{-\frac{1}{1-\epsilon}}
\]

\end_inset

There is a continuum of 
\begin_inset Formula $N_{ij}$
\end_inset

 homogenous intermediate producers for each type of intermediate.
 Firms of type 
\begin_inset Formula $i$
\end_inset

 in city 
\begin_inset Formula $j$
\end_inset

 produce according to 
\begin_inset Formula 
\[
y_{ij}=z_{ij}n_{ij}^{\alpha}
\]

\end_inset

where 
\begin_inset Formula $n_{ij}$
\end_inset

 is labor demand.
 They also pay a fixed costs in both units of buildings and labor.
 They solve 
\begin_inset Formula 
\[
\pi_{ij}(P_{ij},w_{ij},r_{j})=max_{n_{ij}}P_{ij}z_{ij}n_{ij}^{\alpha}-w_{ij}\left(n_{ij}+f_{c}\right)-r_{j}b_{c}
\]

\end_inset


\begin_inset Formula 
\[
foc:\,\,\,P_{ij}z_{ij}\alpha n_{ij}^{\alpha-1}=w_{ij}
\]

\end_inset


\begin_inset Formula 
\[
\implies\pi_{ij}(P_{ij,}w_{ij},r_{j})=P_{ij}z_{ij}\left(\frac{w_{ij}}{P_{ij}z_{ij}\alpha}\right)^{\frac{\alpha}{\alpha-1}}-w_{ij}\left(\frac{w_{ij}}{P_{ij}z_{ij}\alpha}\right)^{\frac{1}{\alpha-1}}-w_{ij}f_{c}-r_{j}b_{c},\,\,\,j=1,2
\]

\end_inset

solving for 
\begin_inset Formula $y_{Hj}$
\end_inset

and 
\begin_inset Formula $y_{Lj}$
\end_inset

 gives 
\begin_inset Formula $y_{ij}=z_{ij}\left(\frac{w_{ij}}{P_{ij}z_{ij}\alpha}\right)^{\frac{\alpha}{\alpha-1}}$
\end_inset

 and 
\begin_inset Formula $Y_{ij}=N_{ij}y_{ij}.$
\end_inset


\end_layout

\begin_layout Standard
A free entry condition holds so intermediate firms enter until profits are
 zero 
\begin_inset Formula 
\[
\pi_{ij}(P_{ij},w_{ij,}r_{j})=w_{ij}f_{e}
\]

\end_inset


\begin_inset Formula $N_{ij}$
\end_inset

 will be pinned down by the free entry condition, though in equilibrium
 there won't be entry since there is no turnover yet.
 
\end_layout

\begin_layout Standard
Workers provide one unit of labor and consume a bundle of the final good
 and building space.
 The utility of a worker of type 
\begin_inset Formula $i\in\{L,H\}$
\end_inset

 is given by 
\begin_inset Formula 
\[
U_{ij}(w_{ij},r_{j})=max_{c,b}A_{ij}c^{\beta_{i}}b^{1-\beta_{i}}
\]

\end_inset


\begin_inset Formula 
\[
c+r_{j}b=w_{ij}
\]

\end_inset


\begin_inset Formula 
\[
\implies c_{ij}=\beta_{i}w_{j};\,\,\,\,b_{ij}=(1-\beta_{i})\frac{w_{j}}{r_{j}}
\]

\end_inset


\begin_inset Formula 
\[
U_{ij}(w_{ij},r_{j})=A_{ij}\beta_{i}^{\beta_{i}}(1-\beta_{i})^{(1-\beta_{i})}\frac{w_{ij}}{r_{j}^{(1-\beta_{i})}}
\]

\end_inset

They choose their city 
\begin_inset Formula $j$
\end_inset

 to maximize utility.
 In equilibrium workers have to be indifferent between living in each city.
 
\begin_inset Formula 
\[
U_{H1}=U_{H2};\,\,\,\,U_{L1}=U_{L2}
\]

\end_inset

plugging in the equations for the indirect utility function and rearranging
 gives 
\begin_inset Formula 
\[
\frac{w_{j1}}{w_{j2}}=\frac{A_{j2}}{A_{j1}}\left(\frac{r_{1}}{r_{2}}\right)^{1-\beta_{j}}
\]

\end_inset

If 
\begin_inset Formula $\beta_{j}$
\end_inset

 is close to 1 then the city size wage premium is less sensitive to congestion
 forces.
 Labor markets have to clear
\begin_inset Formula 
\[
L=L_{1}+L_{2}
\]

\end_inset


\begin_inset Formula 
\[
H=H_{1}+H_{2}
\]

\end_inset

and 
\begin_inset Formula 
\[
L_{j}=N_{Lj}(n_{Lj}+f_{c})
\]

\end_inset


\begin_inset Formula 
\[
H_{j}=N_{Hj}(n_{Hj}+f_{c})
\]

\end_inset

The final goods market clears 
\begin_inset Formula 
\[
Y_{1}+Y_{2}=c_{H1}H_{1}+c_{H2}H_{2}+c_{L1}L_{1}+c_{L2}L_{2}
\]

\end_inset

(I don't think I need to account for entry here since in equilibrium entry
 shoudld be zero).
 
\end_layout

\begin_layout Standard
The building market clears in each city 
\begin_inset Formula 
\[
(1-\beta_{i})\frac{w_{j}}{r_{j}}L_{j}+(1-\beta_{i})\frac{w_{j}}{r_{j}}H_{j}+N_{Hj}b_{c}+N_{Lj}b_{c}=B_{j}
\]

\end_inset


\end_layout

\begin_layout Subsection*
Solving
\end_layout

\begin_layout Standard
Guess 
\begin_inset Formula $H_{1,}L_{1},N_{H1},N_{H2},N_{L1},N_{L2}$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $H_{2}=H-H_{1}$
\end_inset

 and 
\begin_inset Formula $L_{2}=L-L_{1}$
\end_inset


\end_layout

\begin_layout Enumerate
From the labor market clearing condition in each city 
\begin_inset Formula $\frac{L_{j}}{N_{Lj}}-f_{c}=n_{Lj}$
\end_inset

 
\end_layout

\begin_layout Enumerate
with 
\begin_inset Formula $n_{ij}$
\end_inset

 solve for 
\begin_inset Formula $y_{ij}$
\end_inset

 and then 
\begin_inset Formula $Y_{ij}=N_{ij}n_{ij}$
\end_inset

 
\end_layout

\begin_layout Enumerate
Use the focs and production function for final goods firm to solve for 
\begin_inset Formula $Y_{j}$
\end_inset

 and 
\begin_inset Formula $P_{ij}$
\end_inset

 
\end_layout

\begin_layout Enumerate
\begin_inset Formula $w_{ij}$
\end_inset

 from the first order condition for intermediates
\end_layout

\begin_layout Enumerate
\begin_inset Formula $r_{ij}$
\end_inset

 from the free entry condition 
\end_layout

\begin_layout Standard
Then check that the following 6 equilibrium conditions hold and iterate
 until you find an equilibrium 
\end_layout

\begin_layout Enumerate
Final goods market clears
\end_layout

\begin_layout Enumerate
building market clears in each city (2) 
\end_layout

\begin_layout Enumerate
utilities are equal across cities for each type (2)
\end_layout

\begin_layout Enumerate
price of intermediates satisfies the unit cost function for the final goods
 producer 
\end_layout

\end_body
\end_document
