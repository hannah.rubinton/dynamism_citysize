clear 
clc

%solving two city version of simple model
%parameters 

params.zH=[1,1];
params.zL=[1,1];
params.btaH=.5;
params.btaL=.8; 
params.al = .8; %decreasing returns to scale
params.ep=3; %eos
params.gammaL=[1,1.2]; %weight on L in final good 
params.gammaH=[1.06,.84];
params.fc =.1;
params.bc = 1;
params.feH = [.1,.1];
params.feL=[.1,.1];
params.B=1
params.L=1;
params.H=.24;
params.theta=.5;
params.psiH= .5;
params.psiL= .5; %if negative then they like big cities 
params.alH = 1.2;
params.alL = 1.2;
params.lambda = 3;
params.AH1=1.1;
params.AL1=1.2;
params.year=1980; 

%% calibrate 1980 
pout = calibrate_med_twocity([1,1,1,1,1,1],params)
lb = [eps,eps,eps,eps,eps,eps];
ub = [2,2,2,2,10,10];
funcal = @(p) calibrate_med_twocity(p,params)
pout = fmincon(funcal,[1,1,1,1,1,1],[],[],[],[],lb,ub)
params.gammaL=[pout(1),pout(2)];
params.gammaH=[pout(3),pout(4)];
params.AH1 = pout(5);
params.AL1 = pout(6);

%solve model with calibrated parameters 
fun = @(x) eq_med_model_twocity(x,params)
eq_1980=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
H1 = eq_1980(1);
L1 = eq_1980(2);
NH1 = eq_1980(3);
NH2 = eq_1980(4);
NL1 = eq_1980(5);
NL2 = eq_1980(6);
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,model1980] ...
    = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);
params1980 = params;
params1980.WL2 = WL2; 

%% final equilibrium 
%params.feH = [.01,.01];
%params.feL = [.3,.3];
params.year=2005;
params.L=1.19
params.H=.54;
params.avgW1980 = model1980.avgW;
params.WL2 = params1980.WL2;

pout = calibrate_med_twocity([1,1,1,1,1,1],params)
lb = [eps,eps,eps,eps,eps,eps];
ub = [2,2,2,2,10,10];
funcal = @(p) calibrate_med_twocity(p,params)
pout = fmincon(funcal,[1,1,1,1,1,1],[],[],[],[],lb,ub)
params.gammaL=[pout(1),pout(2)];
params.gammaH=[pout(3),pout(4)];
params.AH1 = pout(5);
params.AL1 = pout(6);

%solve model with calibrated parameters 
fun = @(x) eq_med_model_twocity(x,params)
eq_2005=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
H1 = eq_2005(1);
L1 = eq_2005(2);
NH1 = eq_2005(3);
NH2 = eq_2005(4);
NL1 = eq_2005(5);
NL2 = eq_2005(6);
H2 = params.H-H1;
L2 = params.L-L1;
[ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,model2005] ...
    = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);
params2005 = params;






% %% steady state comparison 
% sk_premium_0
% sk_premium_T
% gr_sk_premium = (sk_premium_T - sk_premium_0)./sk_premium_0.*100
% sizepremium_0
% sizepremium_T
% gr_sizepremium = (sizepremium_T - sizepremium_0)./sizepremium_0.*100
% citysize_0
% citysize_T
% gr_citysize = (citysize_T - citysize_0)./citysize_0.*100
% eq_0
% eq_T
% (eq_T - eq_0)./eq_0.*100
% Hshare_0
% Hshare_T 
% gr_Hshare = (Hshare_T - Hshare_0)./Hshare_0*100
% relative_N_0
% relative_N_T
% gr_relative_N = (relative_N_T - relative_N_0)./relative_N_0*100
% HperN_0
% HperN_T
% gr_HperN = (HperN_T - HperN_0)./HperN_0*100
% 
