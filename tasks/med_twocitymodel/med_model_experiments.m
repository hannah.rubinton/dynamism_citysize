% %run solve_med_twocity to get the calibrated parameters for 1980 and 2005
% %then use these parameters to run three experiments for what would happen 
% %in 2005 if you used the same gammas as 1980 
% solve_med_twocity;
% 
% 
% %% 1980 gammas
% params=params1980;
% params.year=2005;
% params.L=1.19
% params.H=.54;
% %solve model with calibrated parameters 
% fun = @(x) eq_med_model_twocity(x,params)
% eq=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
% H1 = eq(1);
% L1 = eq(2);
% NH1 = eq(3);
% NH2 = eq(4);
% NL1 = eq(5);
% NL2 = eq(6);
% H2 = params.H-H1;
% L2 = params.L-L1;
% [ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,exp1] ...
%     = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);
% 
% 
% 
% %% 1980 gammas but lower entry costs 
% params=params1980;
% params.year=2005;
% params.L=1.19
% params.H=.54; 
% params.feH = [.01,.01];
% %solve model with calibrated parameters 
% fun = @(x) eq_med_model_twocity(x,params)
% eq=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
% H1 = eq(1);
% L1 = eq(2);
% NH1 = eq(3);
% NH2 = eq(4);
% NL1 = eq(5);
% NL2 = eq(6);
% H2 = params.H-H1;
% L2 = params.L-L1;
% [ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,exp2] ...
%     = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);
% 
% 
% 
% %% 1980 gammas but lower entry costs, 2005 amentities 
% params=params1980;
% params.year=2005;
% params.L=1.19
% params.H=.54;
% params.feH = [.01,.01];
% params.AH1=params2005.AH1;
% params.AL1=params2005.AL1;
% %solve model with calibrated parameters 
% fun = @(x) eq_med_model_twocity(x,params)
% eq=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
% H1 = eq(1);
% L1 = eq(2);
% NH1 = eq(3);
% NH2 = eq(4);
% NL1 = eq(5);
% NL2 = eq(6);
% H2 = params.H-H1;
% L2 = params.L-L1;
% [ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,exp3] ...
%     = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);
% 
% 
% 
% 
% 
% %% 2005 parameters but lower entry costs 
% params = params2005;
% params.feH = [.01,.01];
% %solve model with calibrated parameters 
% fun = @(x) eq_med_model_twocity(x,params)
% eq=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
% H1 = eq(1);
% L1 = eq(2);
% NH1 = eq(3);
% NH2 = eq(4);
% NL1 = eq(5);
% NL2 = eq(6);
% H2 = params.H-H1;
% L2 = params.L-L1;
% [ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,exp4] ...
%     = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);
% 
% 
% 
% 
% %% 2005 params but gammaH  increased proportionally by 30% (that is 
% % how much they increased for city 2) but lower entry costs, 2005 amentities 
% params=params2005;
% params.gammaH = params1980.gammaH.*(1.3);
% 
% %solve model with calibrated parameters 
% fun = @(x) eq_med_model_twocity(x,params)
% eq=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
% H1 = eq(1);
% L1 = eq(2);
% NH1 = eq(3);
% NH2 = eq(4);
% NL1 = eq(5);
% NL2 = eq(6);
% H2 = params.H-H1;
% L2 = params.L-L1;
% [ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,exp5] ...
%     = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);
% 
% 
% %% 2005 params but gammaH  increased proportionally by 30% (that is 
% % how much they increased for city 2) but lower entry costs, 2005 amentities 
% params=params2005;
% 
% params.feH = [.01,.01];
% params.gammaH = params1980.gammaH.*(1.3);
% %solve model with calibrated parameters 
% fun = @(x) eq_med_model_twocity(x,params)
% eq=fsolve(fun,[params.H/2,.5,.05,.05,.05,.05])
% H1 = eq(1);
% L1 = eq(2);
% NH1 = eq(3);
% NH2 = eq(4);
% NL1 = eq(5);
% NL2 = eq(6);
% H2 = params.H-H1;
% L2 = params.L-L1;
% [ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2,exp6] ...
%     = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);

