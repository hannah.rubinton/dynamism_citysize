
cap log close
clear
set more off

global mergeddata "/Users/hannah/Dropbox/dynamism_citysize/tasks/merge_cbsa_data/output"
global ctask "/Users/hannah/Dropbox/dynamism_citysize/tasks/med_twocitymodel"

use "$mergeddata/sr_cbsa.dta", clear

drop lq*
drop job_*
drop sh_emp*
drop if year==.
keep if inlist(year,1980,1990,2000,2007,2013)

collapse  CtoH2  incwage_college incwage_nc pop  /// 
	(sum) clf firms wapop emp, by(citysizecat year)
	
gen relative_wages = incwage_college / incwage_nc


egen agg_clf = total(clf), by(year)
gen temp = clf*CtoH2
egen agg_CtoH = total(temp), by(year)
replace agg_CtoH = agg_CtoH/agg_clf
