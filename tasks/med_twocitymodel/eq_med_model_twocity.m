function [ diff] = eq_med_model_twocity(x,params)
   
    H1 = x(1);
    L1 = x(2);
    NH1 = x(3);
    NH2 = x(4);
    NL1 = x(5);
    NL2 = x(6);

    [ WL1, WH1, WL2,WH2, r1,r2,PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2, UL1,UH1,UL2,UH2] ...
        = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params);


    %parameters 
    zH=params.zH; 
    zL=params.zL; 
    ep=params.ep;
    btaH=params.btaH;
    btaL=params.btaL;
    al=params.al;
    gmL=params.gammaL;
    gmH=params.gammaH;
    fc=params.fc;
    bc=params.bc;
    feH=params.feH;
    feL=params.feL;
    B=params.B;
    L=params.L;
    H=params.H;
    lam=params.lambda;

    H2 = params.H - H1;
    L2 = params.L - L1; 

    %utilites equal 
    eqUH = UH1 - UH2;
    eqUL = UL1 - UL2; 
    
    %building market clears 
%     eqB1 = B - ((1-btaL)*WL1/r1*L1 + (1-btaH)*WH1/r1*H1 + NH1*bc + NL1*bc);
%     eqB2 = B - ((1-btaL)*WL2/r2*L2 + (1-btaH)*WH2/r2*H2 + NH2*bc + NL2*bc);
    eqB1 = r1^(1/(lam-1))/lam - (NH1*bc + NL1*bc);
    eqB2 = r2^(1/(lam-1))/lam - (NH2*bc + NL2*bc);
    %market clearing 
    %eqY = Y1 + Y2 - (btaH*WH1*H1 + btaH*WH2*H2 + btaL*WL1*L1 + btaL*WL2*L2);
    nH1 = H1/NH1 - fc; 
    nH2 = H2/NH2 - fc;
    %free entry
    eqfeH1 = r1*bc - (PH1*zH(1)*nH1^al(1) - WH1*(nH1+fc) - WH1*feH(1));
    eqfeH2 = r2*bc - (PH2*zH(2)*nH2^al(2) - WH2*(nH2+fc) - WH2*feH(2));
    
    diff = [eqUH, eqUL, eqB1, eqB2, eqfeH1, eqfeH2];

end