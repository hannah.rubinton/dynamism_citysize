function [ WL1, WH1, WL2, WH2,r1,r2, PL1,PH1,PL2,PH2,Y1,Y2,YL1,YH1,YL2,YH2,...
    UL1,UH1,UL2,UH2,results] = med_model_twocity(H1,L1,NH1,NH2,NL1,NL2,params)
    
    %parameters 
    zH=params.zH; 
    zL=params.zL; 
    ep=params.ep;
    btaH=params.btaH;
    btaL=params.btaL;
    al=params.al;
    gmL=params.gammaL;
    gmH=params.gammaH;
    fc=params.fc;
    bc=params.bc;
    feH=params.feH;
    feL=params.feL;
    B=params.B;
    L=params.L;
    H=params.H;
    th=params.theta;
    lam=params.lambda;
    AH1=params.AH1;
    AL1=params.AL1;

    H2 = H-H1;
    L2 = L-L1;
    
    %intermediate labor demand 
    nL1 = L1/NL1 - fc;
    nL2 = L2/NL2 - fc;
    nH1 = H1/NH1 - fc;
    nH2 = H2/NH2 - fc;
    
    %intermediate output
    yL1 = zL(1)*nL1^al(1);
    yL2 = zL(2)*nL2^al(2);
    yH1 = zH(1)*nH1^al(1);
    yH2 = zH(2)*nH2^al(2);
    YL1 = yL1*NL1;
    YL2 = yL2*NL2;
    YH1 = yH1*NH1;
    YH2 = yH2*NH2;
    
    %final good output and prices
    Y1 = (gmL(1)*YL1^((ep-1)/ep)+gmH(1)*YH1^((ep-1)/ep))^(ep/(ep-1));
    Y2 = (gmL(2)*YL2^((ep-1)/ep)+gmH(2)*YH2^((ep-1)/ep))^(ep/(ep-1));
    PL1 = Y1^(1/ep)*gmL(1)*YL1^(-1/ep);
    PH1 = Y1^(1/ep)*gmH(1)*YH1^(-1/ep); 
    PL2 = Y2^(1/ep)*gmL(2)*YL2^(-1/ep);
    PH2 = Y2^(1/ep)*gmH(2)*YH2^(-1/ep);  
    
    %wages from first order condition of intermediate
    WL1 = PL1*zL(1)*al(1)*nL1^(al(1)-1);
    WH1 = PH1*zH(1)*al(1)*nH1^(al(1)-1);
    WL2 = PL2*zL(2)*al(2)*nL2^(al(2)-1);
    WH2 = PH2*zH(2)*al(2)*nH2^(al(2)-1);
    
    %rent from free entry condition 
    r1 = (PL1*zL(1)*nL1^al(1) - WL1*(nL1+fc) - WL1*feL(1))/bc;
    r2 = (PL2*zL(2)*nL2^al(2) - WL2*(nL2+fc) - WL2*feL(2))/bc;
    
    
    
%     %worker utility 
%     UH1 = btaH^btaH*(1-btaH)^(1-btaH)*WH1/r1^(1-btaH);
%     UH2 = btaH^btaH*(1-btaH)^(1-btaH)*WH2/r2^(1-btaH);
%     UL1 = btaL^btaL*(1-btaL)^(1-btaL)*WL1/r1^(1-btaL);
%     UL2 = btaL^btaL*(1-btaL)^(1-btaL)*WL2/r2^(1-btaL); 
    %congestion 
    CH1 = params.psiH*(H1+L1)^params.alH;
    CH2 = params.psiH*(H2+L2)^params.alH;
    CL1 = params.psiL*(H1+L1)^params.alL;
    CL2 = params.psiL*(H2+L2)^params.alL;
    %u = @(w) (w^(1-th)-1)/(1-th);
    u = @(c) (c^(1-th))/(1-th);
    %worker utility 
    UH1 = AH1*u(WH1) - CH1;
    UH2 = u(WH2) - CH2;
    UL1 = AL1*u(WL1) - CL1;
    UL2 = u(WL2) - CL2; 
    
    
    results.sk_premium = [WH1/WL1, WH2/WL2];
    results.sizepremium = [WH1/WH2, WL1/WL2];
    results.citysize = [L1+H1, L2+H2];
    results.Hshare = [H1/(L1+H1), H2/(L2+H2)];
    results.avgW = (H1*WH1 + H2*WH2 + L1*WL1 + L2*WL2)/(params.L+params.H) ;
    if params.year==1980
        results.WL2_WL2 = WL2/WL2;
        results.WH2_WL2 = WH2/WL2;
        results.WH1_WL2 = WH1/WL2;
        results.WL1_WL2 = WL1/WL2;
    else 
      results.WL2_WL2 = WL2/params.WL2;
        results.WH2_WL2 = WH2/params.WL2;
        results.WH1_WL2 = WH1/params.WL2;
        results.WL1_WL2 = WL1/params.WL2;  
    end
    results.H1share = H1/L1;
    results.H2share = H2/L2;
    results.relative_N = [NH1/NL1, NH2/NL2]; 
    results.HperN = [H1/NH1, H2/NH2];
    results.LperN = [L1/NL1, L2/NL2];
end
    
    
    
    
    
    