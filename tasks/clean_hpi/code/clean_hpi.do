
cap log close
clear
set more off

global HPI "/Users/hannah/Google Drive File Stream/My Drive/rawdata/HPI"
global data "/Users/hannah/Dropbox/dynamism_citysize/tasks/clean_hpi/output"

/******************
Downloaded from https://www.fhfa.gov/DataTools/Downloads/Pages/House-Price-Index-Datasets.aspx#qat
quarterly all transactions index - non seasonally adjusted
*******************/
import delimited "$HPI/HPI_AT_metro.csv" , clear

rename v1 cbsaname
rename v2 cbsa
rename v3 year 
rename v4 quarter
rename v5 HPI
drop cbsaname v6
drop if HPI=="-"
destring HPI, replace

collapse (mean) HPI, by(year cbsa)


save "$data/hpi_cbsa.dta", replace 









